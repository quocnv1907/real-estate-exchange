package com.estate.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "estate_photo")
public class EstatePhotoAlbum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "image_url_main", columnDefinition = "LONGTEXT")
    private String imageUrlMain;

    @Column(name = "image_url_1", columnDefinition = "LONGTEXT")
    private String imageUrl1;

    @Column(name = "image_url_2", columnDefinition = "LONGTEXT")
    private String imageUrl2;

    @Column(name = "image_url_3", columnDefinition = "LONGTEXT")
    private String imageUrl3;

    @Column(name = "image_url_4", columnDefinition = "LONGTEXT")
    private String imageUrl4;

    @Column(name = "estate_id")
    private long estateId;

    public EstatePhotoAlbum() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageUrlMain() {
        return imageUrlMain;
    }

    public void setImageUrlMain(String mainUrl) {
        this.imageUrlMain = mainUrl;
    }

    public String getImageUrl1() {
        return imageUrl1;
    }

    public void setImageUrl1(String image1) {
        this.imageUrl1 = image1;
    }

    public String getImageUrl2() {
        return imageUrl2;
    }

    public void setImageUrl2(String image2) {
        this.imageUrl2 = image2;
    }

    public String getImageUrl3() {
        return imageUrl3;
    }

    public void setImageUrl3(String image3) {
        this.imageUrl3 = image3;
    }

    public String getImageUrl4() {
        return imageUrl4;
    }

    public void setImageUrl4(String imageUrl4) {
        this.imageUrl4 = imageUrl4;
    }

    @Transient
    private String estateName;

    public long getEstateId() {
        return estateId;
    }

    public void setEstateId(long estateId) {
        this.estateId = estateId;
    }

    public String getEstateName() {
        return estateName;
    }

    public void setEstateName(String estateName) {
        this.estateName = estateName;
    }

}
