package com.estate.entity;

import java.util.List;

import jakarta.persistence.*;

@Entity
@Table(name = "provinces")
public class Province {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    // @JsonManagedReference
    @OneToMany(mappedBy = "province",fetch = FetchType.LAZY)
    private List<District> districts;

    // quan hệ one to many với bảng street
   // @JsonManagedReference
    @OneToMany(mappedBy = "province", fetch = FetchType.LAZY)
    private List<Street> streets;

    public Province() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

}
