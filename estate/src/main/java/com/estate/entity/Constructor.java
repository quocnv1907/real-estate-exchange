package com.estate.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "constructor")
public class Constructor {

     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;

     @Column(name = "name", nullable = false)
     private String name;

     @Column(name = "description")
     private String description;

     @Column(name = "address")
     private String address;

     @Column(name = "phone")
     private String phone;

     @Column(name = "phone2")
     private String phone2;

     @Column(name = "fax")
     private String fax;

     @Column(name = "email")
     private String email;

     @Column(name = "website")
     private String website;
     
     public Constructor() {
     }

     public long getId() {
          return id;
     }

     public void setId(long id) {
          this.id = id;
     }

     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }

     public String getDescription() {
          return description;
     }

     public void setDescription(String description) {
          this.description = description;
     }

     public String getAddress() {
          return address;
     }

     public void setAddress(String address) {
          this.address = address;
     }

     public String getPhone() {
          return phone;
     }

     public void setPhone(String phone) {
          this.phone = phone;
     }

     public String getPhone2() {
          return phone2;
     }

     public void setPhone2(String phone2) {
          this.phone2 = phone2;
     }

     public String getFax() {
          return fax;
     }

     public void setFax(String fax) {
          this.fax = fax;
     }

     public String getEmail() {
          return email;
     }

     public void setEmail(String email) {
          this.email = email;
     }

     public String getWebsite() {
          return website;
     }

     public void setWebsite(String website) {
          this.website = website;
     }
}
