package com.estate.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.estate.entity.Project;
import com.estate.entity.ProjectPhotoAlbum;
import com.estate.repository.ProjectImageRepository;
import com.estate.repository.ProjectRepository;

public class ProjectImageService {
    @Autowired
    private ProjectRepository projectRepo;

    @Autowired
    private ProjectImageRepository projectImageRepo;

    // lấy danh sách tất cả album
    public List<ProjectPhotoAlbum> getListAlbum() {
        return projectImageRepo.findAll();
    }

    public List<ProjectPhotoAlbum> getAllEstateImage() {
        List<Object[]> results = projectImageRepo.findAllProjectImage();
        return results.stream().map(obj -> {
            ProjectPhotoAlbum album = new ProjectPhotoAlbum();
            album.setId((Long) obj[0]);
            album.setImageUrlMain((String) obj[1]);
            album.setImageUrl1((String) obj[2]);
            album.setImageUrl2((String) obj[3]);
            album.setImageUrl3((String) obj[4]);
            album.setImageUrl4((String) obj[5]);
            album.setProjectName((String) obj[6]);
            return album;
        }).collect(Collectors.toList());
    }

    // get district by id
    public Optional<ProjectPhotoAlbum> getAlbumById(long id) {
        return projectImageRepo.findById(id);
    }

    // create new album
    public ProjectPhotoAlbum createAlbum(ProjectPhotoAlbum album, long estateId) throws Exception {
        ProjectPhotoAlbum newAlbum = new ProjectPhotoAlbum();
        Optional<Project> projectData = projectRepo.findById(estateId);
        if (projectData.isPresent()) {
            newAlbum.setImageUrlMain(album.getImageUrlMain());
            newAlbum.setImageUrl1(album.getImageUrl1());
            newAlbum.setImageUrl2(album.getImageUrl2());
            newAlbum.setImageUrl3(album.getImageUrl3());
            newAlbum.setImageUrl4(album.getImageUrl4());
            newAlbum.setProjectId(album.getProjectId());
        }
        return projectImageRepo.save(newAlbum);
    }

    // update
    public ProjectPhotoAlbum updateAlbumById(long id, ProjectPhotoAlbum albumDetail) throws Exception {
        Optional<ProjectPhotoAlbum> existAlbum = projectImageRepo.findById(id);
        if (existAlbum.isPresent()) {
            ProjectPhotoAlbum album = existAlbum.get();
            album.setImageUrlMain(albumDetail.getImageUrlMain());
            album.setImageUrl1(albumDetail.getImageUrl1());
            album.setImageUrl2(albumDetail.getImageUrl2());
            album.setImageUrl3(albumDetail.getImageUrl3());
            album.setImageUrl4(albumDetail.getImageUrl4());
            return projectImageRepo.save(album);
        } else {
            throw new Exception("District not found");
        }
    }

    // delete by ID
    public boolean deleteAlbumById(long id) {
        if (projectImageRepo.existsById(id)) {
            projectImageRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<ProjectPhotoAlbum> getAllProjectImages() {
        return projectImageRepo.findAllProjectImage().stream().map(obj -> {
            ProjectPhotoAlbum album = new ProjectPhotoAlbum();
            album.setId(((Number) obj[0]).longValue());
            album.setImageUrlMain((String) obj[1]);
            album.setImageUrl1((String) obj[2]);
            album.setImageUrl2((String) obj[3]);
            album.setImageUrl3((String) obj[4]);
            album.setImageUrl4((String) obj[5]);
            return album;
        }).collect(Collectors.toList());
    }
}
