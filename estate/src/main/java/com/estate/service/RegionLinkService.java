package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.RegionLink;
import com.estate.repository.RegionLinkRepository;

@Service
public class RegionLinkService {

    @Autowired
    private RegionLinkRepository regionLinkRepo;

    // list all
    public List<RegionLink> getListRegionLink() {
        return regionLinkRepo.findAll();
    }

    public Optional<RegionLink> getRegionLinkById(long id) {
        return regionLinkRepo.findById(id);
    }

    // tạo mới address map
    public RegionLink createRegionLink(RegionLink regionLink, long projectId) throws Exception {
        RegionLink newRegionLink = new RegionLink();
       // Optional<Project> projectData = projectRepo.findById(projectId);
       // if (projectData.isPresent()) {
         //   Project project = projectData.get();
            newRegionLink.setName(regionLink.getName());
            newRegionLink.setDescription(regionLink.getDescription());
            newRegionLink.setPhoto(regionLink.getPhoto());
            newRegionLink.setAddress(regionLink.getAddress());
            newRegionLink.setLatitude(regionLink.getLongtitude());
            newRegionLink.setLongtitude(regionLink.getLongtitude());
       // }
        return regionLinkRepo.save(newRegionLink);
    }

    // update address map
    public RegionLink updateRegionLinkById(long id, RegionLink regionLink) throws Exception {
        Optional<RegionLink> regionLinkData = regionLinkRepo.findById(id);
        if (regionLinkData.isPresent()) {
            RegionLink existRegionLink = regionLinkData.get();
            existRegionLink.setName(regionLink.getName());
            existRegionLink.setDescription(regionLink.getDescription());
            existRegionLink.setPhoto(regionLink.getPhoto());
            existRegionLink.setAddress(regionLink.getAddress());
            existRegionLink.setLatitude(regionLink.getLongtitude());
            existRegionLink.setLongtitude(regionLink.getLongtitude());
            return regionLinkRepo.save(existRegionLink);
        } else {
            throw new Exception("Regionlink not found");
        }
    }

    // check if country exist
    public boolean deleteARegionLinkById(long id) {
        if (regionLinkRepo.existsById(id)) {
            regionLinkRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
