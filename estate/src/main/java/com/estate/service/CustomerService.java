package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.Customer;
import com.estate.repository.CustomerRepository;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepo;

    public List<Customer> getListCustomer() {
        return customerRepo.findAll();
    }

    public Optional<Customer> getCustomerById(long id) {
        return customerRepo.findById(id);
    }

    public Customer createNewCustomer(Customer customer) throws Exception {
        Customer newCustomer = new Customer();
        newCustomer.setUserName(customer.getUserName());
        newCustomer.setPassword(customer.getPassword());
        newCustomer.setAddress(customer.getAddress());
        newCustomer.setMobile(customer.getMobile());
        newCustomer.setEmail(customer.getEmail());
        newCustomer.setNote(customer.getNote());
        newCustomer.setCreatedBy(customer.getCreatedBy());
        newCustomer.setUpdateBy(customer.getUpdateBy());
        newCustomer.setCreateDate(customer.getCreateDate());
        newCustomer.setUpdateDate(customer.getUpdateDate());
        return customerRepo.save(newCustomer);
    }

    // cập nhật thông tin customer
    public Customer updateCustomerById(long id, Customer customerDetail) throws Exception {
        Optional<Customer> customerData = customerRepo.findById(id);
        if (customerData.isPresent()) {
            Customer existCustomer = customerData.get();
            existCustomer.setUserName(customerDetail.getUserName());
            existCustomer.setPassword(customerDetail.getPassword());
            existCustomer.setAddress(customerDetail.getAddress());
            existCustomer.setMobile(customerDetail.getMobile());
            existCustomer.setEmail(customerDetail.getEmail());
            existCustomer.setNote(customerDetail.getNote());
            existCustomer.setCreatedBy(customerDetail.getCreatedBy());
            existCustomer.setUpdateBy(customerDetail.getUpdateBy());
            existCustomer.setCreateDate(customerDetail.getCreateDate());
            existCustomer.setUpdateDate(customerDetail.getUpdateDate());
            return customerRepo.save(existCustomer);
        } else {
            throw new Exception("Customer not found");
        }
    }

    // check if country exist
    public boolean deleteCustomerById(long id) {
        if (customerRepo.existsById(id)) {
            customerRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
