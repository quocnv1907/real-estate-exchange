package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.AddressMap;
import com.estate.repository.AddressMapRepository;

@Service
public class AddressMapService { 
    
    @Autowired
    private AddressMapRepository addressMapRepo;

    //list all
    public List<AddressMap> getListAddressMap() {
        return addressMapRepo.findAll();
    }

    public Optional<AddressMap> getAddressMapById(long id) {
        return addressMapRepo.findById(id);
    }

    // tạo mới address map
    public AddressMap createAddressMap(AddressMap addressMap) throws Exception {
        AddressMap newAddressMap = new AddressMap();
        newAddressMap.setAddress(addressMap.getAddress());
        newAddressMap.setLatitude(addressMap.getLatitude());
        newAddressMap.setLongtitude(addressMap.getLongtitude());
        return addressMapRepo.save(newAddressMap);
    }

    // update address map
    public AddressMap updateAddressMapById(long id, AddressMap addressMap) throws Exception {
        Optional<AddressMap> addressMapData = addressMapRepo.findById(id);
        if (addressMapData.isPresent()) {
            AddressMap existAddressMap = addressMapData.get();
            existAddressMap.setAddress(addressMap.getAddress());
            existAddressMap.setLatitude(addressMap.getLatitude());
            existAddressMap.setLongtitude(addressMap.getLongtitude());
            return addressMapRepo.save(existAddressMap);
        } else {
            throw new Exception("Address map not found");
        }
    }

    // check if country exist
    public boolean deleteAddressMapById(long id) {
        if (addressMapRepo.existsById(id)) {
            addressMapRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
 

}
