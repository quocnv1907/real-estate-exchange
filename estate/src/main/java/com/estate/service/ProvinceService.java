package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.District;
import com.estate.entity.Province;
import com.estate.repository.ProvinceRepository;

@Service
public class ProvinceService {

    @Autowired
    private ProvinceRepository provinceRepo;

    // list all
    public List<Province> getListProvince() {
        return provinceRepo.findAll();
    }

    public Optional<Province> getProvinceById(long id) {
        return provinceRepo.findById(id);
    }

    // tạo mới province
    public Province createProvince(Province province) throws Exception {
        Province newProvince = new Province();
        newProvince.setCode(province.getCode());
        newProvince.setName(province.getName());
        return provinceRepo.save(newProvince);
    }

    // update address map
    public Province updateProvinceById(long id, Province province) throws Exception {
        Optional<Province> provinceData = provinceRepo.findById(id);
        if (provinceData.isPresent()) {
            Province existProvince = provinceData.get();
            existProvince.setCode(province.getCode());
            existProvince.setName(province.getName());
            return provinceRepo.save(existProvince);
        } else {
            throw new Exception("Province  not found");
        }
    }

    // check if country exist
    public boolean deleteProvinceById(long id) {
        if (provinceRepo.existsById(id)) {
            provinceRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    // get district by province Id
    public List<District> getDistrictByProvinceId(long provinceId) {
        Province province = provinceRepo.findById(provinceId).get();
        if (province != null) {
            return province.getDistricts();
        }
        return null;
    }

}
