package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.Constructor;
import com.estate.repository.ConstructorRepository;

@Service
public class ConstructorService {
    @Autowired
    private ConstructorRepository constructorRepo;

    public List<Constructor> getListConstructor() {
        return constructorRepo.findAll();
    }

    public Optional<Constructor> getConstructorById(long id) {
        return constructorRepo.findById(id);
    }

    public Constructor createNewConstructor(Constructor constructor) throws Exception {
        Constructor newConstructor = new Constructor();
        newConstructor.setName(constructor.getName());
        newConstructor.setDescription(constructor.getDescription());
        newConstructor.setAddress(constructor.getEmail());
        newConstructor.setPhone(constructor.getPhone());
        newConstructor.setPhone2(constructor.getPhone2());
        newConstructor.setFax(constructor.getFax());
        newConstructor.setEmail(constructor.getEmail());
        newConstructor.setWebsite(constructor.getWebsite());
        return constructorRepo.save(newConstructor);
    }

    // update country
    public Constructor updateConstructorById(long id, Constructor constructor) throws Exception {
        Optional<Constructor> constructorData = constructorRepo.findById(id);
        if (constructorData.isPresent()) {
            Constructor existConstructor = constructorData.get();
            existConstructor.setName(constructor.getName());
            existConstructor.setDescription(constructor.getDescription());
            existConstructor.setAddress(constructor.getAddress());
            existConstructor.setPhone(constructor.getPhone());
            existConstructor.setPhone2(constructor.getPhone2());
            existConstructor.setFax(constructor.getFax());
            existConstructor.setEmail(constructor.getEmail());
            existConstructor.setWebsite(constructor.getWebsite());
            return constructorRepo.save(existConstructor);
        } else {
            throw new Exception("Constructor not found");
        }
    }

    // check if country exist
    public boolean deleteConstructorById(long id) {
        if (constructorRepo.existsById(id)) {
            constructorRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
