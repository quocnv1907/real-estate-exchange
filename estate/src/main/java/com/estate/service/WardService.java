package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.District;
import com.estate.entity.Ward;
import com.estate.repository.DistrictRepository;
import com.estate.repository.ProvinceRepository;
import com.estate.repository.WardRepository;

@Service
public class WardService {

    @Autowired
    private WardRepository wardRepo;

    @Autowired
    private ProvinceRepository provinceRepo;

    @Autowired
    private DistrictRepository districtRepo;

    // get all employee
    public List<Ward> getListWard() {
        return wardRepo.findAll();
    }

    // GET employee by id
    public Optional<Ward> getWardById(long id) {
        return wardRepo.findById(id);
    }

    // CREATE new employee
    public Ward createNewWard(Ward ward, long districtId) throws Exception {
        Optional<District> districtData = districtRepo.findById(districtId);
        Ward newWard = new Ward();
        if (districtData.isPresent()) {
           // District district = districtData.get();
            newWard.setName(ward.getName());
            newWard.setPrefix(ward.getName());
          //  newWard.setDistrict(district);
        }
        return wardRepo.save(newWard);
    }

    // UPDATE
    public Ward updateWardById(long id, Ward ward) throws Exception {
        Optional<Ward> wardData = wardRepo.findById(id);
        if (wardData.isPresent()) {
            Ward existWard = wardData.get();
            existWard.setName(ward.getName());
            existWard.setPrefix(ward.getPrefix());
            return wardRepo.save(existWard);
        } else {
            throw new Exception("ward not found");
        }
    }

    // DELETE by id
    public boolean deleteWardById(Long id) {
        if (wardRepo.existsById(id)) {
            wardRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
