package com.estate.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.estate.entity.EstatePhotoAlbum;
import com.estate.entity.RealEstate;
import com.estate.repository.EstatePhotoRepository;
import com.estate.repository.RealEstateRepository;

@Service
public class EstatePhotoService {
    @Autowired
    private RealEstateRepository realEstateRepo;

    @Autowired
    private EstatePhotoRepository albumRepo;

    // lấy danh sách tất cả album
    // public List<EstatePhotoAlbum> getListAlbum() {
    //     return albumRepo.findAll();
    // }

    public List<EstatePhotoAlbum> getAllEstateImage() {
        List<Object[]> results = albumRepo.findAllEstateImage();
        return results.stream().map(obj -> {
            EstatePhotoAlbum album = new EstatePhotoAlbum();
            album.setId((Long) obj[0]);
            album.setImageUrlMain((String) obj[1]);
            album.setImageUrl1((String) obj[2]);
            album.setImageUrl2((String) obj[3]);
            album.setImageUrl3((String) obj[4]);
            album.setImageUrl4((String) obj[5]);
            album.setEstateName((String) obj[6]);
            return album;
        }).collect(Collectors.toList());
    }

    // get album by id
    public Optional<EstatePhotoAlbum> getAlbumById(long id) {
        return albumRepo.findById(id);
    }

    // create new album
    public EstatePhotoAlbum createAlbum(EstatePhotoAlbum album, long estateId) throws Exception {
        EstatePhotoAlbum newAlbum = new EstatePhotoAlbum();
        Optional<RealEstate> estateData = realEstateRepo.findById(estateId);
        if (estateData.isPresent()) {
            newAlbum.setImageUrlMain(album.getImageUrlMain());
            newAlbum.setImageUrl1(album.getImageUrl1());
            newAlbum.setImageUrl2(album.getImageUrl2());
            newAlbum.setImageUrl3(album.getImageUrl3());
            newAlbum.setImageUrl4(album.getImageUrl4());
            newAlbum.setEstateId(album.getEstateId());
        }
        return albumRepo.save(newAlbum);
    }

    // update
    public EstatePhotoAlbum updateAlbumById(long id, EstatePhotoAlbum albumDetail) throws Exception {
        Optional<EstatePhotoAlbum> existAlbum = albumRepo.findById(id);
        if (existAlbum.isPresent()) {
            EstatePhotoAlbum album = existAlbum.get();
            album.setImageUrlMain(albumDetail.getImageUrlMain());
            album.setImageUrl1(albumDetail.getImageUrl1());
            album.setImageUrl2(albumDetail.getImageUrl2());
            album.setImageUrl3(albumDetail.getImageUrl3());
            album.setImageUrl4(albumDetail.getImageUrl4());
            return albumRepo.save(album);
        } else {
            throw new Exception("District not found");
        }
    }

    // delete by ID
    public boolean deleteAlbumById(long id) {
        if (albumRepo.existsById(id)) {
            albumRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<EstatePhotoAlbum> getAllEstatePhotoAlbums() {
        return albumRepo.findAllEstateImage().stream().map(obj -> {
            EstatePhotoAlbum album = new EstatePhotoAlbum();
            album.setId(((Number) obj[0]).longValue());
            album.setImageUrlMain((String) obj[1]);
            album.setImageUrl1((String) obj[2]);
            album.setImageUrl2((String) obj[3]);
            album.setImageUrl3((String) obj[4]);
            album.setImageUrl4((String) obj[5]);
            return album;
        }).collect(Collectors.toList());
    }
}
