package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.DesignUnit;
import com.estate.repository.DesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    private DesignUnitRepository designUnitRepo;

    public List<DesignUnit> getListDesignUnit() {
        return designUnitRepo.findAll();
    }

    public Optional<DesignUnit> getDesignUnitById(long id) {
        return designUnitRepo.findById(id);
    }

    public DesignUnit createNewDesignUnit(DesignUnit designUnit) throws Exception {
        DesignUnit newDesignUnit = new DesignUnit();
        newDesignUnit.setName(designUnit.getName());
        newDesignUnit.setDescription(designUnit.getDescription());
        newDesignUnit.setAddress(designUnit.getAddress());
        newDesignUnit.setPhone(designUnit.getPhone());
        newDesignUnit.setPhone2(designUnit.getPhone2());
        newDesignUnit.setFax(designUnit.getFax());
        newDesignUnit.setEmail(designUnit.getEmail());
        newDesignUnit.setWebsite(designUnit.getWebsite());
        newDesignUnit.setFax(designUnit.getFax());
        return designUnitRepo.save(newDesignUnit);
    }

    // update country
    public DesignUnit updateDesignUnitById(long id, DesignUnit designUnit) throws Exception {
        Optional<DesignUnit> designUnitData = designUnitRepo.findById(id);
        if (designUnitData.isPresent()) {
            DesignUnit existDesignUnit = designUnitData.get();
            existDesignUnit.setName(designUnit.getName());
            existDesignUnit.setDescription(designUnit.getDescription());
            existDesignUnit.setAddress(designUnit.getAddress());
            existDesignUnit.setPhone(designUnit.getPhone());
            existDesignUnit.setPhone2(designUnit.getPhone2());
            existDesignUnit.setFax(designUnit.getFax());
            existDesignUnit.setEmail(designUnit.getEmail());
            existDesignUnit.setWebsite(designUnit.getWebsite());
            existDesignUnit.setFax(designUnit.getFax());
            return designUnitRepo.save(existDesignUnit);
        } else {
            throw new Exception("Design unit not found");
        }
    }

    // check if country exist
    public boolean deleteDesignUnitById(long id) {
        if (designUnitRepo.existsById(id)) {
            designUnitRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
