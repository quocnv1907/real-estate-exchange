package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.Investor;
import com.estate.repository.InvestorRepository;

@Service
public class InvestorService {

    @Autowired
    private InvestorRepository investorRepo;

    // lấy danh sách tất cả investor
    public List<Investor> getListInvestor() {
        return investorRepo.findAll();
    }

    // lấy chi tiết investor
    public Optional<Investor> getInvestorById(long id) {
        return investorRepo.findById(id);
    }

    // tạo mới investor
    public Investor createInvestor(Investor investor) throws Exception {
        Investor newInvestor = new Investor();
        newInvestor.setName(investor.getName());
        newInvestor.setDescription(investor.getDescription());
        newInvestor.setAddress(investor.getAddress());
        newInvestor.setPhone(investor.getPhone());
        newInvestor.setPhone2(investor.getPhone2());
        newInvestor.setEmail(investor.getEmail());
        newInvestor.setFax(investor.getFax());
        newInvestor.setWebsite(investor.getWebsite());
        return investorRepo.save(newInvestor);
    }

    // update address map
    public Investor updateInvestorById(long id, Investor investor) throws Exception {
        Optional<Investor> investorData = investorRepo.findById(id);
        if (investorData.isPresent()) {
            Investor existInvestor = investorData.get();
            existInvestor.setName(investor.getName());
            existInvestor.setDescription(investor.getDescription());
            existInvestor.setAddress(investor.getAddress());
            existInvestor.setPhone(investor.getPhone());
            existInvestor.setPhone2(investor.getPhone2());
            existInvestor.setEmail(investor.getEmail());
            existInvestor.setWebsite(investor.getWebsite());
            existInvestor.setFax(investor.getFax());
            return investorRepo.save(existInvestor);
        } else {
            throw new Exception("Investor map not found");
        }
    }

    // check if country exist
    public boolean deleteInvestorById(long id) {
        if (investorRepo.existsById(id)) {
            investorRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
