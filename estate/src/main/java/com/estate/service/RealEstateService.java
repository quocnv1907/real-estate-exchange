package com.estate.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.estate.entity.RealEstate;
import com.estate.repository.RealEstateRepository;

@Service
public class RealEstateService {

    @Autowired
    private RealEstateRepository estateRepo;

    // lấy danh sách ( có album)
    public List<RealEstate> getAllRealEstate() {
        List<Object[]> results = estateRepo.findAllRealEstate();
        return results.stream().map(obj -> {
            RealEstate realEstate = new RealEstate();
            realEstate.setId((Long) obj[0]);
            realEstate.setBathroom((Integer) obj[1]);
            realEstate.setBedroom((Integer) obj[2]);
            realEstate.setConstructorId((Long) obj[3]);
            realEstate.setCreateBy((String) obj[4]);
            realEstate.setCreateDate((Date) obj[5]);
            realEstate.setDescription((String) obj[6]);
            realEstate.setDesignerId((Long) obj[7]);
            realEstate.setDistrictId((Long) obj[8]);
            realEstate.setEstateType((String) obj[9]);
            realEstate.setFurnitureType((String) obj[10]);
            realEstate.setInvestorId((Long) obj[11]);
            realEstate.setLatitude((Double) obj[12]);
            realEstate.setLegalDocument((String) obj[13]);
            realEstate.setLength((Double) obj[14]);
            realEstate.setLongtitude((Double) obj[15]);
            realEstate.setPrice((Integer) obj[16]);
            realEstate.setPriceRent((Double) obj[17]);
            realEstate.setProvinceId((Long) obj[18]);
            realEstate.setStatus((String) obj[19]);
            realEstate.setStreetId((Long) obj[20]);
            realEstate.setStructure((String) obj[21]);
            realEstate.setUpdateBy((String) obj[22]);
            realEstate.setWardId((Long) obj[23]);
            realEstate.setWidth((Double) obj[24]);
            realEstate.setTitle((String) obj[25]);
            realEstate.setImageUrlMain((String) obj[26]);
            realEstate.setImageUrl1((String) obj[27]);
            realEstate.setImageUrl2((String) obj[28]);
            realEstate.setImageUrl3((String) obj[29]);
            realEstate.setImageUrl4((String) obj[30]);
            realEstate.setProvinceName((String) obj[31]);
            realEstate.setDistrictName((String) obj[32]);
            realEstate.setWardName((String) obj[33]);
            realEstate.setStreetName((String) obj[34]);
            realEstate.setInvestorName((String) obj[35]);
            realEstate.setConstructorName((String) obj[36]);
            realEstate.setDesignerName((String) obj[37]);
            return realEstate;
        }).collect(Collectors.toList());
    }

    public RealEstate getRealEstateDetail(long estateId) {
        List<RealEstate> allEstates = getAllRealEstate();
        return allEstates.stream()
                .filter(estate -> estate.getId() == estateId)
                .findFirst()
                .orElse(null);
    }

    public Page<RealEstate> getAllRealEstateFilteredWithPagination(
            String estateName, String provinceName, String estateType1,
            String estateType2, String estateType3, Integer minPrice,
            Integer maxPrice, Double minArea, Double maxArea,
            int page, int size) {

        // Lọc danh sách theo các điều kiện
        List<RealEstate> filteredRealEstates = getAllRealEstate().stream()
                .filter(realEstate -> (estateName == null
                        || realEstate.getTitle().toLowerCase().contains(estateName.toLowerCase())) &&
                        (provinceName == null || realEstate.getProvinceName() != null
                                && realEstate.getProvinceName().equalsIgnoreCase(provinceName))
                        &&
                        (estateType1 == null || realEstate.getEstateType().equalsIgnoreCase(estateType1)) &&
                        (estateType2 == null || realEstate.getEstateType().equalsIgnoreCase(estateType2)) &&
                        (estateType3 == null || realEstate.getEstateType().equalsIgnoreCase(estateType3)) &&
                        (minPrice == null || realEstate.getPrice() >= minPrice) &&
                        (maxPrice == null || realEstate.getPrice() <= maxPrice) &&
                        (minArea == null || (realEstate.getLength() * realEstate.getWidth()) >= minArea) &&
                        (maxArea == null || (realEstate.getLength() * realEstate.getWidth()) <= maxArea))
                .collect(Collectors.toList());

        // Tính toán phân trang
        int start = Math.min(page * size, filteredRealEstates.size());
        int end = Math.min((page + 1) * size, filteredRealEstates.size());

        // Lấy danh sách theo trang
        List<RealEstate> pagedResult = filteredRealEstates.subList(start, end);

        // Trả về đối tượng Page
        return new PageImpl<>(pagedResult, PageRequest.of(page, size), filteredRealEstates.size());
    }

    //
    public RealEstate createEstate(RealEstate estate) throws Exception {
        RealEstate newEstate = new RealEstate();
        newEstate.setTitle(estate.getTitle());
        newEstate.setEstateType(estate.getEstateType());
        newEstate.setStatus(estate.getStatus());
        newEstate.setPrice(estate.getPrice());
        newEstate.setPriceRent(estate.getPriceRent());
        newEstate.setBathroom(estate.getBathroom());
        newEstate.setBedroom(estate.getBedroom());
        newEstate.setLength(estate.getLength());
        newEstate.setWidth(estate.getWidth());
        newEstate.setLegalDocument(estate.getLegalDocument());
        newEstate.setFurnitureType(estate.getFurnitureType());
        newEstate.setDescription(estate.getDescription());
        newEstate.setStructure(estate.getStructure());
        newEstate.setProvinceId(estate.getProvinceId());
        newEstate.setDistrictId(estate.getDistrictId());
        newEstate.setWardId(estate.getWardId());
        newEstate.setStreetId(estate.getStreetId());
        newEstate.setInvestorId(estate.getInvestorId());
        newEstate.setConstructorId(estate.getConstructorId());
        newEstate.setDesignerId(estate.getDesignerId());
        newEstate.setImageUrlMain(estate.getImageUrlMain());
        newEstate.setImageUrl1(estate.getImageUrl1());
        newEstate.setImageUrl2(estate.getImageUrl2());
        newEstate.setImageUrl3(estate.getImageUrl3());
        newEstate.setImageUrl4(estate.getImageUrl4());
        return estateRepo.save(newEstate);
    }

    // update
    public RealEstate updateEstateById(long id, RealEstate estate) throws Exception {
        Optional<RealEstate> estateData = estateRepo.findById(id);
        if (estateData.isPresent()) {
            RealEstate existEstate = estateData.get();
            existEstate.setTitle(estate.getTitle());
            existEstate.setEstateType(estate.getEstateType());
            existEstate.setStatus(estate.getStatus());
            existEstate.setPrice(estate.getPrice());
            existEstate.setPriceRent(estate.getPriceRent());
            existEstate.setBathroom(estate.getBathroom());
            existEstate.setBedroom(estate.getBedroom());
            existEstate.setLength(estate.getLength());
            existEstate.setWidth(estate.getWidth());
            existEstate.setLegalDocument(estate.getLegalDocument());
            existEstate.setFurnitureType(estate.getFurnitureType());
            existEstate.setDescription(estate.getDescription());
            existEstate.setProvinceId(estate.getProvinceId());
            existEstate.setDistrictId(estate.getDistrictId());
            existEstate.setWardId(estate.getWardId());
            existEstate.setStructure(estate.getStructure());
            existEstate.setStreetId(estate.getStreetId());
            existEstate.setInvestorId(estate.getInvestorId());
            existEstate.setConstructorId(estate.getConstructorId());
            existEstate.setDesignerId(estate.getDesignerId());
            existEstate.setImageUrlMain(estate.getImageUrlMain());
            existEstate.setImageUrl1(estate.getImageUrl1());
            existEstate.setImageUrl2(estate.getImageUrl2());
            existEstate.setImageUrl3(estate.getImageUrl3());
            existEstate.setImageUrl4(estate.getImageUrl4());
            return estateRepo.save(existEstate);
        } else {
            throw new Exception("Estate not found");
        }
    }

    // check if country exist
    public boolean deleteEstateById(long id) {
        if (estateRepo.existsById(id)) {
            estateRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<RealEstate> getEstateByProvinceName(String provinceName) {
        List<RealEstate> allEstates = getAllRealEstate();
        return allEstates.stream()
                .filter(estate -> provinceName.equals(estate.getProvinceName()))
                .collect(Collectors.toList());
    }
}
