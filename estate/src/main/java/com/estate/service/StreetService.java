package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.District;
import com.estate.entity.Province;
import com.estate.entity.Street;
import com.estate.repository.DistrictRepository;
import com.estate.repository.ProvinceRepository;
import com.estate.repository.StreetRepository;

@Service
public class StreetService {

    @Autowired
    private StreetRepository streetRepo;

    @Autowired
    private ProvinceRepository provinceRepo;

    @Autowired
    private DistrictRepository districtRepo;

    // list all
    public List<Street> getListStreet() {
        return streetRepo.findAll();
    }

    public Optional<Street> getStreetById(long id) {
        return streetRepo.findById(id);
    }

    // tạo mới address map
    public Street createStreet(Street street, long provinceId, long districtId) throws Exception {
        Street newStreet = new Street();
        Optional<Province> provinceData = provinceRepo.findById(provinceId);
        Optional<District> districtData = districtRepo.findById(districtId);
        if (provinceData.isPresent() && districtData.isPresent()) {
            Province province = provinceData.get();
            District district = districtData.get();
            newStreet.setName(street.getName());
            newStreet.setPrefix(street.getPrefix());
            newStreet.setProvince(province);
            newStreet.setDistrict(district);
        }
        return streetRepo.save(newStreet);
    }

    // update address map
    public Street updateStreetById(long id, Street street) throws Exception {
        Optional<Street> streetData = streetRepo.findById(id);
        if (streetData.isPresent()) {
            Street existStreet = streetData.get();
            existStreet.setName(street.getName());
            existStreet.setPrefix(street.getPrefix());
            return streetRepo.save(existStreet);
        } else {
            throw new Exception("Street not found");
        }
    }

    // check if country exist
    public boolean deleteStreetById(long id) {
        if (streetRepo.existsById(id)) {
            streetRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<Street> getListStreetByProvinceAndDistrict(long provinceId, long districtId) {
        return streetRepo.findStreetByProvinceDistrict(provinceId, districtId);
    }

}
