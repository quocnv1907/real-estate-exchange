package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.MasterLayout;
import com.estate.entity.Project;
import com.estate.repository.MasterLayoutRepository;
import com.estate.repository.ProjectRepository;

@Service
public class MasterLayoutService {

    @Autowired
    private MasterLayoutRepository masterLayoutRepo;

    @Autowired
    private ProjectRepository projectRepo;

    // list all
    public List<MasterLayout> getListMasterLayout() {
        return masterLayoutRepo.findAll();
    }

    public Optional<MasterLayout> getMasterLayoutById(long id) {
        return masterLayoutRepo.findById(id);
    }

    // tạo mới master layout dựa trên project
    public MasterLayout createMasterLayout(MasterLayout masterLayout, long projectId) throws Exception {
        MasterLayout newMasterLayout = new MasterLayout();
        
        Optional<Project> projectData = projectRepo.findById(projectId);
        if (projectData.isPresent()) {
            Project project = projectData.get();
            newMasterLayout.setName(masterLayout.getName());
            newMasterLayout.setDescription(masterLayout.getDescription());
            newMasterLayout.setArea(masterLayout.getArea());
            newMasterLayout.setNumApartment(masterLayout.getNumApartment());
            newMasterLayout.setPhoto(masterLayout.getPhoto());
            newMasterLayout.setCreateDate(masterLayout.getCreateDate());
            newMasterLayout.setUpdateDate(masterLayout.getUpdateDate());
            newMasterLayout.setProject(project);
        }
        return masterLayoutRepo.save(newMasterLayout);
    }

    // update address map
    public MasterLayout updateMasterLayoutById(long id, MasterLayout masterLayout) throws Exception {
        Optional<MasterLayout> masterLayoutData = masterLayoutRepo.findById(id);
        if (masterLayoutData.isPresent()) {
            MasterLayout existMasterLayout = masterLayoutData.get();
            existMasterLayout.setName(masterLayout.getName());
            existMasterLayout.setDescription(masterLayout.getDescription());
            existMasterLayout.setArea(masterLayout.getArea());
            existMasterLayout.setNumApartment(masterLayout.getNumApartment());
            existMasterLayout.setPhoto(masterLayout.getPhoto());
            existMasterLayout.setCreateDate(masterLayout.getCreateDate());
            existMasterLayout.setUpdateDate(masterLayout.getUpdateDate());
            return masterLayoutRepo.save(existMasterLayout);
        } else {
            throw new Exception("Master layout  not found");
        }
    }

    // check if country exist
    public boolean deleteMasterLayoutById(long id) {
        if (masterLayoutRepo.existsById(id)) {
            masterLayoutRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
