package com.estate.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.Project;
import com.estate.repository.ProjectRepository;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepo;

    public List<Project> getListProjectWithoutImage() {
        return projectRepo.findListProjectWithoutImage().stream().map(obj -> {
            Project project = new Project();
            project.setId(((Number) obj[0]).longValue());
            project.setArea(((Number) obj[1]).doubleValue());
            project.setConstructArea(((Number) obj[2]).doubleValue());
            project.setConstructorId(((Number) obj[3]).longValue());
            project.setDescription((String) obj[4]);
            project.setDesignerId(((Number) obj[5]).longValue());
            project.setDistrictId(((Number) obj[6]).longValue());
            project.setInvestorId(((Number) obj[7]).longValue());
            project.setLatitude(((Number) obj[8]).doubleValue());
            project.setLongtitude(((Number) obj[9]).doubleValue());
            project.setName((String) obj[10]);
            project.setNumApartment(((Number) obj[11]).intValue());
            project.setNumBlock(((Number) obj[12]).intValue());
            project.setNumFloor(((Number) obj[13]).intValue());
            project.setProvinceId(((Number) obj[14]).longValue());
            project.setRegionLink((String) obj[15]);
            project.setStreetId(((Number) obj[16]).longValue());
            project.setUtilities((String) obj[17]);
            project.setWardId(((Number) obj[18]).longValue());
            project.setProvinceName((String) obj[19]);
            project.setDistrictName((String) obj[20]);
            project.setWardName((String) obj[21]);
            project.setStreetName((String) obj[22]);
            project.setInvestorName((String) obj[23]);
            project.setConstructorName((String) obj[24]);
            project.setDesignerName((String) obj[25]);
            return project;
        }).collect(Collectors.toList());
    }

    public List<Project> getAllProject() {
        return projectRepo.findAllProject().stream().map(obj -> {
            Project project = new Project();
            project.setId(((Number) obj[0]).longValue());
            project.setArea(((Number) obj[1]).doubleValue());
            project.setConstructArea(((Number) obj[2]).doubleValue());
            project.setConstructorId(((Number) obj[3]).longValue());
            project.setDescription((String) obj[4]);
            project.setDesignerId(((Number) obj[5]).longValue());
            project.setDistrictId(((Number) obj[6]).longValue());
            project.setInvestorId(((Number) obj[7]).longValue());
            project.setLatitude(((Number) obj[8]).doubleValue());
            project.setLongtitude(((Number) obj[9]).doubleValue());
            project.setName((String) obj[10]);
            project.setNumApartment(((Number) obj[11]).intValue());
            project.setNumBlock(((Number) obj[12]).intValue());
            project.setNumFloor(((Number) obj[13]).intValue());
            project.setProvinceId(((Number) obj[14]).longValue());
            project.setRegionLink((String) obj[15]);
            project.setStreetId(((Number) obj[16]).longValue());
            project.setUtilities((String) obj[17]);
            project.setWardId(((Number) obj[18]).longValue());
            project.setProvinceName((String) obj[19]);
            project.setDistrictName((String) obj[20]);
            project.setWardName((String) obj[21]);
            project.setStreetName((String) obj[22]);
            project.setInvestorName((String) obj[23]);
            project.setConstructorName((String) obj[24]);
            project.setDesignerName((String) obj[25]);
            project.setImageUrlMain((String) obj[26]);
            project.setImageUrl1((String) obj[27]);
            project.setImageUrl2((String) obj[28]);
            project.setImageUrl3((String) obj[29]);
            project.setImageUrl4((String) obj[30]);
            return project;
        }).collect(Collectors.toList());
    }

   
    public Optional<Project> getProjectById(long id) {
        return projectRepo.findById(id);
    }

    // tạo mới master layout dựa trên project
    public Project createProject(Project project)
            throws Exception {
        Project newProject = new Project();

        newProject.setName(project.getName());
        newProject.setDescription(project.getDescription());
        newProject.setArea(project.getArea());
        newProject.setConstructArea(project.getConstructArea());
        newProject.setNumBlock(project.getNumBlock());
        newProject.setNumFloor(project.getNumFloor());
        newProject.setNumApartment(project.getNumApartment());
        newProject.setUtilities(project.getUtilities());
        newProject.setRegionLink(project.getRegionLink());
        newProject.setLatitude(project.getLatitude());
        newProject.setLongtitude(project.getLongtitude());
        //
        newProject.setProvinceId(project.getProvinceId());
        newProject.setDistrictId(project.getDistrictId());
        newProject.setWardId(project.getWardId());
        newProject.setStreetId(project.getStreetId());
        newProject.setInvestorId(project.getInvestorId());
        newProject.setConstructorId(project.getConstructorId());
        newProject.setDesignerId(project.getDesignerId());
        return projectRepo.save(newProject);
    }

    // update address map
    public Project updateProjectById(long id, Project project) throws Exception {
        Optional<Project> projectData = projectRepo.findById(id);
        if (projectData.isPresent()) {
            Project existProject = projectData.get();
            existProject.setName(project.getName());
            existProject.setProvinceId(project.getProvinceId());
            existProject.setDistrictId(project.getDistrictId());
            existProject.setWardId(project.getWardId());
            existProject.setStreetId(project.getStreetId());
            existProject.setDescription(project.getDescription());
            existProject.setArea(project.getArea());
            existProject.setConstructArea(project.getConstructArea());
            existProject.setNumBlock(project.getNumBlock());
            existProject.setNumApartment(project.getNumApartment());
            existProject.setUtilities(project.getUtilities());
            existProject.setRegionLink(project.getRegionLink());
            existProject.setLatitude(project.getLatitude());
            existProject.setLongtitude(project.getLongtitude());
            existProject.setInvestorId(project.getInvestorId());
            existProject.setConstructorId(project.getConstructorId());
            existProject.setDesignerId(project.getDesignerId());
            return projectRepo.save(existProject);
        } else {
            throw new Exception("Project layout  not found");
        }
    }

    // check if country exist
    public boolean deleteProjectById(long id) {
        if (projectRepo.existsById(id)) {
            projectRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
