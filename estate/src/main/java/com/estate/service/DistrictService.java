package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.District;
import com.estate.entity.Province;
import com.estate.entity.Ward;
import com.estate.repository.DistrictRepository;
import com.estate.repository.ProvinceRepository;

@Service
public class DistrictService {

    @Autowired
    private DistrictRepository districtRepo;

    @Autowired
    private ProvinceRepository provinceRepo;

    // get list district
    public List<District> getDistricts() {
        return districtRepo.findAll();
    }

    // get district by id
    public Optional<District> getDistrictById(long id) {
        return districtRepo.findById(id);
    }

    // create new district by province ID
    public District createDistrict(District district, long provinceId) throws Exception {
        District newDistrict = new District();
        Optional<Province> provinceData = provinceRepo.findById(provinceId);
        if (provinceData.isPresent()) {
            Province province = provinceData.get();
            newDistrict.setName(district.getName());
            newDistrict.setPrefix(district.getPrefix());
            newDistrict.setProvince(province);
        }
        return districtRepo.save(newDistrict);
    }

    // update district
    public District updateDistrictById(long id, District districtDetails) throws Exception {
        Optional<District> existDistrict = districtRepo.findById(id);
        if (existDistrict.isPresent()) {
            District district = existDistrict.get();
            district.setName(districtDetails.getName());
            district.setPrefix(districtDetails.getPrefix());
            district.setWards(districtDetails.getWards());
            return districtRepo.save(district);
        } else {
            throw new Exception("District not found");
        }
    }

    // delete by ID
    public boolean deleteDistrictrById(long id) {
        if (districtRepo.existsById(id)) {
            districtRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public List<Ward> getWardByDistrictId(long districtId){
        District district = districtRepo.findById(districtId).get();
        if(district != null){
            return district.getWards();
        }
        return null;
    }

}
