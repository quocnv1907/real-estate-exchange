package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.Location;
import com.estate.repository.LocationRepository;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepo;

    // list all
    public List<Location> getListLocation() {
        return locationRepo.findAll();
    }

    public Optional<Location> getLocationById(long id) {
        return locationRepo.findById(id);
    }

    // tạo mới address map
    public Location createLocation(Location location) throws Exception {
        Location newLocation = new Location();
        newLocation.setLatitude(location.getLatitude());
        newLocation.setLongtitude(location.getLongtitude());
        return locationRepo.save(newLocation);
    }

    // update address map
    public Location updateLocationById(long id, Location location) throws Exception {
        Optional<Location> locationData = locationRepo.findById(id);
        if (locationData.isPresent()) {
            Location existLocation = locationData.get();
            existLocation.setLatitude(location.getLatitude());
            existLocation.setLongtitude(location.getLongtitude());
            return locationRepo.save(existLocation);
        } else {
            throw new Exception("Location  not found");
        }
    }

    // check if country exist
    public boolean deleteLocationById(long id) {
        if (locationRepo.existsById(id)) {
            locationRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
