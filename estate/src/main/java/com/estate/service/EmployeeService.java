package com.estate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estate.entity.Employee;
import com.estate.repository.EmployeeRepository;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepo;

    // list all
    public List<Employee> getListEmployee() {
        return employeeRepo.findAll();
    }

    public Optional<Employee> getEmployeeById(long id) {
        return employeeRepo.findById(id);
    }

    // tạo mới employee
    public Employee createEmployee(Employee employee) throws Exception {
        Employee newEmployee = new Employee();
        newEmployee.setFirstName(employee.getFirstName());
        newEmployee.setLastName(employee.getLastName());
        newEmployee.setTitle(employee.getTitle());
        newEmployee.setTitleOfCourtesy(employee.getTitleOfCourtesy());
        newEmployee.setBirthday(employee.getBirthday());
        newEmployee.setHireDate(employee.getHireDate());
        newEmployee.setAddress(employee.getAddress());
        newEmployee.setCity(employee.getCity());
        newEmployee.setRegion(employee.getRegion());
        newEmployee.setPostalCode(employee.getPostalCode());
        newEmployee.setCountry(employee.getCountry());
        newEmployee.setHomePhone(employee.getHomePhone());
        newEmployee.setPhoto(employee.getPhoto());
        newEmployee.setNotes(employee.getNotes());
        newEmployee.setReportTo(employee.getReportTo());
        newEmployee.setUserName(employee.getUserName());
        newEmployee.setPassword(employee.getPassword());
        newEmployee.setEmail(employee.getEmail());
        newEmployee.setActivated(employee.getActivated());
        newEmployee.setProfile(employee.getProfile());
        newEmployee.setUserLevel(employee.getUserLevel());
        return employeeRepo.save(newEmployee);
    }

    // update address map
    public Employee updateEmployeeById(long id, Employee employee) throws Exception {
        Optional<Employee> employeeData = employeeRepo.findById(id);
        if (employeeData.isPresent()) {
            Employee existEmployee = employeeData.get();
            existEmployee.setFirstName(employee.getFirstName());
            existEmployee.setLastName(employee.getLastName());
            existEmployee.setTitle(employee.getTitle());
            existEmployee.setTitleOfCourtesy(employee.getTitleOfCourtesy());
            existEmployee.setBirthday(employee.getBirthday());
            existEmployee.setHireDate(employee.getHireDate());
            existEmployee.setAddress(employee.getAddress());
            existEmployee.setCity(employee.getCity());
            existEmployee.setRegion(employee.getRegion());
            existEmployee.setPostalCode(employee.getPostalCode());
            existEmployee.setCountry(employee.getCountry());
            existEmployee.setHomePhone(employee.getHomePhone());
            existEmployee.setPhoto(employee.getPhoto());
            existEmployee.setNotes(employee.getNotes());
            existEmployee.setReportTo(employee.getReportTo());
            existEmployee.setUserName(employee.getUserName());
            existEmployee.setPassword(employee.getPassword());
            existEmployee.setEmail(employee.getEmail());
            existEmployee.setActivated(employee.getActivated());
            existEmployee.setProfile(employee.getProfile());
            existEmployee.setUserLevel(employee.getUserLevel());
            return employeeRepo.save(existEmployee);
        } else {
            throw new Exception("Employee map not found");
        }
    }

    // check if country exist
    public boolean deleteEmployeeById(long id) {
        if (employeeRepo.existsById(id)) {
            employeeRepo.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
