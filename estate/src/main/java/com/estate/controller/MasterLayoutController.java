package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.MasterLayout;
import com.estate.service.MasterLayoutService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class MasterLayoutController {

    @Autowired
    private MasterLayoutService masterLayoutSer;

    // lấy danh sách tất cả
    @GetMapping("/masterLayouts")
    public ResponseEntity<List<MasterLayout>> getAllMasterLayout() {
        try {
            return new ResponseEntity<>(masterLayoutSer.getListMasterLayout(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/masterLayouts/{id}")
    public ResponseEntity<MasterLayout> getMasterLayoutDetail(@PathVariable("id") long id) {
        try {
            Optional<MasterLayout> existMasterLayout = masterLayoutSer.getMasterLayoutById(id);
            if (existMasterLayout.isPresent()) {
                return new ResponseEntity<>(existMasterLayout.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/masterLayouts/create/{id}")
    public ResponseEntity<Object> createMasterLayout(@RequestBody MasterLayout masterLayout,
            @PathVariable("id") long projectId) {
        try {
            MasterLayout newMasterLayout = masterLayoutSer.createMasterLayout(masterLayout, projectId);
            return new ResponseEntity<>(newMasterLayout, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/masterLayouts/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateMasterLayout(@PathVariable("id") long id,
            @RequestBody MasterLayout masterLayout) {
        try {
            MasterLayout updatedMasterLayout = masterLayoutSer.updateMasterLayoutById(id, masterLayout);
            return new ResponseEntity<>(updatedMasterLayout, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/masterLayouts/delete/{id}")
    public ResponseEntity<MasterLayout> deleteMasterLayout(@PathVariable("id") long id) {
        try {
            if (masterLayoutSer.deleteMasterLayoutById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
