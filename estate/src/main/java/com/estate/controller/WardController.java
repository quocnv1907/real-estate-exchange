package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.Ward;
import com.estate.service.WardService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class WardController {

    @Autowired
    private WardService wardSer;

    // GET list
    @GetMapping("/wards")
    public ResponseEntity<List<Ward>> getAllWard() {
        try {
            return new ResponseEntity<>(wardSer.getListWard(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET employee by ID
    @GetMapping("/wards/{id}")
    public ResponseEntity<Ward> getWardById(@PathVariable("id") long id) {
        try {
            Optional<Ward> ward = wardSer.getWardById(id);
            if (ward.isPresent()) {
                return new ResponseEntity<>(ward.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE new ward by department ID
    @PostMapping("/wards/create/{districtId}")
    public ResponseEntity<Object> createWard(@RequestBody Ward ward, @PathVariable("districtId") long districtId) {
        try {
            Ward createdWard = wardSer.createNewWard(ward, districtId);
            return new ResponseEntity<>(createdWard, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    // update
    @PutMapping("/wards/update/{id}")
    public ResponseEntity<Object> updateWard(@PathVariable("id") long id, @RequestBody Ward ward) {
        try {
            Ward updatedWard = wardSer.updateWardById(id, ward);
            return new ResponseEntity<>(updatedWard, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE BY ID
    @DeleteMapping("/wards/delete/{id}")
    public ResponseEntity<Ward> deleteWard(@PathVariable("id") long id) {
        try {
            if (wardSer.deleteWardById(id)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
