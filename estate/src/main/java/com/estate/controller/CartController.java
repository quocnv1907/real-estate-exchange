package com.estate.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/cart")
public class CartController {
     @PostMapping("/add")
    public ResponseEntity<String> addToCart(@RequestParam long customerId, @RequestParam long realEstateId) {
        // Fetch customer and real estate from database
        // Invoke service method to add to cart
        // Return success message or handle errors
        return null;
    }

    @PostMapping("/remove")
    public ResponseEntity<String> removeFromCart(@RequestParam long customerId, @RequestParam long realEstateId) {
        // Fetch customer and real estate from database
        // Invoke service method to remove from cart
        // Return success message or handle errors
        return null;
    }

    @DeleteMapping("/{cartItemId}")
    public ResponseEntity<String> deleteCartItem(@PathVariable("cartItemId") long cartItemId) {
        // Fetch cart item from database
        // Invoke service method to delete cart item
        // Return success message or handle errors
        return null;
    }

    // Add additional endpoints as needed
    
}

