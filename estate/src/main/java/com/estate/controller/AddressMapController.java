package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.AddressMap;
import com.estate.service.AddressMapService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AddressMapController {

    @Autowired
    private AddressMapService addressMapSer;

    // lấy danh sách tất cả address map
    @GetMapping("/addressMaps")
    public ResponseEntity<List<AddressMap>> getAllAddressMap() {
        try {
            return new ResponseEntity<>(addressMapSer.getListAddressMap(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/addressMaps/{id}")
    public ResponseEntity<AddressMap> getAddressMapDetail(@PathVariable("id") long id) {
        try {
            Optional<AddressMap> existAddressMap = addressMapSer.getAddressMapById(id);
            if (existAddressMap.isPresent()) {
                return new ResponseEntity<>(existAddressMap.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/addressMaps/create")
    public ResponseEntity<Object> createAddressMap(@RequestBody AddressMap addressMap) {
        try {
            AddressMap newAddressMap = addressMapSer.createAddressMap(addressMap);
            return new ResponseEntity<>(newAddressMap, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/addressMaps/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateAddressMap(@PathVariable("id") long id, @RequestBody AddressMap addressMap) {
        try {
            AddressMap updatedAddressMap = addressMapSer.updateAddressMapById(id, addressMap);
            return new ResponseEntity<>(updatedAddressMap, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/addressMaps/delete/{id}")
    public ResponseEntity<AddressMap> deleteAddressMap(@PathVariable("id") long id) {
        try {
            if (addressMapSer.deleteAddressMapById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
