package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.Constructor;
import com.estate.service.ConstructorService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ConstructorController {

    @Autowired
    private ConstructorService constructorSer;

    @GetMapping("/constructors")
    public ResponseEntity<List<Constructor>> getAllConstructor() {
        try {
            return new ResponseEntity<>(constructorSer.getListConstructor(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/constructors/{id}")
    public ResponseEntity<Constructor> getConstructorDetail(@PathVariable("id") long id) {
        try {
            Optional<Constructor> existConstrutor = constructorSer.getConstructorById(id);
            if (existConstrutor.isPresent()) {
                return new ResponseEntity<>(existConstrutor.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/constructors/create")
    public ResponseEntity<Object> createConstructor(@RequestBody Constructor constructor) {
        try {
            Constructor newConstructor = constructorSer.createNewConstructor(constructor);
            return new ResponseEntity<>(newConstructor, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/constructors/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateConstructor(@PathVariable("id") long id, @RequestBody Constructor constructor) {
        try {
            Constructor updatedConstructor = constructorSer.updateConstructorById(id, constructor);
            return new ResponseEntity<>(updatedConstructor, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/constructors/delete/{id}")
    public ResponseEntity<Constructor> deleteConstrutor(@PathVariable("id") long id) {
        try {
            if (constructorSer.deleteConstructorById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
