package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.District;
import com.estate.entity.Province;
import com.estate.service.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ProvinceController {

    @Autowired
    private ProvinceService provinceSer;

    // lấy danh sách tất cả address map
    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvines() {
        try {
            return new ResponseEntity<>(provinceSer.getListProvince(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> getProvinceDetail(@PathVariable("id") long id) {
        try {
            Optional<Province> existProvince = provinceSer.getProvinceById(id);
            if (existProvince.isPresent()) {
                return new ResponseEntity<>(existProvince.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/provinces/create")
    public ResponseEntity<Object> createProvince(@RequestBody Province province) {
        try {
            Province newProvince = provinceSer.createProvince(province);
            return new ResponseEntity<>(newProvince, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/provinces/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateProvince(@PathVariable("id") long id, @RequestBody Province province) {
        try {
            Province updatedProvince = provinceSer.updateProvinceById(id, province);
            return ResponseEntity.ok(updatedProvince);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/provinces/delete/{id}")
    public ResponseEntity<Province> deleteProvince(@PathVariable("id") long id) {
        try {
            if (provinceSer.deleteProvinceById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     @GetMapping("/districts/{provinceId}")
    public ResponseEntity<List<District>> getDistrictByProvinceId(@PathVariable(value = "provinceId") long provinceId) {
        try {
            return new ResponseEntity<>(provinceSer.getDistrictByProvinceId(provinceId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
