package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.Location;
import com.estate.service.LocationService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class LocationController {

    @Autowired
    private LocationService locationSer;

    // lấy danh sách tất cả address map
    @GetMapping("/locations")
    public ResponseEntity<List<Location>> getAllLocation() {
        try {
            return new ResponseEntity<>(locationSer.getListLocation(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/locations/{id}")
    public ResponseEntity<Location> getLocationDetail(@PathVariable("id") long id) {
        try {
            Optional<Location> existLocation = locationSer.getLocationById(id);
            if (existLocation.isPresent()) {
                return new ResponseEntity<>(existLocation.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/locations/create")
    public ResponseEntity<Object> createLocation(@RequestBody Location location) {
        try {
            Location newLocation = locationSer.createLocation(location);
            return new ResponseEntity<>(newLocation, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/locations/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateLocation(@PathVariable("id") long id, @RequestBody Location location) {
        try {
            Location updatedLocation = locationSer.updateLocationById(id, location);
            return new ResponseEntity<>(updatedLocation, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/locations/delete/{id}")
    public ResponseEntity<Location> deleteLocation(@PathVariable("id") long id) {
        try {
            if (locationSer.deleteLocationById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
