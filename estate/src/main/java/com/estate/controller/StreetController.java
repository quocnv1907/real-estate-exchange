package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.Street;
import com.estate.service.StreetService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class StreetController {
    @Autowired
    private StreetService streetSer;

    // GET list
    @GetMapping("/streets")
    public ResponseEntity<List<Street>> getAllStreets() {
        try {
            return new ResponseEntity<>(streetSer.getListStreet(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET employee by ID
    @GetMapping("/streets/{id}")
    public ResponseEntity<Street> getStreetDetail(@PathVariable("id") long id) {
        try {
            Optional<Street> streetDetail = streetSer.getStreetById(id);
            if (streetDetail.isPresent()) {
                return new ResponseEntity<>(streetDetail.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE new employee by department ID
    @PostMapping("/streets/create/{provinceId}/{districtId}")
    public ResponseEntity<Object> createStreet(@RequestBody Street street, @PathVariable("provinceId") long provinceId,
            @PathVariable("districtId") long districtId) {
        try {
            Street createdStreet = streetSer.createStreet(street, provinceId, districtId);
            return new ResponseEntity<>(createdStreet, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // update
    @PutMapping("/streets/update/{id}")
    public ResponseEntity<Street> updateStreet(@PathVariable("id") long id, @RequestBody Street street) {
        try {
            Street updatedStreet = streetSer.updateStreetById(id, street);
            return new ResponseEntity<>(updatedStreet, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE BY ID
    @DeleteMapping("/streets/delete/{id}")
    public ResponseEntity<Street> deleteStreet(@PathVariable("id") long id) {
        try {
            if (streetSer.deleteStreetById(id)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streetByProvinceDistrict/{provinceId}/{districtId}")
    public ResponseEntity<?> getStreetsByProvinceDistrict(@PathVariable("provinceId") long provinceId,
            @PathVariable("districtId") long districtId) {
        try {
            List<Street> streets = streetSer.getListStreetByProvinceAndDistrict(provinceId, districtId);
            return new ResponseEntity<>(streets, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
