package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.District;
import com.estate.entity.Ward;
import com.estate.service.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DistrictController {
    @Autowired
    DistrictService districtSer;

    // read
    @GetMapping("/districts")
    public ResponseEntity<List<District>> getAllDistrict() {
        try {
            return new ResponseEntity<>(districtSer.getDistricts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get detail district by id
    @GetMapping("/districts-detail/{id}")
    public ResponseEntity<District> getDistrictDetailProvince(@PathVariable("id") long id) {
        try {
            Optional<District> district = districtSer.getDistrictById(id);
            if (district.isPresent()) {
                return new ResponseEntity<>(district.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create
    @PostMapping("/district-create/{id}")
    public ResponseEntity<Object> createNewDistrict(@PathVariable("id") long provinceId,
            @RequestBody District district) {
        try {
            District newDistrict = districtSer.createDistrict(district, provinceId);
            return new ResponseEntity<>(newDistrict, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    // update
    @PutMapping("/district-update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateDistrict(@PathVariable("id") long id, @RequestBody District district) {
        try {
            District updatedDistrict = districtSer.updateDistrictById(id, district);
            return new ResponseEntity<>(updatedDistrict, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    // delete
    @DeleteMapping("/district-delete/{id}") // Dùng phương thức DELETE
    public ResponseEntity<District> deleteUserById(@PathVariable("id") long id) {
        if (districtSer.deleteDistrictrById(id)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/wards-by-district/{districtId}")
    public ResponseEntity<List<Ward>> getWardByDistrict(@PathVariable(value = "districtId") long districtId) {
        try {
            return new ResponseEntity<>(districtSer.getWardByDistrictId(districtId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
