package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.RegionLink;
import com.estate.service.RegionLinkService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RegionLinkController {
    
    @Autowired
    private RegionLinkService regionLinkSer;

    // GET list
    @GetMapping("/regionLinks")
    public ResponseEntity<List<RegionLink>> getAllRegionLink() {
        try {
            return new ResponseEntity<>(regionLinkSer.getListRegionLink(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET employee by ID
    @GetMapping("/regionLinks/{id}")
    public ResponseEntity<RegionLink> getRegionLinkDetail(@PathVariable("id") long id) {
        try {
            Optional<RegionLink> regionLinkDetail = regionLinkSer.getRegionLinkById(id);
            if (regionLinkDetail.isPresent()) {
                return new ResponseEntity<>(regionLinkDetail.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE new employee by department ID
    @PostMapping("/regionLinks/create/{id}")
    public ResponseEntity<Object> createRegionLink(@RequestBody RegionLink regionLink, @PathVariable("id") long projectId) {
        try {
            RegionLink createdRegionLink = regionLinkSer.createRegionLink(regionLink, projectId);
            return new ResponseEntity<>(createdRegionLink, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // update
    @PutMapping("/regionLinks/update/{id}")
    public ResponseEntity<Object> updateRegionLink(@PathVariable("id") long id, @RequestBody RegionLink regionLink) {
        try {
            RegionLink updatedRegionLink = regionLinkSer.updateRegionLinkById(id, regionLink);
            return new ResponseEntity<>(updatedRegionLink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE BY ID
    @DeleteMapping("/regionLinks/delete/{id}")
    public ResponseEntity<RegionLink> deleteRegionLink(@PathVariable("id") long id) {
        try {
            if (regionLinkSer.deleteARegionLinkById(id)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
