package com.estate.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.estate.entity.EstatePhotoAlbum;
import com.estate.service.EstatePhotoService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class EstateImageController {

    @Autowired
    private EstatePhotoService estatePhotoSer;

    @GetMapping("/all_estate_images")
    public ResponseEntity<List<EstatePhotoAlbum>> getAllEstateImage() {
        try {
            return new ResponseEntity<>(estatePhotoSer.getAllEstateImage(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lấy danh sách tất cả 
    // @GetMapping("/albums")
    // public ResponseEntity<List<EstatePhotoAlbum>> getAllAlbum() {
    //     try {
    //         return new ResponseEntity<>(estatePhotoSer.getListAlbum(), HttpStatus.OK);
    //     } catch (Exception e) {
    //         return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    //     }
    // }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/albums/{id}")
    public ResponseEntity<EstatePhotoAlbum> getAlbumDetail(@PathVariable("id") long id) {
        try {
            Optional<EstatePhotoAlbum> existAlbum = estatePhotoSer.getAlbumById(id);
            if (existAlbum.isPresent()) {
                return new ResponseEntity<>(existAlbum.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/albums-create/{estateId}")
    public ResponseEntity<Object> createAlbum(@RequestBody EstatePhotoAlbum album,
            @PathVariable("estateId") long estateId) {
        try {
            EstatePhotoAlbum newAlbum = estatePhotoSer.createAlbum(album, estateId);
            return new ResponseEntity<>(newAlbum, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/albums/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateAlbum(@PathVariable("id") long id, @RequestBody EstatePhotoAlbum album) {
        try {
            EstatePhotoAlbum updatedAlbum = estatePhotoSer.updateAlbumById(id, album);
            return new ResponseEntity<>(updatedAlbum, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/albums/delete/{id}")
    public ResponseEntity<EstatePhotoAlbum> deleteAlbumEstate(@PathVariable("id") long id) {
        try {
            if (estatePhotoSer.deleteAlbumById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
