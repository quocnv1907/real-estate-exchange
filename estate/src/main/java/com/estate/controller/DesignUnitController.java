package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.DesignUnit;
import com.estate.service.DesignUnitService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DesignUnitController {

    @Autowired
    private DesignUnitService designUnitSer;

    @GetMapping("/designUnits")
    public ResponseEntity<List<DesignUnit>> getAllDesignUnit() {
        try {
            return new ResponseEntity<>(designUnitSer.getListDesignUnit(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin department theo {id}
    @GetMapping("/designUnits/{id}")
    public ResponseEntity<DesignUnit> getDesignUnitById(@PathVariable("id") long id) {
        try {
            Optional<DesignUnit> designUnit = designUnitSer.getDesignUnitById(id);
            if (designUnit.isPresent()) {
                return new ResponseEntity<>(designUnit.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/designUnits/create")
    public ResponseEntity<Object> createCustomer(@RequestBody DesignUnit designUnit) {
        try {
            DesignUnit newDesignUnit = designUnitSer.createNewDesignUnit(designUnit);
            return new ResponseEntity<>(newDesignUnit, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/designUnits/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateDesignUnit(@PathVariable("id") long id, @RequestBody DesignUnit designUnit) {
        try {
            DesignUnit updatedDesignUnit = designUnitSer.updateDesignUnitById(id, designUnit);
            return new ResponseEntity<>(updatedDesignUnit, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/designUnits/delete/{id}")
    public ResponseEntity<DesignUnit> deleteDesignUnit(@PathVariable("id") long id) {
        try {
            if (designUnitSer.deleteDesignUnitById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
