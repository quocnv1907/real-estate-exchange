package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.RealEstate;
import com.estate.service.RealEstateService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RealEstateController {

    @Autowired
    private RealEstateService estateService;

    @GetMapping("/all_estates")
    public List<RealEstate> getAllRealEstateDetails() {
        return estateService.getAllRealEstate();
    }

@GetMapping("/estates/filter")
public Page<RealEstate> getFilteredRealEstateWithPagination(
        @RequestParam(required = false) String estateName,
        @RequestParam(required = false) String province,
        @RequestParam(required = false) String estateType1,
        @RequestParam(required = false) String estateType2,
        @RequestParam(required = false) String estateType3,
        @RequestParam(required = false) Integer minPrice,
        @RequestParam(required = false) Integer maxPrice,
        @RequestParam(required = false) Double minArea,
        @RequestParam(required = false) Double maxArea,
        @RequestParam(defaultValue = "0") int page,
        @RequestParam(defaultValue = "1000") int size) {
    try {
        return estateService.getAllRealEstateFilteredWithPagination(
                estateName, province, estateType1, estateType2, estateType3,
                minPrice, maxPrice, minArea, maxArea, page, size);
    } catch (Exception e) {
        System.err.println("Error fetching filtered real estate with pagination: " + e.getMessage());
        return Page.empty();
    }
}


    // Lấy thông tin chi tiết theo ID - bao gồm album
    @GetMapping("/estate_detail/{estateId}")
    public RealEstate getRealEstateDetail(@PathVariable long estateId) {
        try {
            return Optional.ofNullable(estateService.getRealEstateDetail(estateId))
                    .orElseThrow(() -> new Exception("Estate not found"));
        } catch (Exception e) {
            System.err.println("Error fetching real estate details: " + e.getMessage());
            return null;
        }
    }

    // CREATE
    @PostMapping("/estates/create")
    public ResponseEntity<Object> createEstate(@RequestBody RealEstate estate) {
        try {
            RealEstate newEstate = estateService.createEstate(estate);
            return new ResponseEntity<>(newEstate, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id}
    @PutMapping("/estates/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateEstate(@PathVariable("id") long id, @RequestBody RealEstate estate) {
        try {
            RealEstate updatedEstate = estateService.updateEstateById(id, estate);
            return new ResponseEntity<>(updatedEstate, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete theo {id}
    @DeleteMapping("/estates_delete/{id}")
    public ResponseEntity<RealEstate> deleteEstate(@PathVariable("id") long id) {
        try {
            if (estateService.deleteEstateById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lấy ds bất động sản theo tỉnh thành phố
    @GetMapping("/estates/by-province")
    public List<RealEstate> getEstatesByProvinceName(@RequestParam String provinceName) {
        try {
            return estateService.getEstateByProvinceName(provinceName);
        } catch (Exception e) {
            return List.of();
        }
    }

}
