package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.Investor;
import com.estate.service.InvestorService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InvestorController {

    @Autowired
    private InvestorService investorSer;

    // lấy danh sách tất cả investor
    @GetMapping("/investors")
    public ResponseEntity<List<Investor>> getAllInvestor() {
        try {
            return new ResponseEntity<>(investorSer.getListInvestor(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/investors/{id}")
    public ResponseEntity<Investor> getInvestorDetail(@PathVariable("id") long id) {
        try {
            Optional<Investor> existInvestor = investorSer.getInvestorById(id);
            if (existInvestor.isPresent()) {
                return new ResponseEntity<>(existInvestor.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/investors/create")
    public ResponseEntity<Object> createInvestor(@RequestBody Investor investor) {
        try {
            Investor newInvestor = investorSer.createInvestor(investor);
            return new ResponseEntity<>(newInvestor, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/investors/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateAddressMap(@PathVariable("id") long id, @RequestBody Investor investor) {
        try {
            Investor updatedAddressMap = investorSer.updateInvestorById(id, investor);
            return new ResponseEntity<>(updatedAddressMap, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/investors/delete/{id}")
    public ResponseEntity<Investor> deleteAddressMap(@PathVariable("id") long id) {
        try {
            if (investorSer.deleteInvestorById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
