package com.estate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estate.entity.Project;
import com.estate.service.ProjectService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/projects_without_image")
    public List<Project> getProjectDetails() {
        return projectService.getListProjectWithoutImage();
    }

    // lấy danh sách tất cả bao gồm album
    @GetMapping("/projects")
    public ResponseEntity<List<Project>> getAllProjects() {
        try {
            return new ResponseEntity<>(projectService.getAllProject(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin chi tiết theo {id}
    @GetMapping("/projects/{id}")
    public ResponseEntity<Project> getProjectDetail(@PathVariable("id") long id) {
        try {
            Optional<Project> existProject = projectService.getProjectById(id);
            if (existProject.isPresent()) {
                return new ResponseEntity<>(existProject.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // CREATE
    @PostMapping("/projects-create")
    public ResponseEntity<Object> createProject(@RequestBody Project project) {
        try {
            Project newProject = projectService.createProject(project);
            return new ResponseEntity<>(newProject, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // UPDATE theo {id} dùng service, sử dụng phương thức PUT
    @PutMapping("/projects/update/{id}")

    public ResponseEntity<Object> updateProject(@PathVariable("id") long id, @RequestBody Project project) {
        try {
            Project updatedProject = projectService.updateProjectById(id, project);
            return new ResponseEntity<>(updatedProject, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // Xoá/delete phòng theo {id}
    @DeleteMapping("/projects/delete/{id}")
    public ResponseEntity<Project> deleteProject(@PathVariable("id") long id) {
        try {
            if (projectService.deleteProjectById(id)) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
