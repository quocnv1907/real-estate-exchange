package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.Province;

@Repository
public interface ProvinceRepository  extends JpaRepository<Province, Long>{

}
