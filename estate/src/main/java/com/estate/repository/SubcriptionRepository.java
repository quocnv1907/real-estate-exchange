package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.Subscription;

@Repository
public interface SubcriptionRepository extends JpaRepository<Subscription, Long> {
}
