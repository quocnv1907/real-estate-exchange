package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.Investor;

@Repository
public interface InvestorRepository extends JpaRepository<Investor, Long> {

}
