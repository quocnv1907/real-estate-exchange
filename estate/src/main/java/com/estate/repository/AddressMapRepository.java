package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.AddressMap;

@Repository
public interface AddressMapRepository extends JpaRepository<AddressMap, Long> {

}
