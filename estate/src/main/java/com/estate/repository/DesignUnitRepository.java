package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.DesignUnit;

@Repository
public interface DesignUnitRepository extends JpaRepository<DesignUnit, Long> {

}
