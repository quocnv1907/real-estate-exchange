package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.Ward;

@Repository
public interface WardRepository extends JpaRepository<Ward, Long> {

}
