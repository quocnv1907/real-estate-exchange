package com.estate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.estate.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query(value = "SELECT p.*, pr.name AS provinceName, d.name AS districtName, " +
            "w.name AS wardName, s.name AS streetName, i.name AS investorName, " +
            "c.name AS constructorName, de.name AS designerName " +
            "FROM project p " +
            "JOIN provinces pr ON p.province_id = pr.id " +
            "JOIN districts d ON p.district_id = d.id " +
            "JOIN ward w ON p.ward_id = w.id " +
            "JOIN street s ON p.street_id = s.id " +
            "JOIN investor i ON p.investor_id = i.id " +
            "JOIN constructor c ON p.constructor_id = c.id " +
            "JOIN design_unit de ON p.designer_id = de.id", nativeQuery = true)
    List<Object[]> findListProjectWithoutImage();

    @Query(value = "SELECT p.*, pr.name AS provinceName, d.name AS districtName, " +
            "w.name AS wardName, s.name AS streetName, i.name AS investorName, " +
            "c.name AS constructorName, de.name AS designerName " +
            "pp.image_url_main, pp.image_url_1, pp.image_url_2, pp.image_url_3, pp.image_url_4 " +
            "FROM project p " +
            "JOIN provinces pr ON p.province_id = pr.id " +
            "JOIN districts d ON p.district_id = d.id " +
            "JOIN ward w ON p.ward_id = w.id " +
            "JOIN street s ON p.street_id = s.id " +
            "JOIN investor i ON p.investor_id = i.id " +
            "JOIN constructor c ON p.constructor_id = c.id " +
            "JOIN design_unit de ON p.designer_id = de.id " +
            "JOIN project_photo pp ON pp.project_id = p.id", nativeQuery = true)
    List<Object[]> findAllProject();

}
