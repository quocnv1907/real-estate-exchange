package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.Constructor;

@Repository
public interface ConstructorRepository extends JpaRepository<Constructor, Long> {

}
