package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.RegionLink;

@Repository
public interface RegionLinkRepository extends JpaRepository<RegionLink, Long>{
}
