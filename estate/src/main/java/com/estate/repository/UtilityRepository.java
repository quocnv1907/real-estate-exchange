package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.Utility;

@Repository
public interface UtilityRepository extends JpaRepository<Utility, Long> {

}
