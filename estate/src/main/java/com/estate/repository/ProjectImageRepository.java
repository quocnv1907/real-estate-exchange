package com.estate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.estate.entity.ProjectPhotoAlbum;

public interface ProjectImageRepository extends JpaRepository<ProjectPhotoAlbum, Long>{
    @Query(value = "SELECT pp.id, pp.image_url_main, pp.image_url_1, pp.image_url_2, pp.image_url_3, pp.image_url_4, pr.name as project_name "
            +
            "FROM project_photo pp " +
            "JOIN project pr ON pp.project_id = pr.id", nativeQuery = true)
    List<Object[]> findAllProjectImage();
}
