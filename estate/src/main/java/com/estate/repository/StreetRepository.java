package com.estate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.estate.entity.Street;

@Repository
public interface StreetRepository extends JpaRepository<Street, Long> {
    @Query(value = "select * from street where province_id LIKE :provinceId and district_id LIKE :districtId", nativeQuery = true)
    List<Street> findStreetByProvinceDistrict(@Param("provinceId") long provinceId, @Param("districtId") long districtId );

}
