package com.estate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.estate.entity.EstatePhotoAlbum;

@Repository
public interface EstatePhotoRepository extends JpaRepository<EstatePhotoAlbum, Long> {
    @Query(value = "SELECT ep.id, ep.image_url_main, ep.image_url_1, ep.image_url_2, ep.image_url_3, ep.image_url_4, re.title as estate_name "
            +
            "FROM estate_photo ep " +
            "JOIN real_estate re ON ep.estate_id = re.id", nativeQuery = true)
    List<Object[]> findAllEstateImage();
}
