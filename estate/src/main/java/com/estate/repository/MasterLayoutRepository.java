package com.estate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.estate.entity.MasterLayout;

@Repository
public interface MasterLayoutRepository extends JpaRepository<MasterLayout, Long> {

}
