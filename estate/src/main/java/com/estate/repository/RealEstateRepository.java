package com.estate.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.estate.entity.RealEstate;

@Repository
public interface RealEstateRepository extends JpaRepository<RealEstate, Long> {
        @Query(value = "SELECT re.*, pr.name AS provinceName, d.name AS districtName, " +
                        "w.name AS wardName, s.name AS streetName, i.name AS investorName, " +
                        "c.name AS constructorName, de.name AS designerName " +
                        "FROM real_estate re " +
                        "LEFT JOIN provinces pr ON re.province_id = pr.id " +
                        "LEFT JOIN districts d ON re.district_id = d.id " +
                        "LEFT JOIN ward w ON re.ward_id = w.id " +
                        "LEFT JOIN street s ON re.street_id = s.id " +
                        "LEFT JOIN investor i ON re.investor_id = i.id " +
                        "LEFT JOIN constructor c ON re.constructor_id = c.id " +
                        "LEFT JOIN design_unit de ON re.designer_id = de.id;", nativeQuery = true)
        List<Object[]> findAllRealEstate();

}
