package com.estate;

import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@SpringBootApplication
public class EstateApplication {

        public static void main(String[] args) throws IOException {
                SpringApplication.run(EstateApplication.class, args);
                initializeFirebase();
        }

        private static void initializeFirebase() throws IOException {
                FileInputStream serviceAccount = new FileInputStream("src/main/resources/google-services.json");
                FirebaseOptions options = FirebaseOptions.builder()
                                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                                .setStorageBucket("real-estate-bd8a4.appspot.com")
                                .build();

                if (FirebaseApp.getApps().isEmpty()) {
                        FirebaseApp.initializeApp(options);
                }
        }

}
