var estateId
//var inputEstateName;
var selectedProvince;
var estateType = [];
var provinceId;


const $priceSlider = $('#price-slider');

// Lấy giá trị từ data-slider-value
// const sliderValue = $priceSlider.data('slider-value'); // Sử dụng .data() để lấy giá trị

// // Chuyển chuỗi thành mảng
// const [minValue, maxValue] = JSON.parse(sliderValue);

// // In ra kết quả
// console.log('Min value:', minValue);
// console.log('Max value:', maxValue);

var gURL = "http://localhost:8080/api/estates/filter";

$(document).ready(function () {
 // getInputSearchFromUrl();
  //getEstateByProvinceName();
  //loadListEstate();
  callApiLoadListResult();
});


//lấy giá trị province
$("#province-dropdown").on('click', '.form-check-input', function () {
  if ($(this).is(':checked')) {
    selectedProvince = $(this).val();
    console.log("tỉnh đã chọn:" + selectedProvince);
  }
});


$(document).on("click", "#btn-estate-by-province", function () {
  var provinceName = $(this).data("name");
  window.location.href = "search-result.html" + "?province=" + provinceName;
})


//nhấn nút tìm kiếm
$("#search-button").on("click", function () {
  var estateName = $("#input-search-estate").find(":selected").text();
  //gắn từ khóa tìm kiếm vào link url
  window.location.href = "search-result.html" + "?estateName=" + estateName
    + "&provinceName=" + selectedProvince
    + "&estateType1=" + estateType[0]
    + "&estateType2=" + estateType[1]
    + "&estateType3=" + estateType[2]
    + "&minPrice="
});


// Đối tượng chứa các tham số lọc và phân trang


function callApiLoadListResult() {
  const params = getInputSearchFromUrl();
  $.ajax({
    type: 'GET',
    url: gURL,
    dataType: 'json',
    data: params,
    success: function (data) {
      displayResultOnWeb(data.content);
      console.log(data.content);
      $("#count-result").text(data.content.length);
    },
    error: function (error) {
      console.log(error);
    }
  });
}

function displayResultOnWeb(estates) {
  estates.forEach(function (estate) {
    var truncatedDescription = estate.description.slice(0, 50) + (estate.description.length > 100 ? "..." : "");
    var priceDisplay = {
      "Cho thuê": `${estate.price} triệu/tháng`, "Mở bán": `${estate.price} tỉ`
    }[estate.status] || estate.price;

    // Hiển thị chuỗi mô tả giới hạn lên HTML
    $('#result-description').text(truncatedDescription);
    $("#estate-result-container").append(
      `<div class="row result-item shadow me-auto mb-4">
         <div class="col-md-5 bg-container p-0">
             <a href="#" class="btn-detail-estate" data-id="${estate.id}"><img class="profile" src="${estate.imageUrlMain}" height="100%"></a>
          </div>
          <div class="col-md-7">
            <div class="bg-white pt-3 px-3 testimonials">
              <a href="#" class="btn-detail-estate" data-id="${estate.id}"><p class="result-estate-name">${estate.title}</p></a>
              <a href="#" class="btn-detail-estate" data-id="${estate.id}"><p class="result-estate-description" id="result-description">${truncatedDescription}&nbsp;<br></p></a>
              <!--row detail -->
              <a href="#" class="btn-detail-estate" data-id="${estate.id}"><div class="d-flex justify-content-between">
                <div class="d-flex flex-column align-items-center">
                  <span class="font-weight-bold percentage text-danger"><img src="icon/price.png" /></span>
                  <span id="result-price">${priceDisplay}</span>
                </div>
                <div class="d-flex flex-column align-items-center">
                  <span class="font-weight-bold percentage text-success"><img src="icon/area.png" /></span>
                  <span id="result-area">${estate.width * estate.length} m<sup>2</sup></span>
                </div>
                <div class="d-flex flex-column align-items-center">
                  <span class="font-weight-bold percentage text-"><img src="icon/address.png"></span>
                  <span>${estate.provinceName}</span>
                </div>
                <div class="d-flex flex-column align-items-center">
                  <span class="font-weight-bold percentage text-"><img src="icon/bed.png"></span>
                  <span>${estate.bedroom}</span>
                </div>
                <div class="d-flex flex-column align-items-center">
                  <span class="font-weight-bold percentage text-"><img src="icon/toilet.png"></span>
                  <span>${estate.bathroom}</span>
                </div>
              </div>
              </a>
              <!--broker info -->
              <div class="d-flex justify-content-between align-items-center mt-4">
                <div class="d-flex flex-row align-items-center">
                  <img src="img/binhduong.jpg" width="30" height="30" class="rounded-circle" id="image-author">
                  <div class="d-flex flex-column">
                    <span id="author-name">Nguyễn Văn Tuấn</span>
                    <span class="" id="create-date">20/4/2023</span>
                  </div>
                </div>
                <div class="d-flex flex-column align-items-center">
                  <span class="btn btn-white px-2 py-1" id="btn-show-phone">Hiện số điện thoại</span>
                </div>
                <div class="d-flex flex-column align-items-center">
                  <button class="icon-save btn shadow p-1" id="btn-save" data-mdb-ripple-init data-mdb-tooltip-init data-mdb-tooltip="true" data-mdb-placement="bottom" title="Bấm để lưu tin"><img src="icon/save.png"></button>
                </div>
              </div>
            </div>
          </div>
        </div>`
    );
    $('#estate-result-container .icon-save').each(function () {
      new mdb.Tooltip(this);
    });
  });
}


function callApiGetEstateByProvinceName(provinceName) {
  $.ajax({
    url: 'http://localhost:8080/api/estates/by-province?provinceName=' + provinceName,
    method: "GET",
    success: function (data) {
      displayResultOnWeb(data);
      $("#count-result").text(data.length);
    },
    error: (pXhrObj) => console.log(pXhrObj)
  });
}


function getInputSearchFromUrl() {
  const params = new URLSearchParams(window.location.search);
  return {
    estateName: params.get('estateName'),
    province: params.get('provinceName'),
    page: 0,
    size: 3
  }
}


function getEstateByProvinceName() {
  var provinceName = new URLSearchParams(window.location.search).get('province');
  if (provinceName) {
    callApiGetEstateByProvinceName(provinceName);
  }
}