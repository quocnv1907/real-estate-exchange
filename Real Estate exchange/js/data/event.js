$(document).on("change", "#select-search-status", function () {
    $(".filter-price-rent").toggleClass("d-none", $(this).val() !== "Cho thuê");
    $(".filter-price").toggleClass("d-none", $(this).val() === "Cho thuê");
    $('#price-dropdown .form-check-input').prop('checked', false);
    $('#dropdownMenuButton-price').html("Khoảng giá");
})
