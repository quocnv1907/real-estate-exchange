var estateId;


$(document).ready(function () {
    loadListProvince();
    loadListEstate();
    //lấy ID từ URL
    estateId = new URLSearchParams(window.location.search).get('id');
    if (estateId !== null) {
        callApiGetEstateDetailById(estateId);
    }
});

//chuyển hướng khi click vào trang chi tiết
$(document).on("click", ".btn-detail-estate", function (event) {
    event.preventDefault();
    estateId = $(this).data("id");
    window.location.href = "estate-detail.html" + "?id=" + estateId;
});


function loadListProvince() {
    $.getJSON("http://localhost:8080/api/provinces")
        .done(loadListProvinceOnSelect)
        .fail(console.error);
}

function loadListEstate() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/all_estates",
        dataType: 'json',
        success: function (data) {
            displayEstateOnWeb(data),
                loadListEstateOnSelect(data),
                console.log(data)
        },
        error: console.log
    });
}


function callApiGetEstateDetailById(id) {
    $.ajax({
        type: 'GET',
        url: `http://localhost:8080/api/estate_detail/${id}`,
        dataType: 'json',
        success: displayEstateDetailOnWeb,
        error: console.log
    });
}


function loadListProvinceOnSelect(provinceList) {
    const dropdown = $('#province-dropdown'); // Lấy phần tử dropdown menu
    dropdown.empty(); // Xóa các phần tử cũ
    provinceList.forEach(function (province, index) {
        dropdown.append(`
            <div class="form-check my-2">
                <input class="form-check-input" type="radio" name="province-radio" value="${province.name}" id="${index}" />
                <label class="form-check-label check-label-province" style="padding-top:1px" for="${index}">${province.name}</label>
            </div>
        `);
    });
}


function loadListEstateOnSelect(listEstate) {
    for (var i = 0; i < listEstate.length; i++) {
        var estateOption = $("<option/>");
        estateOption.val(listEstate[i].id); // Gán giá trị cho thuộc tính value
        estateOption.text(listEstate[i].title); // Gán văn bản hiển thị
        $("#input-search-estate, #input-search-2").append(estateOption); // Thêm vào cả hai thẻ select
    }
}


function displayEstateOnWeb(estates) {
    const carouselInner = document.querySelector('#carousel-index .carousel-inner');
    carouselInner.innerHTML = '';  // Clear any existing items
    // Create carousel items
    for (let i = 0; i < estates.length; i += 4) {
        const carouselItem = document.createElement('div');
        carouselItem.className = 'carousel-item';

        // Create a row for the cards
        const row = document.createElement('div');
        row.className = 'row mb-5';
        // Add cards to the row
        for (let j = i; j < i + 4 && j < estates.length; j++) {
            const estate = estates[j];
            let priceDisplay = {
                "Cho thuê": `${estate.price} triệu/tháng`,
                "Mở bán": `${estate.price} tỉ`
            }[estate.status] || estate.price;

            const card = document.createElement('div');
            card.className = 'col-lg-3 col-md-6 col-sm-12 mb-4';
            card.innerHTML = `
                    <div class="card shadow h-100">
                        <a href="#" data-id="${estate.id}" class="btn-detail-estate">
                            <img src="${estate.imageUrlMain}" alt="card-img-top" />
                        </a>
                        <div class="card-body px-3 pt-2 pb-0">
                            <a href="#" data-id="${estate.id}" class="btn-detail-estate">
                                <p class="card-title small text-dark fw-bold lh-sm">${estate.title}</p>
                            </a>            
                        </div>

                        <div class="card-body px-3 py-0">
                            <div class="d-flex justify-content-between">
                                <div class="d-flex flex-column align-items-center">
                                     <p class="card-text small fw-bold" id="estate-price">${priceDisplay}</p>
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                     <p class="card-text small fw-bold" id="estate-area">${estate.width * estate.length} m<sup>2</sup></p>
                                </div>
                            </div>
                        </div>
                    
                        <div class="bg-light d-flex justify-content-between align-items-center px-3 py-2">
                            <div class="d-flex flex-column">
                                <span class="small text-muted">
                                     <i class="fas fa-map-marker-alt"></i>&nbsp;${estate.provinceName}
                                </span>
                            </div>
                            <div class="d-flex flex-column">
                                <i class="icon-save fa fa-bookmark-o"
                                    data-mdb-ripple-init data-mdb-tooltip-init
                                    data-mdb-tooltip="true" data-mdb-placement="bottom"
                                    title="Bấm để lưu tin">
                                </i>
                            </div> 
                        </div>
                    </div>
            `;
            row.appendChild(card);
        }
        carouselItem.appendChild(row);
        carouselInner.appendChild(carouselItem);

        // Set the first item as active
        if (i === 0) {
            carouselItem.classList.add('active');
        }
    }
    // Khởi tạo tooltip sau khi thêm HTML vào DOM
    $('#carousel-index .icon-save').each(function () {
        new mdb.Tooltip(this);
    });
}


//hiện chi tiết
function displayEstateDetailOnWeb(estate) {
    //$("#sale-rent-currency").html(estate.status == "Mở bán" ? "&nbsp;tỉ" : "&nbsp;triệu/tháng");
    $("#estate-price-detail").html(
        estate.status === "Mở bán" ? estate.price + " tỉ" :
            estate.status === "Cho thuê" ? estate.price + " triệu/tháng" :
                estate.price
    );
    $("#estate-name").html(estate.title);
    $("#estate-description").html(estate.description);
    $("#estate-legal").html(estate.legalDocument);
    $("#estate-status").html(estate.status);
    $("#estate-type").html(estate.estateType);
    $("#estate-address").html("Đường " + estate.streetName + ", phường " + estate.wardName + ", " + estate.districtName + ", " + estate.provinceName);
    $("#estate-bedroom").html(estate.bedroom);
    $("#estate-toilet").html(estate.bathroom);
    $("#estate-width").html(estate.width);
    $("#estate-length").html(estate.length);
    $("#estate-furnitureType").html(estate.furnitureType);
    $("#estate-image-main").attr("src", estate.imageUrlMain);
    $("#estate-image-1").attr("src", estate.imageUrl1);
    $("#estate-image-2").attr("src", estate.imageUrl2);
    $("#estate-image-3").attr("src", estate.imageUrl3);
    $("#estate-image-4").attr("src", estate.imageUrl4);
    //fancybox
    $("#data-url-main").attr("data-src", estate.imageUrlMain);
    $("#data-url-1").attr("data-src", estate.imageUrl1);
    $("#data-url-2").attr("data-src", estate.imageUrl2);
    $("#data-url-3").attr("data-src", estate.imageUrl3);
    $("#data-url-4").attr("data-src", estate.imageUrl4);
    //thumbnail
    $("#estate-image-main-thumb").attr("src", estate.imageUrlMain);
    $("#estate-image-1-thumb").attr("src", estate.imageUrl1);
    $("#estate-image-2-thumb").attr("src", estate.imageUrl2);
    $("#estate-image-3-thumb").attr("src", estate.imageUrl3);
    $("#estate-image-4-thumb").attr("src", estate.imageUrl4);
}


