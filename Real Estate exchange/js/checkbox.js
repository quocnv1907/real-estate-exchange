

//thay đổi text trên button province
$("#province-dropdown").on('click', '.form-check-input', function () {
    let selectedCity = $(this).siblings('label').text();
    $('#dropdownMenuButton-province').text(selectedCity);// Thay đổi nội dung của button thành tên thành phố đã chọn

});

//đặt lại html tỉnh-thành phố
$('#reset-province').on('click', function () {
    $('#province-dropdown .form-check-input').prop('checked', false);
    $('.form-check-input').val("");
    $("#dropdownMenuButton-province").html("Khu vực");
});

//lấy type - Cập nhật estateType dựa trên trạng thái checkbox
$('#estateType-dropdown').on('click', '.form-check-input', function () {
    const value = $(this).next('label').text();
    const isChecked = $(this).is(':checked');
    isChecked ? estateType.push(value) : estateType = estateType.filter(item => item !== value);
    //console.log('Các giá trị đã chọn: ', estateType);
});

// Bỏ chọn tất cả các checkbox type
$('#reset-type').on('click', function () {
    estateType = [];
    $('#estateType-dropdown .form-check-input').prop('checked', false);
    //console.log('Đã reset. Mảng hiện tại: ', estateType);
});

