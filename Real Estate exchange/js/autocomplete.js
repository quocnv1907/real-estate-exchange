$('.select2').select2();

$('#input-search-estate,  #input-search-2').on('select2:select', function (e) {
  // Khi có một tùy chọn được chọn, xóa tất cả các lựa chọn cũ và chỉ giữ lại tùy chọn mới
  var selectedValue = e.params.data.id;
  $(this).val(selectedValue).trigger('change');
});


$('#input-search-estate, #input-search-2').select2({
  tags: true,
  templateResult: formatEstateOption, // Hàm để hiển thị text và secondary text
  templateSelection: formatEstateSelection, // Tùy chỉnh hiển thị khi một mục được chọn
  createTag: function (params) {
    var term = $.trim(params.term);
    if (term === '') {
      return null;
    }
    return {
      id: term,
      text: term,
      newTag: true // Gắn cờ để đánh dấu đây là một mục nhập mới
    };
  }
});

// Hàm tùy chỉnh hiển thị trong danh sách gợi ý
function formatEstateOption(estate) {
  let $estate = $(
    `<div>
        <strong>${estate.text}</strong>
    </div>`
  );
  return $estate;
}

// Hàm hiển thị khi mục đã chọn
function formatEstateSelection(estate) {
  return estate.text;
}
