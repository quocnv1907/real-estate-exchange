$(document).ready(function () {
  // Initialize Fancybox for all images within the carousel
  $('[data-fancybox="gallery"]').fancybox({
    buttons: [
      'slideShow',
      'fullScreen',
      'download',
      'thumbs',
      'close'
    ],
    loop: true,
    protect: true
  });

  const options = {
    contentClick: "toggleCover",
    Images: {
      Panzoom: {
        panMode: "mousemove",
        mouseMoveFactor: 1.2,
        mouseMoveFriction: 0.12,
      },
    },
  };
});

// var $carousel = $('#bootstrap-gallery-carousel');

// // Ensure only one click is processed
// var sliding = false;
// $carousel.on('slide.bs.carousel', function () {
//   sliding = true;
// });

// $carousel.on('slid.bs.carousel', function () {
//   sliding = false;
// });

// // Ensure that slide transition completes before allowing the next slide action
// $('.carousel-control-prev, .carousel-control-next').on('click', function (e) {
//   if (sliding) {
//     e.preventDefault();
//   }
// });