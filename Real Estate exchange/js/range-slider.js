$(document).ready(function () {
    // Initialize the slider
    var sliderPrice = $('#price-slider').slider();
    var sliderPriceRent = $('#price-rent-slider').slider();
    var sliderArea = $('#area-slider').slider(); //diện tích
    var bedroomSlider = $("#bedroom-slider").slider();
    var toiletSlider = $("#toilet-slider").slider();
    var bedroomRange = '';
    var toiletRange = '';

    // Prevent dropdown from closing when interacting with the slider
    $('.dropdown-menu').on('click', function (e) {
        e.stopPropagation();  // Ngăn dropdown đóng khi tương tác với slider
    });

    // giá bán
    sliderPrice.on('slide slideStop', function (event) {
        var range = event.value;
        var min = range[0];
        var max = range[1];
        $('#dropdownMenuButton-price').html(min + '&nbsp; - &nbsp; ' + max + " tỉ");
    });

    //giá thuê
    sliderPriceRent.on('slide slideStop', function (event) {
        var range = event.value;
        var min = range[0];
        var max = range[1];
        //console.log("giá min " + min + " giá max: " + max)
        $('#dropdownMenuButton-price').html(min + '&nbsp; - &nbsp;' + max + " triệu");
    });

    //Thay đổi hiệu ứng text PRICE
    $('#price-dropdown').on('click', '.form-check-input', function () {
        let selectedPrice = $(this).siblings('.form-check-label').text().trim();
        $('#dropdownMenuButton-price').text(selectedPrice);
    });


    //giá bán
    const setPriceSliderValue = (min, max) => {
        $('#price-slider').slider('setValue', [min, max]);
    };

    //giá thuê
    const setRentSliderValue = (min, max) => {
        $('#price-rent-slider').slider('setValue', [min, max]);
    };

    const setAreaSliderValue = (min, max) => {
        $('#area-slider').slider('setValue', [min, max]);
    };

    $('input[type=radio]').on('click', function () {
        switch (this.id) {
            //giá bán
            case 'price-radio-1':
                setPriceSliderValue(0, 0.5);
                break;
            case 'price-radio-2':
                setPriceSliderValue(0.5, 1);
                break;
            case 'price-radio-3':
                setPriceSliderValue(1, 5);
                break;
            case 'price-radio-4':
                setPriceSliderValue(5, 10);
                break;
            case 'price-radio-5':
                setPriceSliderValue(10, 50);
                break;
            //giá thuê
            case 'rent-radio-1':
                setRentSliderValue(0, 10);
                break;
            case 'rent-radio-2':
                setRentSliderValue(10, 50);
                break;
            case 'rent-radio-3':
                setRentSliderValue(50, 100);
                break;
            case 'rent-radio-4':
                setRentSliderValue(100, 200);
                break;
        }
    });

    $('input[type=radio]').on('click', function () {
        switch (this.id) {
            //giá bán
            case 'area-1':
                setAreaSliderValue(0, 100);
                break;
            case 'area-2':
                setAreaSliderValue(100, 200);
                break;
            case 'area-3':
                setAreaSliderValue(200, 500);
                break;
            case 'area-4':
                setAreaSliderValue(500, 1000);
                break;
        }
    });

    //reset price
    $('#reset-price').on('click', function () {
        setPriceSliderValue(0, 50);
        $('#price-dropdown .form-check-input').prop('checked', false);
        $('#dropdownMenuButton-price').html("Khoảng giá");
    })

    //reset rent price
    $('#reset-price-rent').on('click', function () {
        setRentSliderValue(0, 200);
        $('#price-dropdown .form-check-input').prop('checked', false);
        $('#dropdownMenuButton-price').html("Khoảng giá");
    })


    sliderArea.on('slide slideStop', function (event) {
        var range = event.value;
        var min = range[0];
        var max = range[1];
        $('#dropdown-area').html(min + '&nbsp; - &nbsp;' + max + " m<sup>2</sup>");
    });


    bedroomSlider.on('slide slideStop', function (event) {
        var range = event.value;
        var min = range[0];
        var max = range[1];
        bedroomRange = min + '&nbsp;-&nbsp;' + max;
        updateButton();
    });

    toiletSlider.on('slide slideStop', function (event) {
        var range = event.value;
        var min = range[0];
        var max = range[1];
        toiletRange = min + '&nbsp;-&nbsp;' + max;
        updateButton();
    });


    function updateButton() {
        var text = '';
        if (bedroomRange) {
            text += bedroomRange + " phòng ngủ";
        }
        if (toiletRange) {
            if (text) text += ', ';
            text += toiletRange + " toilet";
        }
        // Giới hạn chiều dài text hiển thị
        var maxLength = 25;
        var displayText = text.length > maxLength ? text.substring(0, maxLength) + '...' : text;
        $('#dropdownMenuButton-more').html(displayText);
    }

});





// $(function () {
//     /* BOOTSTRAP SLIDER */
//     //$('.slider').bootstrapSlider()

//     /* ION SLIDER */
//     $('#range_1').ionRangeSlider({
//         min: 0,
//         max: 5000,
//         from: 1000,
//         to: 4000,
//         type: 'double',
//         step: 1,
//         prefix: '$',
//         prettify: false,
//         hasGrid: true
//     })
//     $('#range_2').ionRangeSlider()

//     $('#range_5').ionRangeSlider({
//         min: 0,
//         max: 10,
//         type: 'single',
//         step: 0.1,
//         postfix: ' mm',
//         prettify: false,
//         hasGrid: true
//     })
//     $('#range_6').ionRangeSlider({
//         min: -50,
//         max: 50,
//         from: 0,
//         type: 'single',
//         step: 1,
//         postfix: '°',
//         prettify: false,
//         hasGrid: true
//     })

//     $('#range_4').ionRangeSlider({
//         type: 'single',
//         step: 100,
//         postfix: ' light years',
//         from: 55000,
//         hideMinMax: true,
//         hideFromTo: false
//     })
//     $('#range_3').ionRangeSlider({
//         type: 'double',
//         postfix: ' miles',
//         step: 10000,
//         from: 25000000,
//         to: 35000000,
//         onChange: function (obj) {
//             var t = ''
//             for (var prop in obj) {
//                 t += prop + ': ' + obj[prop] + '\r\n'
//             }
//             $('#result').html(t)
//         },
//         onLoad: function (obj) {
//             //
//         }
//     })
// })