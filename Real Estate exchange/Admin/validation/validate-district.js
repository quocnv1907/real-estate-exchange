
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateDistrict();
        }
    });

    $('#modal-add-district').validate({
        rules: {
            name: {
                required: true
            },
            prefix: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên quận huyện"
            },
            prefix: {
                required: "Hãy chọn đơn vị hành chính",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-district').on('hidden.bs.modal', function () {
        //Clear form inputs
        // $('#modal-add-investor')[0].reset();
        // Remove error classes and messages
        $('#modal-add-district').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-district').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateDistrict();
        }
    });

    $('#modal-edit-district').validate({
        rules: {
            name: {
                required: true
            },
            prefix: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên quận - huyện"
            },
            prefix: {
                required: "Hãy chọn đơn vị hành chính",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-district').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-district')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-district').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-district').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


