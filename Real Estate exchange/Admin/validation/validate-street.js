
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateStreet();
        }
    });

    $('#modal-add-street').validate({
        rules: {
            name: {
                required: true
            },
            prefix: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên đường"
            },
            prefix: {
                required: "Hãy chọn loại đường"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-street').on('hidden.bs.modal', function () {
        //Clear form inputs
        // $('#modal-add-investor')[0].reset();
        // Remove error classes and messages
        $('#modal-add-street').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-street').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateStreet();
        }
    });

    $('#modal-edit-street').validate({
        rules: {
            name: {
                required: true
            },
            prefix: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên đường"
            },
            prefix: {
                required: "Hãy chọn loại đường"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-street').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-street')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-street').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-street').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


