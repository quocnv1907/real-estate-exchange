
const URL_LIST_DEPARTMENT = "http://localhost:8080/api/departments";

//apply for add form department
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
         //   onBtnCreateDepartment();
        }
    });

    //check employeeCode exist
    $.validator.addMethod("departmentCodeExist", function (value, element) {
        return checkDeptCodeExists(value);
    });

    $('#modal-add-department').validate({
        rules: {
            code: {
                required: true,
                minlength: 2,
                departmentCodeExist: true
            },
            name: {
                required: true,
                minlength: 2 
            },
            operation: {
                required: true
            },
            introduction: {
                required: true,  
                minlength: 20 
            }
        },
        messages: {
            code: {
                required: "Hãy nhập mã phòng",
                minlength: "Mã phòng có ít nhất 2 kí tự",
                departmentCodeExist: "Mã phòng đã tồn tại"
            },
            name: {
                required: "Hãy nhập tên phòng",
                minlength: "Tên phòng có ít nhất 2 kí tự"
            },
            operation: {
                required: "Hãy nhập vai trò của phòng",
            },
            introduction: {
                required: "Hãy viết lời giới thiệu",
                minlength: "Lời giới thiệu phải ít nhất 20 kí tự"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-department').on('hidden.bs.modal', function () {
         //Clear form inputs
         $('#modal-add-department')[0].reset();
        // Remove error classes and messages
        $('#modal-add-department').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-department').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit department
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateDepartment();
        }
    });

    //check employeeCode exist
    $.validator.addMethod("departmentCodeExist", function (value, element) {
        return checkDeptCodeExists(value);
    });

    $('#modal-edit-department').validate({
        rules: {
            code: {
                required: true,
                minlength: 2,
                departmentCodeExist: true
            },
            name: {
                required: true,
                minlength: 2 
            },
            operation: {
                required: true
            },
            introduction: {
                required: true,  
                minlength: 20 
            }
        },
        messages: {
            code: {
                required: "Hãy nhập mã phòng",
                minlength: "Mã phòng có ít nhất 2 kí tự",
                departmentCodeExist: "Mã phòng đã tồn tại"
            },
            name: {
                required: "Hãy nhập tên phòng",
                minlength: "Tên phòng có ít nhất 2 kí tự"
            },
            operation: {
                required: "Hãy nhập vai trò của phòng",
            },
            introduction: {
                required: "Hãy viết lời giới thiệu",
                minlength: "Lời giới thiệu phải ít nhất 20 kí tự"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-department').on('hidden.bs.modal', function () {
         //Clear form inputs
         $('#modal-edit-department')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-department').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-department').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


function checkDeptCodeExists(value) {
    var exists = false;
    $.ajax({
        type: "GET",
        url: URL_LIST_DEPARTMENT,
        async: false,
        success: function (data) {
            exists = data.some(function (dept) {
                return dept.departmentCode === value;
            });
        }
    });
    return !exists;
}

  

  