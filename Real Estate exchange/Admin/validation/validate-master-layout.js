
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateMasterLayout();
        }
    });

    $('#modal-add-master-layout').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true,
                minlength: 10
            },
            area: {
                required: true,
                min: 0
            },
            num_apart: {
                required: true,
                min: 0
            },
            photo: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên sơ đồ"
            },
            description: {
                required: "Hãy viết mô tả",
                minlength: "Mô tả ít nhất 10 kí tự"
            },
            area: {
                required: "Hãy nhập tổng diện tích",
                min: "Diện tích không được là số âm"
            },
            num_apart: {
                required: "Hãy nhập tổng số căn hộ",
                min: "Số lượng không được là số âm"
            },
            photo: {
                required: "Hãy tải ảnh lên!"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-master-layout').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-add-master-layout')[0].reset();
        // Remove error classes and messages
        $('#modal-add-master-layout').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-master-layout').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateMasterLayout();
        }
    });

    $('#modal-edit-master-layout').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true,
                minlength: 10
            },
            area: {
                required: true,
                min: 0
            },
            num_apart: {
                required: true,
                min: 0
            },
            photo: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên sơ đồ"
            },
            description: {
                required: "Hãy viết mô tả",
                minlength: "Mô tả ít nhất 10 kí tự"
            },
            area: {
                required: "Hãy nhập tổng diện tích",
                min: "Diện tích không được là số âm"
            },
            num_apart: {
                required: "Hãy nhập tổng số căn hộ",
                min: "Số lượng không được là số âm"
            },
            photo: {
                required: "Hãy tải ảnh lên!"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-product-line').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-product-line')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-product-line').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-product-line').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


