
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateProduct();
        }
    });

    $('#modal-add-employee').validate({
        rules: {
            firstname: {
                required: true,
                pattern: /^[a-zA-Z\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*$/ // Chỉ chấp nhận chữ cái và khoảng trắng
            },
            lastname: {
                required: true,
                pattern: /^[a-zA-Z\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*$/ // Chỉ chấp nhận chữ cái và khoảng trắng
            },
            extension: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            office_code: {
                required: true
            },
            job_title: {
                required: true,
            }
        },
        messages: {
            firstname: {
                required: "Please enter first name",
                pattern: "First name must contain only alphabetic characters"
            },
            lastname: {
                required: "Please enter last name",
                pattern: "Last name must contain only alphabetic characters. "
            },
            extension: {
                required: "Please enter extension",
            },
            email: {
                required: "Please enter email",
                email: "Please enter valid email format!"
            },
            office_code: {
                required: "Please choose office code"
            },
            job_title: {
                required: "Please enter phone number"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-employee').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-add-employee')[0].reset();
        // Remove error classes and messages
        $('#modal-add-employee').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-employee').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateEmployee();
        }
    });

    $('#modal-edit-employee').validate({
        rules: {
            firstname: {
                required: true,
                pattern: /^[a-zA-Z\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*$/ // Chỉ chấp nhận chữ cái và khoảng trắng
            },
            lastname: {
                required: true,
                pattern: /^[a-zA-Z\sàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*$/ // Chỉ chấp nhận chữ cái và khoảng trắng
            },
            extension: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            office_code: {
                required: true,
                min: 0
            },
            report: {
                required: true,
            },
            job_title:{
                required: true
            }
        },
        messages: {
            firstname: {
                required: "Please enter first name",
                pattern: "First name must contain only alphabetic characters"
            },
            lastname: {
                required: "Hãy nhập họ tên",
                pattern: "Last name must contain only alphabetic characters"
            },
            extension: {
                required: "Please enter extension",
            },
            email: {
                required: "Hãy nhập ngày/tháng/năm sinh",
                email: "Please enter valid email format!"
            },
            office_code: {
                required: "Please enter office code",
                min: "Office code must be a positive integer"
            },
            report: {
                required: "Please enter report"
            },
            job_title: {
                required: "Please enter job title!",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-employee').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-employee')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-employee').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-employee').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


