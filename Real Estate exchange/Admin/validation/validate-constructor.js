
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateConstructor();
        }
    });

    $('#modal-add-constructor').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true,
                minlength: 10
            },
            address: {
                required: true,
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 11
            },
            fax: {
                required: true,
            },
            email: {
                required: true,
                // Sử dụng biểu thức chính quy để kiểm tra định dạng email
                pattern: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
            },
            website: {
                required: true,
                url: true
            },
            phone2: {
                digits: true,
                minlength: 10,
                maxlength: 11
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên công ty"
            },
            description: {
                required: "Hãy viết mô tả",
                minlength: "Mô tả có ít nhất 10 kí tự"
            },
            address: {
                required: "Hãy nhập địa chỉ",
            },
            phone: {
                required: "Hãy nhập số điện thoại",
                digits: "Số điện thoại chỉ bao gồm số.",
                minlength: "Số điện thoại phải có ít nhất 10 số.",
                maxlength: "Số điện thoại không được vượt quá 11 số."
            },
            fax: {
                required: "Hãy nhập số fax"
            },
            email: {
                required: "Hãy nhập email",
                pattern: "Định dạng email không hợp lệ"
            },
            website: {
                required: "Hãy nhập địa chỉ website",
                url: "Vui lòng nhập url hợp lệ"
            },
            phone2: {
                digits: "Số điện thoại chỉ bao gồm số.",
                minlength: "Số điện thoại phải có ít nhất 10 số.",
                maxlength: "Số điện thoại không được vượt quá 11 số."
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-constructor').on('hidden.bs.modal', function () {
        //Clear form inputs
        // $('#modal-add-investor')[0].reset();
        // Remove error classes and messages
        $('#modal-add-constructor').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-constructor').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateConstructor();
        }
    });

    $('#modal-edit-constructor').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true,
                minlength: 10
            },
            address: {
                required: true,
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 11
            },
            fax: {
                required: true,
            },
            email: {
                required: true,
                // Sử dụng biểu thức chính quy để kiểm tra định dạng email
                pattern: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
            },
            website: {
                required: true,
                url: true
            },
            phone2: {
                digits: true,
                minlength: 10,
                maxlength: 11
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên công ty"
            },
            description: {
                required: "Hãy viết mô tả",
                minlength: "Mô tả ít nhất 10 kí tự"
            },
            address: {
                required: "Hãy nhập địa chỉ",
            },
            phone: {
                required: "Hãy nhập số điện thoại",
                digits: "Số điện thoại chỉ bao gồm số.",
                minlength: "Số điện thoại phải có ít nhất 10 số.",
                maxlength: "Số điện thoại không được vượt quá 11 số."
            },
            fax: {
                required: "Hãy nhập số fax"
            },
            email: {
                required: "Hãy nhập email",
                pattern: "Định dạng email không hợp lệ"
            },
            website: {
                required: "Hãy nhập địa chỉ website",
                url: "Vui lòng nhập url hợp lệ"
            },
            phone2: {
                digits: "Số điện thoại chỉ bao gồm số.",
                minlength: "Số điện thoại phải có ít nhất 10 số.",
                maxlength: "Số điện thoại không được vượt quá 11 số."
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-constructor').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-constructor')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-constructor').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-constructor').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


