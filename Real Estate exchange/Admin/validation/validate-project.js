
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateProject();
        }
    });

    $('#modal-add-project').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            province: {
                required: true
            },
            district: {
                required: true,
            },
            ward: {
                required: true,
            },
            street: {
                required: true
            },
            investor: {
                required: true,
            },
            designer: {
                required: true,
            },
            construction: {
                required: true
            },
            area: {
                required: true,
                min: 0
            },
            construct_area: {
                required: true,
                min: 0
            },
            num_block: {
                required: true,
                min: 0
            },
            num_floor: {
                required: true,
                min: 0
            },
            num_apart: {
                required: true,
                min: 0
            },
            apart_area: {
                required: true,
                min: 0
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên dự án"
            },
            description: {
                required: "Hãy viết mô tả"
            },
            province: {
                required: "Hãy chọn địa chỉ thành phố"
            },
            district: {
                required: "Hãy chọn địa chỉ quận huyện",
            },
            ward: {
                required: "Hãy chọn địa chỉ phường xã",
            },
            street: {
                required: "Hãy chọn địa chỉ đường"
            },
            investor: {
                required: "Hãy chọn nhà đầu tư",
            },
            designer: {
                required: "Hãy chọn nhà thiết kế"
            },
            construction : {
                required: "Hãy chọn thầu xây dựng",
            },
            area: {
                required: "Hãy nhập tổng diện tích",
                min: "Diện tích không được là số âm"
            },
            construct_area: {
                required: "Hãy nhập diện tích xây dựng",
                min: "Diện tích phải lớn hơn 0"
            },
            num_block: {
                required: "Hãy nhập số lượng block",
                min: "Số lượng phải lớn hơn 0"
            },
            num_floor: {
                required: "Hãy nhập số lượng nền",
                min: "Số lượng phải lớn hơn 0"
            },
            num_apart: {
                required: "Hãy nhập số lượng căn hộ",
                min: "Số lượng phải lớn hơn 0"
            },
            apart_area: {
                required: "Hãy nhập tổng diện tích căn hộ",
                min: "Diện tích không được là số âm"
            }

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-project').on('hidden.bs.modal', function () {
        //Clear form inputs
       // $('#modal-add-project')[0].reset();
        // Remove error classes and messages
        $('#modal-add-project').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-project').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateProject();
        }
    });

    $('#modal-edit-project').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            province: {
                required: true
            },
            district: {
                required: true,
            },
            ward: {
                required: true,
            },
            street: {
                required: true
            },
            investor: {
                required: true,
            },
            designer: {
                required: true,
            },
            constructor: {
                required: true,
            },
            area: {
                required: true,
                min: 0
            },
            construct_area: {
                required: true,
                min: 0
            },
            num_block: {
                required: true,
                min: 0
            },
            num_floor: {
                required: true,
                min: 0
            },
            num_apart: {
                required: true,
                min: 0
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên dự án"
            },
            description: {
                required: "Hãy viết mô tả"
            },
            province: {
                required: "Hãy chọn địa chỉ thành phố"
            },
            district: {
                required: "Hãy chọn địa chỉ quận huyện",
            },
            ward: {
                required: "Hãy chọn địa chỉ phường xã",
            },
            street: {
                required: "Hãy chọn địa chỉ đường"
            },
            investor: {
                required: "Hãy chọn nhà đầu tư",
            },
            designer: {
                required: "Hãy chọn nhà thiết kế",
            },
            construction: {
                required: "Hãy chọn thầu xây dựng",
            },
            area: {
                required: "Hãy nhập tổng diện tích",
                min: "Diện tích không được là số âm"
            },
            construct_area: {
                required: "Hãy nhập diện tích xây dựng",
                min: "Diện tích phải lớn hơn 0"
            },
            num_block: {
                required: "Hãy nhập số lượng block",
                min: "Số lượng phải lớn hơn 0"
            },
            num_floor: {
                required: true,
                min: "Số lượng phải lớn hơn 0"
            },
            num_apart: {
                required: "Hãy nhập số lượng căn hộ",
                min: "Số lượng phải lớn hơn 0"
            },
            apart_area: {
                required: "Hãy nhập tổng diện tích căn hộ",
                min: "Diện tích không được là số âm"
            }

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-project').on('hidden.bs.modal', function () {
        //Clear form inputs
      //  $('#modal-edit-project')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-project').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-project').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


