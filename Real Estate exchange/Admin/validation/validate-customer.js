
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
           // onBtnCreateCustomer();
        }
    });

    $('#modal-add-customer').validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true,
            },
            phone: {
                required: true,
            },
            address: {
                required: true,
            },
            city: {
                required: true
            },
            state: {
                required: true,
            },
            postal: {
                required: true,
            },
            country: {
                required: true,
            },
            sale_rep: {
                required: true,
            },
            credit: {
                required: true,
            }
        },
        messages: {
            firstname: {
                required: "Please enter first name"
            },
            lastname: {
                required: "Please enter last name",
            },
            phone: {
                required: "Please enter phone number",
            },
            address: {
                required: "Please enter address",
            },
            city: {
                required: "Please enter city"
            },
            state: {
                required: "Please enter state",
            },
            postal: {
                required: "Please enter postal code",
            },
            country: {
                required: "Please enter country",
            },
            sale_rep: {
                required: "Please enter sale rep number",
            },
            credit: {
                required: "Please enter credit limit",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-customer').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-add-customer')[0].reset();
        // Remove error classes and messages
        $('#modal-add-customer').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-customer').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateCustomer();
        }
    });

    $('#modal-edit-customer').validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true,
            },
            phone: {
                required: true,
            },
            address: {
                required: true,
            },
            city: {
                required: true
            },
            postal: {
                required: true,
            },
            state:{
                required: true
            },
            country: {
                required: true,
            },
            sale_rep: {
                required: true,
            },
            credit: {
                required: true,
            }
        },
        messages: {
            firstname: {
                required: "Please enter first name"
            },
            lastname: {
                required: "Please enter last name",
            },
            phone: {
                required: "Please enter phone number",
            },
            address: {
                required: "Please enter address",
            },
            city: {
                required: "Please enter city"
            },
            postal: {
                required: "Please enter postal code",
            },
            state: {
                required : "Please enter state name"
            },
            country: {
                required: "Please enter country",
            },
            sale_rep: {
                required: "Please enter sale rep number",
            },
            credit: {
                required: "Please enter credit limit",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-customer').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-customer')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-customer').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-customer').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


