
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateEstate();
        }
    });

    $('#modal-add-estate').validate({
        rules: {
            title: {
                required: true,
                minlength: 10,
                maxlength: 50
            },
            province: {
                required: true
            },
            district: {
                required: true,
            },
            type: {
                required: true
            },
            status: {
                required: true
            },
            price: {
                required: true,
                min: 0,
                max: 50000000000,
                number: true
            },
            price_rent: {
                required: true,
                min: 0,
                max: 1000000000,
                number: true
            },
            width: {
                required: true,
                min: 1,
                number: true
            },
            length: {
                required: true,
                min: 1,
                number: true,
            },
            bath: {
                required: true,
                min: 0,
                number: true
            },
            bed: {
                required: true,
                min: 0,
                number: true
            },
            legal: {
                required: true
            },
            investor: {
                required: true,
            },
            designer: {
                required: true,
            },
            constructor: {
                required: true
            },
            description: {
                required: true
            },
            image_add_main: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Chưa nhập tiêu đề",
                minlength: "Tiêu đề tối thiếu 10 kí tự",
                maxlength: "Tiêu đề tối đa 50 kí tự"
            },
            province: {
                required: "Hãy chọn tỉnh - thành phố"
            },
            district: {
                required: "Hãy chọn quận - huyện",
            },
            status: {
                required: "Mục đích sử dụng bất động sản ?"
            },
            type: {
                required: "Loại bất động sản ?"
            },
            price: {
                required: "Hãy nhập giá bán",
                min: "Không được nhập số âm",
                max: "Giá bán cao nhất là 50 tỉ đồng",
                number: "Chỉ được nhập số",
            },
            price_rent: {
                required: "Hãy nhập giá cho thuê",
                min: "Không được là số âm",
                max: "Giá thuê cao nhất là 1 tỉ đồng",
                number: "Chỉ được nhập số"
            },
            width: {
                required: "Hãy nhập chiều rộng",
                min: "Chiều rộng phải là số dương",
                number: "Chỉ được nhập số"
            },
            length: {
                required: "Hãy nhập chiều dài",
                min: "Chiều dài phải là số dương",
                number: "Chỉ được nhập số"
            },
            bath: {
                required: "Hãy nhập số lượng toilet",
                min: "Không được chứa số âm",
                number: "Chỉ được nhập số"
            },
            bed: {
                required: "Hãy nhập số phòng ngủ",
                min: "Không được chứa số âm",
                number: "Chỉ được nhập số"
            },
            legal: {
                required: "Giấy tờ pháp lý là gì?"
            },
            investor: {
                required: "Chủ đầu tư ?",
            },
            designer: {
                required: "Đơn vị thiết kế ?",
            },
            constructor: {
                required: "Nhà thầu xây dựng ?"
            },
            description: {
                required: "Chưa viết mô tả"
            },
            image_add_main: {
                required: "Hãy thêm ảnh chính"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
            element.closest('.form-outline').append(error);
            element.closest('.file-upload').append(error);
            element.closest('.form-floating').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-estate').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-add-estate')[0].reset();
        // Remove error classes and messages
        $('#modal-add-estate').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-estate').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateEstate();
        }
    });

    $.validator.addMethod('fileRequired', function (value, element) {
        return $(element).data('loaded') || element.files.length > 0;
    });


    $('#modal-edit-estate').validate({
        rules: {
            name: {
                required: true,
                minlength: 10,
                maxlength: 50
            },
            type: {
                required: true
            },
            status: {
                required: true
            },
            province: {
                required: true
            },
            district: {
                required: true,
            },
            price: {
                required: true,
                min: 0,
                max: 50000000000,
                number: true
            },
            price_rent: {
                required: true,
                min: 0,
                max: 1000000000,
                number: true
            },
            width: {
                required: true,
                min: 0,
                number: true
            },
            length: {
                required: true,
                min: 0,
                number: true
            },
            bath: {
                required: true,
                min: 0,
                number: true
            },
            bed: {
                required: true,
                min: 0,
                number: true
            },
            legal: {
                required: true
            },
            investor: {
                required: true,
            },
            designer: {
                required: true,
            },
            constructor_edit: {
                required: true
            },
            description: {
                required: true
            },
            structure: {
                required: true,
            },
            image_edit_main: {
                fileRequired: true
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tiêu đề bài viết",
                minlength: "Tiêu đề tối thiểu 10 kí tự",
                maxlength: "Tiêu đề tối đa 50 kí tự"
            },
            type: {
                required: "Loại bất động sản là gì?"
            },
            status: {
                required: "Mục đích sử dụng là gì?"
            },
            province: {
                required: "Hãy chọn tỉnh - thành phố"
            },
            district: {
                required: "Hãy chọn quận - huyện",
            },
            price: {
                required: "Hãy nhập giá bán",
                min: "Không được nhập số ấm",
                max: "Giá bán cao nhất là 50 tỉ đồng",
                number: "Chỉ được nhập số"
            },
            price_rent: {
                required: "Hãy nhập giá cho thuê",
                min: "Giá thuê không được là số âm",
                max: "Giá thuê cao nhất là 1 tỉ đồng",
                number: "Chỉ được nhập số"
            },
            width: {
                required: "Hãy nhập chiều rộng",
                min: "Không được nhập số âm",
                number: "Chỉ được nhập số!"
            },
            length: {
                required: "Hãy nhập chiều dài",
                min: "Không được nhập số âm!",
                number: "Chỉ được nhập số!"
            },
            bath: {
                required: "Hãy nhập số lượng toilet",
                min: "Không được nhập số âm!",
                number: "Chỉ được nhập số"
            },
            bed: {
                required: "Hãy nhập số phòng ngủ",
                min: "Không được nhập số âm",
                number: "Chỉ được nhập số"
            },
            legal: {
                required: "Giấy tờ pháp lý?"
            },
            investor: {
                required: "Chủ đầu tư là gì?",
            },
            designer: {
                required: "Đơn vị thiết kế ?",
            },
            constructor_edit: {
                required: "Nhà thầu xây dựng?"
            },
            description: {
                required: "Hãy viết mô tả"
            },
            structure: {
                required: "Kiểu kiến trúc là gì ?",
            },
            image_edit_main: {
                fileRequired: "Hãy thêm ảnh chính"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
            element.closest('.form-outline').append(error);
            element.closest('.file-upload').append(error);
            element.closest('.form-floating').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-estate').on('hidden.bs.modal', function () {
        //Clear form inputs
        // Remove error classes and messages
        window.location.reload();
        $('#modal-edit-estate').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-estate').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


