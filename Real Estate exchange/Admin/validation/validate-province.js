
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreateProvince();
        }
    });

    $('#modal-add-province').validate({
        rules: {
            name: {
                required: true
            },
            code: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên công ty"
            },
            code: {
                required: "Hãy nhập mã tỉnh",
                minlength: "Mã tỉnh phải có ít nhất 2 kí tự"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-province').on('hidden.bs.modal', function () {
        //Clear form inputs
        // $('#modal-add-investor')[0].reset();
        // Remove error classes and messages
        $('#modal-add-province').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-province').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateProvince();
        }
    });

    $('#modal-edit-province').validate({
        rules: {
            name: {
                required: true
            },
            code: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            name: {
                required: "Hãy nhập tên tỉnh - thành phố"
            },
            code: {
                required: "Hãy nhập mã tỉnh - thành phố",
                minlength: "Mã tỉnh phải có ít nhất 2 kí tự"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-province').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-province')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-province').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-province').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


