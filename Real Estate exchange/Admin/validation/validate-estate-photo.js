
//const URL_LIST_EMPLOYEE = "http://localhost:8080/api/employees";

$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnCreatePhotoAlbum();
        }
    });

    $('#modal-add-estate-photo').validate({
        rules: {
            main_img: {
                required: true
            },
            img_1: {
                required: true
            },
            img_2: {
                required: true
            },
            img_3: {
                required: true
            },
            img_4: {
                required: true
            }
        },
        messages: {
            main_img: {
                required: "hãy chọn ảnh chính"
            },
            img_1: {
                required: "Hãy chọn ảnh 1"
            },
            img_2: {
                required: "Hãy chọn ảnh 2"
            },
            img_3: {
                required: "Hãy chọn ảnh 3"
            },
            img_4: {
                required: "Chưa chọn ảnh 4"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-estate-photo').on('hidden.bs.modal', function () {
        //Clear form inputs
        // $('#modal-add-investor')[0].reset();
        // Remove error classes and messages
        $('#modal-add-estate-photo').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-estate-photo').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateEstateAlbum();
        }
    });

    $.validator.addMethod('fileRequired', function(value, element) {
        return $(element).data('loaded') || element.files.length > 0;
    });

    $('#modal-edit-estate-photo').validate({
        rules: {
            main_img: {
                fileRequired: true
            },
            img_1: {
                fileRequired: true
            },
            img_2: {
                fileRequired: true
            },
            img_3: {
                fileRequired: true
            },
            img_4: {
                fileRequired: true
            }
        },
        messages: {
            main_img: {
                fileRequired: "Hãy chọn ảnh chính"
            },
            img_1: {
                fileRequired: "Hãy chọn ảnh 1"
            },
            img_2: {
                fileRequired: "Hãy chọn ảnh 2"
            },
            img_3: {
                fileRequired: "Hãy chọn ảnh 3"
            },
            img_4: {
                fileRequired: "Hãy chọn ảnh 4"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-estate-photo').on('hidden.bs.modal', function () {
        //Clear form inputs
        $('#modal-edit-estate-photo')[0].reset();
        $("#container-img-edit img").css("display", "block");
        // Remove error classes and messages
        $('#modal-edit-estate-photo').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-estate-photo').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


