

$(document).ready(function () {
    $('.file-upload').prepend('<span class="plus-icon">+</span>');
})

// Sự kiện onchange khi file được chọn
$('input[type="file"]').on('change', function () {
    const input = $(this);
    const container = input.parent();
    const file = input[0].files[0];
    const plusIcon = container.find('.plus-icon');
    if (file) {
        const reader = new FileReader();
        reader.onload = function (e) {
            // Tạo phần tử img nếu chưa có
            let img = container.find('img');
            if (img.length === 0) {
                img = $('<img>').appendTo(container);
            }
            img.attr('src', e.target.result);
            img.show(); // Hiển thị ảnh
            img.attr('data-src', e.target.result)  //set thuộc tính cho fancybox

            // Ẩn dấu cộng
            plusIcon.hide();
            input.removeClass('is-invalid'); // Bỏ class is-invalid
            input.next('span.error').hide(); // Ẩn span lỗi

            // Tạo nút xóa nếu chưa có
            let deleteIcon = container.find('.delete-icon');
            if (deleteIcon.length === 0) {
                deleteIcon = $('<div class="delete-icon">×</div>').appendTo(container);
                // Gán sự kiện onclick cho nút xóa
                deleteIcon.on('click', function () {
                    removeImage(input.attr('id'));
                });
            }
            deleteIcon.show(); // Hiển thị nút xóa
        };
        reader.readAsDataURL(file);
    }
});

// Hàm để xóa ảnh
function removeImage(inputId) {
    const input = $('#' + inputId);
    const container = input.parent();
    const plusIcon = container.find('.plus-icon');
    const img = container.find('img');
    const deleteIcon = container.find('.delete-icon');
    // Xóa file đã chọn trong input
    input.val('');
    // Ẩn và loại bỏ ảnh
    img.hide().remove();
    // Hiển thị lại dấu cộng
    plusIcon.show();
    // Ẩn nút xóa
    deleteIcon.hide().remove();
    //xóa data khỏi input
    // Đánh dấu ảnh đã bị xóa
    input.data('loaded', false);
   // input.addClass('is-invalid'); // Bỏ class is-invalid
   // input.next('span.error').show(); // Ẩn span lỗi
}

function addDeleteIcon(imageId, inputId) {
    let input = $('#' + inputId);
    // Kiểm tra xem đã có icon delete hay chưa
    if (!$(`#${imageId}`).siblings('.delete-icon').length) {
        let deleteIcon = $('<div class="delete-icon">×</div>');
        // Gắn sự kiện xóa ảnh khi click vào icon delete
        deleteIcon.click(function () {
            removeImage(imageId); // Gọi hàm xóa ảnh
            input.data('loaded', false);
        });
        // Thêm icon vào đúng container chứa ảnh
        $(`#${imageId}`).parent().append(deleteIcon);
    }
}

function removeDeleteIcon(imageId) {
    $(`#${imageId}`).siblings('.delete-icon').remove();
}

function showImageInPreview(imageUrl, previewId, inputId) {
    let previewElement = $(`#${previewId}`);
    let input = $('#' + inputId);
    if (imageUrl) {
        previewElement.attr('src', imageUrl).show();  // Hiển thị ảnh và thiết lập src của preview
        input.data("loaded", true);
        addDeleteIcon(previewId, inputId);  // Gọi hàm thêm icon delete cho preview hiện tại
    } else {
        previewElement.hide();
        removeDeleteIcon(previewId);
    }
}







