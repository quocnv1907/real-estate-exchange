$(document).ready(function () {

    
    // Template cho tất cả các select có class 'select-with-clear'
    $('.select-with-clear').each(function () {
        const select = $(this);
        const clearIcon = select.siblings('.clear-icon');
        const dropdownIcon = select.siblings('.dropdown-icon');

        // Hiển thị icon clear (X) khi có lựa chọn khác trong select
        select.on('change focus', function () {
            if (select.val() !== select.find('option[selected]').val()) {
                clearIcon.show();      // Hiển thị icon clear (X)
                dropdownIcon.hide();   // Ẩn icon dropdown
            } else {
                clearIcon.hide();      // Ẩn icon clear nếu quay lại mặc định
                dropdownIcon.show();   // Hiển thị lại icon dropdown
            }
        });

        // Khi nhấn vào nút X để xóa lựa chọn
        clearIcon.on('click', function () {
            select.val(select.find('option[selected]').val());  // Reset về giá trị mặc định
            clearIcon.hide();      // Ẩn icon clear (X)
            dropdownIcon.show();   // Hiển thị lại icon dropdown
             // Kích hoạt sự kiện change nếu cần
        });
    });


    $(".select-float-label").each(function () {
        const selectFloatLabel = $(this);
        const formGroup = selectFloatLabel.closest(".form-group");
        const clearIcon = selectFloatLabel.siblings('.clear-icon');
        const dropdownIcon = selectFloatLabel.siblings('.dropdown-icon');
        selectFloatLabel.val(selectFloatLabel.find('option[selected]').val());  // Reset về giá trị mặc định

        // Kiểm tra khi có tùy chọn được chọn
        selectFloatLabel.on('change', function () {
            if (selectFloatLabel.val() !== selectFloatLabel.find('option[selected]').val()) {
                formGroup.addClass('select-focused has-value');
            } else {
                formGroup.removeClass('has-value select-focused');
            }
        });

        // Khi nhấn vào select (focus)
        selectFloatLabel.on('focus', function () {
            formGroup.addClass('select-focused');
        });

        // Khi thoát khỏi select (blur)
        selectFloatLabel.on('blur', function () {
            if (!$(this).val()) {
                formGroup.removeClass('select-focused');
            }
        });

        // Khi nhấn vào icon clear
        clearIcon.on('click', function () {
            selectFloatLabel.val(selectFloatLabel.find('option[selected]').val());  // Reset về giá trị mặc định
            selectFloatLabel.val(""); // Xóa tất cả lựa chọn
            formGroup.removeClass('has-value select-focused');
            dropdownIcon.show();
            clearIcon.hide();
            // selectFloatLabel.trigger("change");
        });

        // Khi click ra ngoài select
        $(document).on('click', function (e) {
            if (!selectFloatLabel.is(e.target) && selectFloatLabel.has(e.target).length === 0) {
                if (!selectFloatLabel.val()) {
                    selectFloatLabel.removeClass('select-focused has-value'); // Xóa cả hai class
                }
            }
        });
    })


    //select-edit
    $(".select-label-data").each(function () {
        const selectLabelData = $(this);
        const formGroup = selectLabelData.closest(".form-group");
        const clearIcon = selectLabelData.siblings('.clear-icon');
        const dropdownIcon = selectLabelData.siblings('.dropdown-icon');

        if (selectLabelData.val(selectLabelData.find('option[selected]').val())) {// đã thêm
            formGroup.addClass('has-value');
            clearIcon.hide();
        }

        // Kiểm tra khi có tùy chọn được chọn
        selectLabelData.on('change', function () {
            if (selectLabelData.val() !== selectLabelData.find('option[selected]').val()) {
                formGroup.addClass('select-focused has-value');
            }
            else {
                formGroup.removeClass('has-value select-focused');
            }
        });

        // Khi nhấn vào select (focus)
        selectLabelData.on('focus', function () {
            formGroup.addClass('select-focused');
        });

        // Khi thoát khỏi select (blur)
        selectLabelData.on('blur', function () {
            if (!$(this).val()) {
                formGroup.removeClass('select-focused');
            }
        });

        // Khi nhấn vào icon clear
        clearIcon.on('click', function () {
            selectLabelData.val("");  // Reset về giá trị mặc định
            formGroup.removeClass('has-value select-focused');
            dropdownIcon.show();
            clearIcon.hide();
        });

        // Khi click ra ngoài select
        $(document).on('click', function (e) {
            if (!selectLabelData.is(e.target) && selectLabelData.has(e.target).length === 0) {
                if (!selectLabelData.val()) {
                    selectLabelData.removeClass('select-focused has-value'); // Xóa cả hai class
                }
            }
        });
    })

    // Áp dụng cho tất cả các modal có form bên trong
    $('form.modal').on('hidden.bs.modal', function () {
        var form = $(this);
        // Kiểm tra nếu form tồn tại trong modal
        // Reset form values
        form[0].reset();

        // Thêm class 'has-value' cho mỗi select-float-label-data nếu có giá trị
        form.find(".select-label-data").each(function () {
            const selectLabelData = $(this);
            const formGroup = selectLabelData.closest(".form-group");
            const clearIcon = selectLabelData.siblings('.clear-icon');
            const dropdownIcon = selectLabelData.siblings('.dropdown-icon');
            formGroup.addClass('has-value');
            dropdownIcon.show();  // Hiển thị icon xóa nếu cần
            clearIcon.hide();
        });
    });
});



function resetSelect(selectId) {
    const selectElement = $(selectId);
    const formGroup = selectElement.closest(".form-group");
    const clearIcon = selectElement.siblings('.clear-icon');
    const dropdownIcon = selectElement.siblings('.dropdown-icon');
    selectElement.val(""); // Đặt giá trị về rỗng
    dropdownIcon.show();
    clearIcon.hide();
    formGroup.removeClass('has-value select-focused');
    selectElement.removeClass('select-focused has-value'); // Xóa cả hai class
}