
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var projectId;
var provinceId;
var districtId;
var wardId;
var streetId;
var investorId;
var constructorId;
var designerId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-projects").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: null },
        { data: "description" },
        { data: "area" },
        { data: "constructArea" },
        { data: "numBlock" },
        { data: "numFloor" },
        { data: "numApartment" }
    ],
    columnDefs: [
        {
            targets: 9,
            render: function (data, type, row) {
                return `
                <div class="text-center">
                    <a class="btn btn-danger my-1 btn-sm btn-edit">
                        <i class="fas fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:15px"></i>
                    </a>
                </div> `;
            }
        },
        {
            targets: 2,
            render: function (data, type, row) {
                return row.streetName + ", " + row.wardName + ", " + row.districtName + ", " + row.provinceName;
            }
        }
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListProject();
    callApiGetListProvince();
    loadListDistrict();
    loadListWard();
    loadListStreet();
    callApiGetListDesigner();
    callApiGetListInvestor();
    callApiGetListConstructor();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-project").modal("show");
});


/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditProject(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-projects").on("click", ".btn-delete", function () {
    projectId = getProjectIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa dự án này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});


$("#select-add-province").on("change", function () {
    projectId = Number($(this).val());
});

$("#select-add-district").on("change", function () {
    districtId = Number($(this).val());
});

$("#select-add-ward").on("change", function () {
    wardId = Number($(this).val());
})

$("#select-add-street").on("change", function () {
    streetId = Number($(this).val());
})

$("#select-add-investor").on("change", function () {
    investorId = Number($(this).val());
});

$("#select-add-constructor").on("change", function () {
    constructorId = Number($(this).val());
});

$("#select-add-designer").on("change", function () {
    designerId = Number($(this).val());
})

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateProject() {
    var project = {
        name: "",
        provinceId: 0,
        districtId: 0,
        wardId: 0,
        streetId: 0,
        description: "",
        area: 0.0,
        constructArea: 0.0,
        numBlock: 0,
        numFloor: 0,
        numApartment: 0,
        investorId: 0,
        constructorId: 0,
        designerId: 0,
        utilities: "",
        regionLink: "",
        photo: "",
        latitude: 0.0,
        longtitude: 0.0,
    }
    getInputAddProject(project);
    console.log(project);
    callApiAddProject(project);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateProject() {
    var project = {
        name: "",
        provinceId: 0,
        districtId: 0,
        wardId: 0,
        streetId: 0,
        description: "",
        area: 0.0,
        constructArea: 0.0,
        numBlock: 0,
        numFloor: 0,
        numApartment: 0,
        investorId: 0,
        constructorId: 0,
        designerId: 0,
        utilities: "",
        regionLink: "",
        photo: "",
        latitude: 0.0,
        longtitude: 0.0,
    }
    getInputEditProject(project);
    callApiUpdateProject(project);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditProject(btnEdit) {
    projectId = getProjectIdFromButton(btnEdit);
    callApiGetProjectById(projectId);
    $("#modal-edit-project").modal("show");
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListProject() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/projects_without_image",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListProjectOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddProject(project) {
    $.ajax({
        url: "http://localhost:8080/api/projects-create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(project),
        success: function () {
            handleAddProjectSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateProject(project) {
    $.ajax({
        url: "http://localhost:8080/api/projects/update/" + projectId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(project),
        success: function () {
            handleUpdateProjectSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/projects/delete/" + projectId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteEstateSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetProjectById(id) {
    $.ajax({
        url: "http://localhost:8080/api/projects/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            //hiển thị thông tin nv lên form modal
            showProjectDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListProjectOnTable(project) {
    dataTable.clear();
    dataTable.rows.add(project);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getProjectIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddProject(project) {
    project.name = $("#input-add-name").val();
    project.provinceId = Number($("#select-add-province").val());
    project.districtId = Number($("#select-add-district").val());
    project.wardId = Number($("#select-add-ward").val());
    project.streetId = Number($("#select-add-street").val());
    project.description = $("#input-add-description").val();
    project.area = Number($("#input-add-area").val());
    project.constructArea = Number($("#input-add-construct-area").val());
    project.numBlock = Number($("#input-add-num-block").val());
    project.numFloor = Number($("#input-add-num-floor").val());
    project.numApartment = Number($("#input-add-num-apart").val());
    project.investorId = Number($("#select-add-investor").val());
    project.constructorId = Number($("#select-add-constructor").val());
    project.designerId = Number($("#select-add-designer").val());
    project.utilities = $("#input-add-utility").val();
    project.regionLink = $("#input-add-region-link").val();
    project.latitude = $("#input-add-latitude").val();
    project.longtitude = $("#input-add-longtitude").val();
    project.photo = $("#input-add-photo").val();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditProject(project) {
    project.name = $("#input-edit-name").val();
    project.provinceId = $("#select-edit-province").val();
    project.districtId = $("#select-edit-district").val();
    project.wardId = $("#select-edit-ward").val();
    project.streetId = $("#select-edit-street").val();
    project.description = $("#input-edit-description").val();
    project.area = Number($("#input-edit-area").val());
    project.constructArea = Number($("#input-edit-construct-area").val());
    project.numBlock = Number($("#input-edit-num-block").val());
    project.numFloor = Number($("#input-edit-num-floor").val());
    project.numApartment = Number($("#input-edit-num-apart").val());
    project.investorId = $("#select-edit-investor").val();
    project.constructorId = $("#select-edit-constructor").val();
    project.designerId = $("#select-edit-designer").val();
    project.utilities = $("#input-edit-utility").val();
    project.regionLink = $("#input-edit-region-link").val();
    project.latitude = Number($("#input-edit-latitude").val());
    project.longtitude = Number($("#input-edit-longtitude").val());
}

/**hiển thị chi tiết lên modal */
function showProjectDetailToModal(project) {
    $("#input-edit-name").val(project.name);
    $("#select-edit-province").val(project.provinceId);
    $("#select-edit-district").val(project.districtId);
    $("#select-edit-ward").val(project.wardId);
    $("#select-edit-street").val(project.streetId);
    $("#select-edit-investor").val(project.investorId);
    $("#select-edit-designer").val(project.designerId);
    $("#select-edit-constructor").val(project.constructorId);
    $("#input-edit-description").val(project.description);
    $("#input-edit-area").val(project.area);
    $("#input-edit-construct-area").val(project.constructArea);
    $("#input-edit-num-block").val(project.numBlock);
    $("#input-edit-num-apart").val(project.numApartment);
    $("#input-edit-num-floor").val(project.numFloor);
    $("#input-edit-utility").val(project.utilities);
    $("#input-edit-region-link").val(project.regionLink);
    $("#input-edit-latitude").val(project.latitude);
    $("#input-edit-longtitude").val(project.longtitude);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateProjectSuccess() {
    $("#modal-edit-project").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListProject();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteEstateSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa dự án");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListProject();
}

function handleAddProjectSuccess() {
    $("#modal-add-project").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Đã thêm dự án");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListProject();
}

///
function callApiGetListProvince() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/provinces",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListProvinceOnSelect(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

//load province to select
function loadListProvinceOnSelect(provinceList) {
    for (i = 0; i < provinceList.length; i++) {
        var provinceOption1 = $("<option/>");

        provinceOption1.prop("value", provinceList[i].id);
        provinceOption1.prop("text", provinceList[i].name);
        //edit
        var provinceOption2 = $("<option/>");
        provinceOption2.prop("value", provinceList[i].id);
        provinceOption2.prop("text", provinceList[i].name);

        $("#select-add-province").append(provinceOption1);
        $("#select-edit-province").append(provinceOption2);
    };
}

function loadListDistrict() {
    $("#select-add-province").on("change", function () {
        $("#select-add-district").prop("disabled", false);
        $("#select-add-district").html("");
        provinceId = $(this).val();
        getDistrictByProvinceId(provinceId);
    });
    $("#select-edit-province").on("change", function () {
        $("#select-edit-district").prop("disabled", false);
        $("#select-edit-district").html("");
        provinceId = $(this).val();
        getDistrictByProvinceId(provinceId);
    });
}

function loadListWard() {
    $("#select-add-district").on("change", function () {
        $("#select-add-ward").prop("disabled", false);
        $("#select-add-ward").html("");
        districtId = $(this).val();
        getWardByDistrictId(districtId);
    });
    $("#select-edit-district").on("change", function () {
        $("#select-edit-ward").prop("disabled", false);
        $("#select-edit-ward").html("");
        districtId = $(this).val();
        getWardByDistrictId(districtId);
    });
    $("#select-edit-ward").on("change focus", function () {
        var selectedValue = $("#select-edit-ward option:selected").text();
        $('#input-edit-ward').text(selectedValue);
    })
}


function loadListStreet() {
    $('#select-add-province, #select-add-district').change(function () {
        var provinceId = $('#select-add-province').val();
        var districtId = $('#select-add-district').val();
        // Kiểm tra cả hai giá trị không rỗng
        if (provinceId && districtId) {
            getStreetByProvinceDistrict(provinceId, districtId);
        }
    });
    $('#select-edit-province, #select-edit-district').change(function () {
        provinceId = $('#select-edit-province').val();
        districtId = $('#select-edit-district').val();
        // Kiểm tra cả hai giá trị không rỗng
        if (provinceId && districtId) {
            getStreetByProvinceDistrict(provinceId, districtId);
        }
    });
}

//call API
function getDistrictByProvinceId(provinceId) {
    $.ajax({
        url: "http://localhost:8080/api/districts/" + provinceId,
        method: "GET",
        success: function (response) {
            console.log(response);
            loadDataToDistrictSelect(response);
        },
        error: function (pXhrObj) {
            console.log(pXhrObj);
        }
    });
}

//find brandTypes array by brandId
function getWardByDistrictId(districtId) {
    $.ajax({
        url: "http://localhost:8080/api/wards-by-district/" + districtId,
        method: "GET",
        success: function (response) {
            console.log(response);
            loadDataToWardSelect(response);
        },
        error: function (pXhrObj) {
            console.log(pXhrObj);
        }
    });
}

function getStreetByProvinceDistrict(provinceId, districtId) {
    $.ajax({
        url: "http://localhost:8080/api/streetByProvinceDistrict/" + provinceId + "/" + districtId,
        method: "GET",
        success: function (response) {
            console.log(response);
            loadDataToStreetSelect(response);
        },
        error: function (pXhrObj) {
            console.log(pXhrObj);
        }
    });
}


function loadDataToDistrictSelect(districtList) {
    $("#select-add-district").prop("disabled", false);
    $("#select-edit-district").prop("disabled", false);
    if (districtList.length > 0) {
        for (i = 0; i < districtList.length; i++) {
            var districtOption1 = $("<option/>");
            districtOption1.prop("value", districtList[i].id);
            districtOption1.prop("text", districtList[i].name);

            var districtOption2 = $("<option/>");
            districtOption2.prop("value", districtList[i].id);
            districtOption2.prop("text", districtList[i].name);

            $("#select-add-district").append(districtOption1);
            $("#select-edit-district").append(districtOption2);
        };
    } else {
        $("#select-add-district").prop("disabled", "disabled");
        $("#select-edit-district").prop("disabled", "disabled");
    }
}

//load data ward to eelct
function loadDataToWardSelect(wardList) {
    $("#select-add-ward").prop("disabled", false);
    $("#select-edit-ward").prop("disabled", false);
    if (wardList.length > 0) {
        for (i = 0; i < wardList.length; i++) {
            var wardOption1 = $("<option/>");
            wardOption1.prop("value", wardList[i].id);
            wardOption1.prop("text", wardList[i].name);

            var wardOption2 = $("<option/>");
            wardOption2.prop("value", wardList[i].id);
            wardOption2.prop("text", wardList[i].name);

            $("#select-add-ward").append(wardOption1);
            $("#select-edit-ward").append(wardOption2);
        };
    } else {
        $("#select-add-ward").prop("disabled", "disabled");
        $("#select-edit-ward").prop("disabled", "disabled");
    }
}


function loadDataToStreetSelect(streetList) {
    $("#select-add-street").prop("disabled", false);
    $("#select-edit-street").prop("disabled", false);
    if (streetList.length > 0) {
        for (i = 0; i < streetList.length; i++) {
            var streetOption1 = $("<option/>");
            streetOption1.prop("value", streetList[i].id);
            streetOption1.prop("text", streetList[i].name);

            var streetOption2 = $("<option/>");
            streetOption2.prop("value", streetList[i].id);
            streetOption2.prop("text", streetList[i].name);

            $("#select-add-street").append(streetOption1);
            $("#select-edit-street").append(streetOption2);
        };
    } else {
        $("#select-add-street").prop("disabled", "disabled");
        $("#select-edit-street").prop("disabled", "disabled");
    }
}


//investor
function callApiGetListInvestor() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/investors",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListInvestorToSelect(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function loadListInvestorToSelect(listInvestor) {
    for (i = 0; i < listInvestor.length; i++) {
        var investorOptionAdd = $("<option/>");
        investorOptionAdd.prop("value", listInvestor[i].id);
        investorOptionAdd.prop("text", listInvestor[i].name);

        var investorOptionEdit = $("<option/>");
        investorOptionEdit.prop("value", listInvestor[i].id);
        investorOptionEdit.prop("text", listInvestor[i].name);

        $("#select-add-investor").append(investorOptionAdd);
        $("#select-edit-investor").append(investorOptionEdit);

        $("#select-edit-investor").on("change", function () {
            var selectedValue = $("#select-edit-investor option:selected").text();
            $('#input-edit-investor').text(selectedValue);
        })
    }
}

//designer
function callApiGetListDesigner() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/designUnits",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListDesignerToSelect(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function loadListDesignerToSelect(listDesigner) {
    for (i = 0; i < listDesigner.length; i++) {
        //option add form
        var designerOptionAdd = $("<option/>");
        designerOptionAdd.prop("value", listDesigner[i].id);
        designerOptionAdd.prop("text", listDesigner[i].name);

        var designerOptionEdit = $("<option/>");
        designerOptionEdit.prop("value", listDesigner[i].id);
        designerOptionEdit.prop("text", listDesigner[i].name);

        $("#select-add-designer").append(designerOptionAdd);
        $("#select-edit-designer").append(designerOptionEdit);

        $("#select-edit-designer").on("change", function () {
            var selectedValue = $("#select-edit-designer option:selected").text();
            $('#input-edit-designer').text(selectedValue);
        })
    }
}

//constructor
function callApiGetListConstructor() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/constructors",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListConstructorToSelect(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function loadListConstructorToSelect(listConstructor) {
    for (i = 0; i < listConstructor.length; i++) {
        //option add form
        var constructorOptionAdd = $("<option/>");
        constructorOptionAdd.prop("value", listConstructor[i].id);
        constructorOptionAdd.prop("text", listConstructor[i].name);

        var constructorOptionEdit = $("<option/>");
        constructorOptionEdit.prop("value", listConstructor[i].id);
        constructorOptionEdit.prop("text", listConstructor[i].name);

        $("#select-add-constructor").append(constructorOptionAdd);
        $("#select-edit-constructor").append(constructorOptionEdit);
    }
}


