
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var investorId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-investors").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "description" },
        { data: "address" },
        { data: "phone" },
        { data: "fax" },
        { data: "email" },
        { data: "website" },
        { data: "phone2" },
    ],
    columnDefs: [
        {
            targets: 9,
            render: function (data, type, row) {
                return `
                <div class="text-center">
                    <a class="btn btn-danger btn-sm btn-edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListInvestor();
   // callApiGetListDepartment();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-investor").modal("show");
});


/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditInvestor(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-investors").on("click", ".btn-delete", function () {
    investorId = getInvestorIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa nhà đầu tư này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});


/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateInvestor() {
    var investor = {
        name: "",
        description: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    getInputAddInvestor(investor);
    console.log(investor);
    callApiAddInvestor(investor);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateInvestor() {
    var investor = {
        name: "",
        description: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: "",
        note: ""
    }
    getInputEditInvestor(investor);
    callApiUpdateInvestor(investor);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditInvestor(btnEdit) {
    investorId = getInvestorIdFromButton(btnEdit);
    callApiGetInvestorById(investorId);
    $("#modal-edit-investor").modal("show");
    console.log(investorId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListInvestor() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/investors",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListCustomerOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddInvestor(emp) {
    $.ajax({
        url: "http://localhost:8080/api/investors/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(emp),
        success: function () {
            handleAddInvestorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateInvestor(investor) {
    $.ajax({
        url: "http://localhost:8080/api/investors/update/" + investorId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(investor),
        success: function () {
            handleUpdateInvestorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/investors/delete/" + investorId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteInvestorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetInvestorById(id) {
    $.ajax({
        url: "http://localhost:8080/api/investors/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            //hiển thị thông tin nv lên form modal
            showInvestorDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListCustomerOnTable(emp) {
    dataTable.clear();
    dataTable.rows.add(emp);
    dataTable.draw();
}

//tải danh sách department từ database lên form select
function loadListDepartmentToSelect(listDept) {
    for (i = 0; i < listDept.length; i++) {
        //option add form
        var deptOptionAdd = $("<option/>");
        deptOptionAdd.prop("value", listDept[i].id);
        deptOptionAdd.prop("text", listDept[i].name);
        $("#select-add-department").append(deptOptionAdd);
    }
}

//Lấy Id của nhân viên từ button 
function getInvestorIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return  dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddInvestor(investor) {
    investor.name = $("#input-add-name").val().trim();
    investor.description = $("#input-add-description").val().trim();
    investor.address = $("#input-add-address").val();
    investor.phone = $("#input-add-phone").val().trim();
    investor.phone2 = $("#input-add-phone2").val().trim();
    investor.fax = $("#input-add-fax").val().trim();
    investor.email = $("#input-add-email").val().trim();
    investor.website = $("#input-add-website").val().trim();
    investor.note = $("#input-add-note").val().trim();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditInvestor(investor) {
    investor.name = $("#input-edit-name").val().trim();
    investor.description = $("#input-edit-description").val().trim();
    investor.address = $("#input-edit-address").val();
    investor.phone = $("#input-edit-phone").val().trim();
    investor.phone2 = $("#input-edit-phone2").val().trim();
    investor.fax = $("#input-edit-fax").val().trim();
    investor.email = $("#input-edit-email").val().trim();
    investor.website = $("#input-edit-website").val().trim();
    investor.note = $("#input-edit-note").val().trim();
}

/**hiển thị chi tiết lên modal */
function showInvestorDetailToModal(investor) {
    $("#input-edit-name").val(investor.name);
    $("#input-edit-description").val(investor.description);
    $("#input-edit-address").val(investor.address);
    $("#input-edit-phone").val(investor.phone);
    $("#input-edit-phone2").val(investor.phone2);
    $("#input-edit-fax").val(investor.fax);
    $("#input-edit-email").val(investor.email);
    $("#input-edit-website").val(investor.website);
    $("#input-edit-note").val(investor.note);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateInvestorSuccess() {
    $("#modal-edit-investor").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Đã cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListInvestor();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteInvestorSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Investor info has been deleted");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListInvestor();
}

function handleAddInvestorSuccess() {
    $("#modal-add-investor").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Investor added");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListInvestor();
}

