
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var wardId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-wards").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "prefix" }
    ],
    columnDefs: [
        {
            targets: 3,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListWards();
    // callApiGetListDepartment();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-ward").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$("#table-wards").on("click", ".btn-edit", function () {
    onBtnEditWard(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-wards").on("click", ".btn-delete", function () {
    wardId = getDistrictIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa phường/xã này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateWard() {
    var ward = {
        name: "",
        prefix: "" 
    }
    getInputAddWard(ward);
    callApiAddWard(ward);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateWard() {
    var ward = {
        name: "",
        prefix: ""
    }
    console.log(ward);
    getInputEditWard(ward);
    callApiUpdateWard(ward);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditWard(btnEdit) {
    wardId = getDistrictIdFromButton(btnEdit);
    callApiGetDistrictById(wardId);
    $("#modal-edit-ward").modal("show");
    console.log(wardId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListWards() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/wards",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListDistrictOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddWard(ward) {
    $.ajax({
        url: "http://localhost:8080/api/wards/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(ward),
        success: function () {
            handleAddWardSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateWard(ward) {
    $.ajax({
        url: "http://localhost:8080/api/wards/update/" + wardId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(ward),
        success: function () {
            handleUpdateWardSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/districts/delete/" + wardId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteWardSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetDistrictById(id) {
    $.ajax({
        url: "http://localhost:8080/api/wards/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showWardDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListDistrictOnTable(district) {
    dataTable.clear();
    dataTable.rows.add(district);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getDistrictIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddWard(ward) {
    ward.name = $("#input-add-name").val().trim();
    ward.prefix = $("#select-add-prefix").val();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditWard(ward) {
    ward.name = $("#input-edit-name").val().trim();
    ward.prefix = $("#select-edit-prefix").val();
}

/**hiển thị chi tiết lên modal */
function showWardDetailToModal(ward) {
    $("#input-edit-name").val(ward.name);
    $("#select-edit-prefix").val(ward.prefix);
  
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateWardSuccess() {
    $("#modal-edit-ward").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListWards();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteWardSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListWards();
}

function handleAddWardSuccess() {
    $("#modal-add-ward").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListWards();
}

