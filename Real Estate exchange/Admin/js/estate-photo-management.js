
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var estateId;
var albumId;

//cấu hình firebase
const firebaseConfig = {
    apiKey: "AIzaSyA34rI_8zXUKfrTcZZGcq3J93F3qM1vo6o",
    authDomain: "real-estate-bd8a4.firebaseapp.com",
    projectId: "real-estate-bd8a4",
    storageBucket: "real-estate-bd8a4.appspot.com",
    messagingSenderId: "683778059137",
    appId: "1:683778059137:web:4aa06ab798e545ccc81680",
    measurementId: "G-YRZHEM7ZS7"
};

firebase.initializeApp(firebaseConfig);


/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-estate-photos").DataTable({
    columns: [
        { data: "id" },
        { data: "estateName" },
        { data: "imageUrlMain" },
        { data: "imageUrl1" },
        { data: "imageUrl2" },
        { data: "imageUrl3" },
        { data: "imageUrl4" },
    ],
    columnDefs: [
        {
            targets: 2,
            render: function (data, type, row) {
                if (type == "display") {
                    return '<img src="' + data + '"style= "width:100px; height:80px;">';
                }
                return data;
            }
        },
        {
            targets: 3,
            render: function (data, type, row) {
                return '<img src="' + data + '"style= "width:100px; height:80px;">';
            }
        },
        {
            targets: 4,
            render: function (data, type, row) {
                return '<img src="' + data + '"style= "width:100px; height:80px;">';
            }
        },
        {
            targets: 5,
            render: function (data, type, row) {
                return '<img src="' + data + '"style= "width:100px; height:80px;">';
            }
        },
        {
            targets: 6,
            render: function (data, type, row) {
                return '<img src="' + data + '"style= "width:100px; height:80px;">';
            }
        },
        {
            targets: 7,
            render: function (data, type, row) {
                return `
                <div class="text-center">
                    <a class="btn btn-danger btn-sm btn-edit">
                        <i class="fas fa-edit" ></i>
                    </a>
                    <a class="btn btn-danger btn-sm" id="btn-delete">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>`;
            }
        },
    ]
});



/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListEstateAlbum();
    callApiGetListEstate();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-estate-photo").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditAlbum(this);
});


/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-estate-photos").on("click", "#btn-delete", function () {
    albumId = getAlbumIdFromButton(this);
    console.log(albumId);
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});

$("#select-add-estate").on("change", function () {
    estateId = $(this).val();
    console.log(estateId);
});

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreatePhotoAlbum() {
    var album = {
        imageUrlMain: "",
        imageUrl1: "",
        imageUrl2: "",
        imageUrl3: "",
        imageUrl4: "",
        estateId: Number(estateId)
    }
    getInputAddAlbum(album).then(() => {
        console.log(album);
        callApiCreateAlbum(album);
    }).catch(console.error);
}

/***Xử lý khi nhấn nút Cập nhật */
async function onBtnUpdateEstateAlbum() {
    var album = {
        imageUrlMain: "",
        imageUrl1: "",
        imageUrl2: "",
        imageUrl3: "",
        imageUrl4: ""
    }
    await getInputEditAlbum(album);
    if ($("#modal-edit-estate-photo").valid()) { // Check if the form is valid before calling the API
        callApiUpdateAlbum(album);
    }
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditAlbum(btnEdit) {
    albumId = getAlbumIdFromButton(btnEdit);
    callApiGetAlbumById(albumId);
    $("#modal-edit-estate-photo").modal("show");
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListEstateAlbum() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/all_estate_images",
        dataType: 'json',
        success: function (data) {
            loadListAlbumOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiCreateAlbum(album) {
    $.ajax({
        url: "http://localhost:8080/api/albums-create/" + estateId,
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(album),
        success: function () {
            handleAddAlbumSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateAlbum(album) {
    $.ajax({
        url: "http://localhost:8080/api/albums/update/" + albumId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(album),
        success: function () {
            handleUpdateAlbumSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/albums/delete/" + albumId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteAlbumSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetAlbumById(id) {
    $.ajax({
        url: "http://localhost:8080/api/albums/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            //hiển thị thông tin nv lên form modal
            showAlbumDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListAlbumOnTable(album) {
    dataTable.clear();
    dataTable.rows.add(album);
    dataTable.draw();
}

function getInputAddAlbum(album) {
    //album.estate_id  = $("#select-add-estate");
    var albumFields = ['#input-file-main', '#input-file-1', '#input-file-2', '#input-file-3', '#input-file-4'];
    return Promise.all(
        albumFields.map((fileInputId, index) => {
            var file = $(fileInputId).prop('files')[0];
            return file ? uploadImage(file).then(url => {
                switch (index) {
                    case 0: album.imageUrlMain = url; break;
                    case 1: album.imageUrl1 = url; break;
                    case 2: album.imageUrl2 = url; break;
                    case 3: album.imageUrl3 = url; break;
                    case 4: album.imageUrl4 = url; break;
                }
            }) : Promise.resolve();
        })
    ).then(() => album);

}

function uploadImage(file) {
    return new Promise((resolve, reject) => {
        const ref = firebase.storage().ref();
        const metadata = {
            contentType: file.type
        };
        const name = file.name;
        const uploadTask = ref.child('images/' + name).put(file, metadata);

        uploadTask.on('state_changed',
            null,
            error => reject(error),
            () => {
                uploadTask.snapshot.ref.getDownloadURL().then(url => {
                    resolve(url);
                }).catch(reject);
            }
        );
    });
}


//lấy dữ liệu nhập vào trên form edit 
async function getInputEditAlbum(album) {
    const inputIds = [
        { input: '#input-edit-file-main', preview: '#img-preview-edit-main', prop: 'imageUrlMain' },
        { input: '#input-edit-file-1', preview: '#img-preview-edit-1', prop: 'imageUrl1' },
        { input: '#input-edit-file-2', preview: '#img-preview-edit-2', prop: 'imageUrl2' },
        { input: '#input-edit-file-3', preview: '#img-preview-edit-3', prop: 'imageUrl3' },
        { input: '#input-edit-file-4', preview: '#img-preview-edit-4', prop: 'imageUrl4' }
    ];

    for (const { input, preview, prop } of inputIds) {
        const fileInput = $(input).prop('files')[0];
        if (fileInput) {
            const url = await uploadImage(fileInput);
            album[prop] = url;
        } else {
            const currentUrl = $(preview).attr('src');
            if (!currentUrl) {
                //If the current URL is empty, it means the image was deleted and not replaced.
                $(input).prop('required', true);  //Add required attribute to trigger validation
            } else {
                album[prop] = currentUrl;
            }
        }
    }
}

/**hiển thị chi tiết lên modal */
function showAlbumDetailToModal(album) {
    $("#img-preview-edit-main").attr('src', album.imageUrlMain);
    $("#img-preview-edit-1").attr("src", album.imageUrl1);
    $("#img-preview-edit-2").attr("src", album.imageUrl2);
    $("#img-preview-edit-3").attr("src", album.imageUrl3);
    $("#img-preview-edit-4").attr("src", album.imageUrl4);

    $('#input-edit-file-main').next('.custom-file-label').text(album.imageUrlMain);
    $('#input-edit-file-1').next('.custom-file-label').text(album.imageUrl1);
    $('#input-edit-file-2').next('.custom-file-label').text(album.imageUrl2);
    $('#input-edit-file-3').next('.custom-file-label').text(album.imageUrl3);
    $('#input-edit-file-4').next('.custom-file-label').text(album.imageUrl4);

    $('#input-edit-file-main').data('loaded', true);
    $('#input-edit-file-1').data('loaded', true);
    $('#input-edit-file-2').data('loaded', true);
    $('#input-edit-file-3').data('loaded', true);
    $('#input-edit-file-4').data('loaded', true);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateAlbumSuccess() {
    $("#modal-edit-estate-photo").modal("hide");
    //hiện modal thông báo thành công
    toastr.success('Cập nhật thành công !')
    callApiGetListEstateAlbum();
}



/**Xử lý front end khi xóa thành công */
function handleDeleteAlbumSuccess() {
    $("#modal-confirm-delete").modal("hide");
    toastr.success('Đã xóa album!');
    callApiGetListEstateAlbum();
}

function handleAddAlbumSuccess() {
    $("#modal-add-estate-photo").modal("hide");
    toastr.success('Đã thêm album!')
    callApiGetListEstateAlbum();
}


function callApiGetListEstate() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/list_estates",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListEstateToSelect(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function loadListEstateToSelect(listEstate) {
    for (i = 0; i < listEstate.length; i++) {
        //option add form
        var estateOptionAdd = $("<option/>");
        estateOptionAdd.prop("value", listEstate[i].id);
        estateOptionAdd.prop("text", listEstate[i].name);
        $("#select-add-estate").append(estateOptionAdd);
    }
}

//Lấy Id của nhân viên từ button 
function getAlbumIdFromButton(button) {
    var tableRow = $(button).parents("tr");
    return dataTable.row(tableRow).data().id;
}