
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var customerId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-customers").DataTable({
    columns: [
        { data: "id" },
        { data: "firstName" },
        { data: "lastName" },
        { data: "phoneNumber" },
        { data: "address" },
        { data: "city" },
        { data: "state" },
        { data: "postalCode" },
        { data: "country" },
        { data: "saleRepEmployeeNumber" },
        { data: "creditLimit" }
    ],
    columnDefs: [
        {
            targets: 11,
            render: function (data, type, row) {
                return `
                <div class="text-center">
                    <a class="btn btn-warning btn-sm btn-edit">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListCustomers();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-customer").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditCustomer(this);
});


/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-customers").on("click", ".btn-delete", function () {
    customerId = getCustomerIdFromButton(this);
    $("#message-confirm-delete").text("Are you sure want to delete this customer?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});


/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateCustomer() {
    var customer = {
        firstName: "",
        lastName: "",
        phoneNumber: "",
        address: "",
        city: "",
        state: "",
        postalCode: "",
        country: "",
        saleRepEmployeeNumber: "0",
        creditLimit: "0",
    }
    getInputAddCustomer(customer);
    console.log(customer);
    callApiAddCustomer(customer);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateCustomer() {
    var customer = {
        firstName: "",
        lastName: "",
        phoneNumber: "",
        address: "",
        city: "",
        state: "",
        postalCode: "",
        country: "",
        saleRepEmployeeNumber: 0,
        creditLimit: 0,
    }
    getInputEditCustomer(customer);
    console.log(customer);
    callApiUpdateCustomer(customer);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditCustomer(btnEdit) {
    customerId = getCustomerIdFromButton(btnEdit);
    callApiGetCustomerById(customerId);
    $("#modal-edit-customer").modal("show");
    console.log(customerId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListCustomers() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/customers",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListProductOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddCustomer(customer) {
    $.ajax({
        url: "http://localhost:8080/api/customers/create" ,
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(customer),
        success: function () {
            handleAddCustomerSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateCustomer(customer) {
    $.ajax({
        url: "http://localhost:8080/api/customers/update/" + customerId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(customer),
        success: function () {
            handleUpdateCustomerSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/customers/delete/" + customerId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteCustomerSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetCustomerById(id) {
    $.ajax({
        url: "http://localhost:8080/api/customers/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            //hiển thị thông tin nv lên form modal
            showCustomerDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListProductOnTable(customer) {
    dataTable.clear();
    dataTable.rows.add(customer);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getCustomerIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddCustomer(customer) {
    customer.firstName = $("#input-add-firstname").val().trim();
    customer.lastName = $("#input-add-lastname").val();
    customer.phoneNumber = $("#input-add-phone").val();
    customer.address = $("#input-add-address").val();
    customer.city = $("#input-add-city").val();
    customer.state = $("#input-add-state").val().trim();
    customer.postalCode = $("#input-add-postal-code").val();
    customer.country = $("#input-add-country").val();
    customer.saleRepEmployeeNumber = $("#input-add-sale-rep").val();
    customer.creditLimit = $("#input-add-credit").val();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditCustomer(customer) {
    customer.firstName = $("#input-edit-firstname").val().trim();
    customer.lastName = $("#input-edit-lastname").val().trim();
    customer.phoneNumber = $("#input-edit-phone").val().trim();
    customer.address = $("#input-edit-address").val().trim();
    customer.city = $("#input-edit-city").val().trim();
    customer.state = $("#input-edit-state").val().trim();
    customer.postalCode = $("#input-edit-postal-code").val().trim();
    customer.country = $("#input-edit-country").val().trim();
    customer.saleRepEmployeeNumber = Number($("#input-edit-sale-rep").val());
    customer.creditLimit = Number($("#input-edit-credit").val());
}

/**hiển thị chi tiết lên modal */
function showCustomerDetailToModal(customer) {
    $("#input-edit-firstname").val(customer.firstName);
    $("#input-edit-lastname").val(customer.lastName);
    $("#input-edit-address").val(customer.address);
    $("#input-edit-phone").val(customer.phoneNumber);
    $("#input-edit-city").val(customer.city);
    $("#input-edit-state").val(customer.state);
    $("#input-edit-postal-code").val(customer.postalCode);
    $("#input-edit-country").val(customer.country);
    $("#input-edit-sale-rep").val(customer.saleRepEmployeeNumber);
    $("#input-edit-credit").val(customer.creditLimit);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateCustomerSuccess() {
    $("#modal-edit-customer").modal("hide");
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Customer info has been updated!");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListCustomers();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteCustomerSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Customer has been deleted!");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListCustomers();
}

function handleAddCustomerSuccess() {
    $("#modal-add-customer").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Customer has been added");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListCustomers();
}

