

const selectedOption = $("<option/>").val("").text("").prop("selected", true).attr("hidden", true);

$(document).ready(function () {
    callApiGetListEstate();
    callApiGetListProvince();
    loadListDistrict();
    loadListWard();
    loadListStreet();
    callApiGetListDesigner();
    callApiGetListInvestor();
    callApiGetListConstructor();
    //
    // callApiGetListDistrictToDetail();
    // callApiGetListWardToDetail();
    // callApiGetListStreetToDetail();
});

/***Lấy ds từ API */
function callApiGetListEstate() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/all_estates",
        dataType: 'json'
    }).done(loadListEstateOnTable).fail(console.log);
}


/***GỌI API thêm pet */
function callApiAddEstate(estate) {
    $.ajax({
        url: "http://localhost:8080/api/estates/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(estate)
    }).done(handleAddEstateSuccess).fail((paramErr) => console.log(paramErr.status));
}

/***Gọi API cập nhật */
function callApiUpdateEstate(estate) {
    $.ajax({
        url: `http://localhost:8080/api/estates/update/${estateId}`,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(estate)
    }).done(handleUpdateEstateSuccess).fail((paramErr) => console.log(paramErr.status));
}

/***lấy chi tiết thông tin của BĐS */
function callApiGetEstateById(id) {
    $.ajax({
        url: `http://localhost:8080/api/estate_detail/${id}`,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            console.log(data); // In dữ liệu ra console
            showEstateDetailToModal(data); // Gọi hàm hiện chi tiết lên modal
        },
        error: (error) => console.log(error.status)
    });
}

/**Gọi API xóa thông tin */
function callApiDeleteEstate(estateId) {
    $.ajax({
        url: `http://localhost:8080/api/estates_delete/${estateId}`,
        type: "DELETE",
        contentType: gCONTENT_TYPE
    }).done(handleDeleteEstateSuccess).fail((paramErr) => console.log(paramErr.status));
}

//investor
function callApiGetListInvestor() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/investors",
        dataType: 'json',
        success: loadListInvestorToSelect,
        error: (error) => console.log(error)
    });
}

//designer
function callApiGetListDesigner() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/designUnits",
        dataType: 'json',
        success: loadListDesignerToSelect,
        error: (error) => console.log(error)
    });
}

//constructor
function callApiGetListConstructor() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/constructors",
        dataType: 'json',
        success: loadListConstructorToSelect,
        error: (error) => console.log(error)
    });
}

//province
function callApiGetListProvince() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/provinces",
        dataType: 'json',
        success: loadListProvinceOnSelect,
        error: (error) => console.log(error)
    });
}


//district
function getDistrictByProvinceId(provinceId) {
    $.ajax({
        url: `http://localhost:8080/api/districts/${provinceId}`,
        method: "GET",
        success: loadDataToDistrictSelect,
        error: (pXhrObj) => console.log(pXhrObj)
    });
}

//ward by district and province
function getWardByDistrictId(districtId) {
    $.ajax({
        url: `http://localhost:8080/api/wards-by-district/${districtId}`,
        method: "GET",
        success: loadDataToWardSelect,
        error: (pXhrObj) => console.log(pXhrObj)
    });
}

//street
function getStreetByProvinceDistrict(provinceId, districtId) {
    $.ajax({
        url: `http://localhost:8080/api/streetByProvinceDistrict/${provinceId}/${districtId}`,
        method: "GET",
        success: loadDataToStreetSelect,
        error: (pXhrObj) => console.log(pXhrObj)
    });
}


//load province to select
function loadListProvinceOnSelect(provinceList) {
    $("#select-add-province, #select-edit-province").append(selectedOption);
    provinceList.forEach(function (province) {
        var provinceOption = $("<option/>", { value: province.id, text: province.name });
        $("#select-add-province, #select-edit-province").append(provinceOption.clone());
    });
}

function loadListDistrict() {
    $("#select-add-province").on("change", function () {
        provinceId = $(this).val();
        $("#select-add-district").prop("disabled", false).html("");
        $("#select-add-ward, #select-add-street").prop("disabled", true).html("");
        getDistrictByProvinceId(provinceId);
    });
    //edit district
    $("#select-edit-province").on("change", function () {
        provinceId = $(this).val();
        $("#select-edit-district").prop("disabled", false).html("");
        $("#select-edit-ward, #select-edit-street").prop("disabled", true).html("");
        getDistrictByProvinceId(provinceId);
    });
}


function loadListWard() {
    $("#select-add-district").on("change", function () {
        districtId = $(this).val();
        $("#select-add-ward").prop("disabled", false).html("");
        $("#select-add-street").prop("disabled", false).html("");
        getWardByDistrictId(districtId);
    });
    //edit
    $("#select-edit-district").on("change", function () {
        districtId = $(this).val();
        $("#select-edit-ward").prop("disabled", false).html("");
        $("#select-edit-street").prop("disabled", false).html("");
        getWardByDistrictId(districtId);
    });
}

function loadListStreet() {
    $('#select-add-province, #select-add-district').change(function () {
        var provinceId = $('#select-add-province').val();
        var districtId = $('#select-add-district').val();
        $("#select-add-street").html("");
        getStreetByProvinceDistrict(provinceId, districtId);
    });
    //edit
    $('#select-edit-province, #select-edit-district').change(function () {
        provinceId = $('#select-edit-province').val();
        districtId = $('#select-edit-district').val();
        $("#select-edit-street").html("");
        getStreetByProvinceDistrict(provinceId, districtId);
    });
}

function loadDataToDistrictSelect(districtList) {
    $("#select-add-district, #select-edit-district").append(selectedOption);
    if (districtList.length > 0) {
        districtList.forEach(district => {
            var districtOption = $("<option/>").val(district.id).text(district.name);
            $("#select-add-district").append(districtOption.clone());
            $("#select-edit-district").append(districtOption);
        });
    }
}

//load data ward to select
function loadDataToWardSelect(wardList) {
    
    if (wardList.length > 0) {
        wardList.forEach(ward => {
            var wardOption = $("<option/>").val(ward.id).text(ward.name);
            $("#select-add-ward").append(wardOption.clone());
            $("#select-edit-ward").append(wardOption);
        });
    }
    $("#select-add-ward, #select-edit-ward").append(selectedOption);
}

function loadDataToStreetSelect(streetList) {
    if (streetList.length > 0) {
        streetList.forEach(street => {
            var streetOption = $("<option/>").val(street.id).text(street.name);
            $("#select-add-street").append(streetOption.clone());
            $("#select-edit-street").append(streetOption);
        });
    }
    $("#select-add-street,#select-edit-street").append(selectedOption);
}


function loadListInvestorToSelect(listInvestor) {
    listInvestor.forEach(investor => {
        var investorOption = $("<option/>").val(investor.id).text(investor.name);
        // Append the option to both select elements
        $("#select-add-investor").append(investorOption.clone());
        $("#select-edit-investor").append(investorOption);
    });
}


function loadListDesignerToSelect(listDesigner) {
    listDesigner.forEach(designer => {
        var designerOption = $("<option/>").val(designer.id).text(designer.name);
        // Append to both add and edit select elements
        $("#select-add-designer").append(designerOption.clone());
        $("#select-edit-designer").append(designerOption);
    });
}

function loadListConstructorToSelect(listConstructor) {
    listConstructor.forEach(constructor => {
        var constructorOption = $("<option/>").val(constructor.id).text(constructor.name);
        // Append to both add and edit select elements
        $("#select-add-constructor").append(constructorOption.clone());
        $("#select-edit-constructor").append(constructorOption);
    });
}





