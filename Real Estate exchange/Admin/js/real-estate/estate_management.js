


/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var estateId;
var provinceId;
var districtId;
var wardId;
var streetId;
var investorId;
var constructorId;
var designerId;



/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-estates").DataTable({
    columns: [
        { data: "id" },
        { data: "title" },
        { data: "estateType" },
        { data: "status" },
        { data: null }, //địa chỉ
        { data: "price" },
        { data: "priceRent" },
        { data: null }, //diện tích
        { data: "bedroom" },
        { data: "bathroom" },
    ],
    columnDefs: [
        {
            targets: 10,
            render: function (data, type, row) {
                return `
                <div class="text-center">
                    <a class="btn my-1 btn-sm btn-edit">
                        <i class="fas fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:15px"></i>
                    </a>
                </div> `;
            }
        },
        {
            targets: 4,
            render: function (data, type, row) {
                return row.streetName + ", " + row.wardName + ", " + row.districtName + ", " + row.provinceName;
            }
        },
        {
            targets: 7,
            render: function (data, type, row) {
                return row.length * row.width + " m<sup>2</sup>";
            }
        },
        {
            targets: 3,
            render: function (data, type, row) {
                let badgeClass;
                if (data === 'Mở bán') {
                    badgeClass = 'bg-danger badge-large';
                } else if (data === 'Cho thuê') {
                    badgeClass = 'bg-success badge-large';
                }
                return `<span class="badge ${badgeClass}" style="font-size:15px">${data}</span>`;
            }
        },
        {
            targets: 5,
            render: function (data, type, row) {
                return row.price + "<br>" + "(tỉ đồng)";
            }
        },
        {
            targets: 6,
            render: function (data, type, row) {
                return row.priceRent + "<br>" + "(triệu/tháng)";
            }
        }
    ],
    dom: '<"top"lf>rt<"bottom"ip><"clear">',
    initComplete: function () {
        // Chèn nút vào phần "l" (dropdown "Show entries")
        $('.dataTables_length').prepend('<button id="btn-add" class="btn btn-dark mr-5 my-2 pr-5">Thêm</button>');
    },
    language: {
        lengthMenu: "Hiển thị _MENU_ bản ghi",
        search: "Tìm kiếm:" // Thay đổi chữ "Search" thành "Tìm kiếm"
    },
});


/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-estates").on("click", ".btn-delete", function () {
    estateId = getEstateIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa mục này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDeleteEstate(estateId);
});


$("#select-add-province").on("change", function () {
    estateId = Number($(this).val());
});

$("#select-add-district").on("change", function () {
    districtId = Number($(this).val());
});

$("#select-add-ward").on("change", function () {
    wardId = Number($(this).val());
})

$("#select-add-street").on("change", function () {
    streetId = Number($(this).val());
})

$("#select-add-investor").on("change", function () {
    investorId = Number($(this).val());
});

$("#select-add-constructor").on("change", function () {
    constructorId = Number($(this).val());
});

$("#select-add-designer").on("change", function () {
    designerId = Number($(this).val());
})

/***hàm được khi khi nhấn nút Thêm  */
async function onBtnCreateEstate() {
    var estate = {
        title: "",
        estateType: "",
        status: "",
        price: 0,
        priceRent: 0,
        width: 0,
        length: 0,
        provinceId: 0,
        districtId: 0,
        wardId: 0,
        streetId: 0,
        description: "",
        bedroom: 0,
        bathroom: 0,
        legalDocument: "",
        investorId: 0,
        constructorId: 0,
        designerId: 0,
        structure: "",
        furnitureType: "",
        latitude: 0.0,
        longtitude: 0.0,
        imageUrlMain: "",
        imageUrl1: "",
        imageUrl2: "",
        imageUrl3: "",
        imageUrl4: ""
    }
    await getInputAddEstate(estate);
    callApiAddEstate(estate);
}


/***Xử lý khi nhấn nút Cập nhật */
async function onBtnUpdateEstate() {
    var estate = {
        title: "",
        estateType: "",
        status: "",
        price: 0.0,
        priceRent: 0.0,
        provinceId: 0,
        width: 0,
        length: 0,
        districtId: 0,
        wardId: 0,
        streetId: 0,
        description: "",
        bedroom: 0,
        bathroom: 0,
        legalDocument: "",
        investorId: 0,
        constructorId: 0,
        designerId: 0,
        structure: "",
        furnitureType: "",
        imageUrlMain: "",
        imageUrl1: "",
        imageUrl2: "",
        imageUrl3: "",
        imageUrl4: "",
        latitude: 0.0,
        longtitude: 0.0,
    }
    await getInputEditEstate(estate);
    callApiUpdateEstate(estate);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Tải danh sách nhân viên lên data table */
function loadListEstateOnTable(estate) {
    dataTable.clear();
    dataTable.rows.add(estate);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getEstateIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
async function getInputAddEstate(estate) {
    estate.title = $("#input-add-name").val();
    estate.provinceId = Number($("#select-add-province").val());
    estate.districtId = Number($("#select-add-district").val());
    estate.wardId = Number($("#select-add-ward").val());
    estate.streetId = Number($("#select-add-street").val());
    estate.estateType = $("#select-add-type").val();
    estate.status = $("#select-add-status").val();
    estate.price = Number($("#input-add-price").val());
    estate.priceRent = Number($("#input-add-price-rent").val());
    estate.length = Number($("#input-add-length").val());
    estate.width = Number($("#input-add-width").val());
    estate.legalDocument = $("#select-add-legal").val();
    estate.bathroom = $("#input-add-num-bath").val();
    estate.bedroom = $("#input-add-num-bed").val();
    estate.furnitureType = $("#input-add-furniture").val();
    estate.investorId = Number($("#select-add-investor").val());
    estate.constructorId = Number($("#select-add-constructor").val());
    estate.designerId = Number($("#select-add-designer").val());
    estate.structure = $("#select-add-structure").val();
    estate.description = $("#input-add-description").val();
    estate.latitude = $("#input-add-latitude").val();
    estate.longtitude = $("#input-add-longtitude").val();
    await getInputAddImage(estate);
}

//lấy dữ liệu nhập vào trên form edit 
async function getInputEditEstate(estate) {
    estate.title = $("#input-edit-name").val();
    estate.provinceId = Number($("#select-edit-province").val());
    estate.districtId = Number($("#select-edit-district").val());
    estate.wardId = Number($("#select-edit-ward").val());
    estate.streetId = Number($("#select-edit-street").val());
    estate.estateType = $("#select-edit-type").val();
    estate.status = $("#select-edit-status").val();
    estate.price = Number($("#input-edit-price").val());
    estate.priceRent = Number($("#input-edit-price-rent").val());
    estate.length = Number($("#input-edit-length").val());
    estate.width = Number($("#input-edit-width").val());
    estate.legalDocument = $("#select-edit-legal").val();
    estate.bathroom = $("#input-edit-num-bath").val();
    estate.bedroom = $("#input-edit-num-bed").val();
    estate.furnitureType = $("#input-edit-furniture").val();
    estate.investorId = Number($("#select-edit-investor").val());
    estate.constructorId = Number($("#select-edit-constructor").val());
    estate.designerId = Number($("#select-edit-designer").val());
    estate.structure = $("#select-edit-structure").val();
    estate.description = $("#input-edit-description").val();
    estate.latitude = $("#input-edit-latitude").val();
    estate.longtitude = $("#input-edit-longtitude").val();
    //lấy thông tin ảnh tải lên
    await getInputEditAlbum(estate);
}

/**hiển thị chi tiết lên modal */
function showEstateDetailToModal(estate) {
    $("#input-edit-name").val(estate.title);
    //hiển thị thành phố - quận huyện- phường xã- đường
    showPrefixOnSelect('select-edit-district', estate.districtId, estate.districtName);
    showPrefixOnSelect('select-edit-province', estate.provinceId, estate.provinceName);
    showPrefixOnSelect('select-edit-ward', estate.wardId, estate.wardName);
    showPrefixOnSelect('select-edit-street', estate.streetId, estate.streetName);
    //getDistrictByProvinceId(estate.provinceId);
    $("#select-edit-type").val(estate.estateType);
    $("#select-edit-status").val(estate.status);
    $("#input-edit-price").val(estate.price);
    $("#input-edit-price-rent").val(estate.priceRent);
    $("#input-edit-length").val(estate.length);
    $("#input-edit-width").val(estate.width);
    $("#select-edit-legal").val(estate.legalDocument);
    $("#input-edit-num-bath").val(estate.bathroom);
    $("#input-edit-num-bed").val(estate.bedroom);
    $("#input-edit-furniture").val(estate.furnitureType);
    $("#input-edit-address").val("Đường " + estate.streetName + ", " + estate.wardName + ", " + estate.districtName + ", " + estate.provinceName);
    $("#select-edit-investor").val(estate.investorId);
    $("#select-edit-constructor").val(estate.constructorId);
    $("#select-edit-designer").val(estate.designerId);
    $("#select-edit-structure").val(estate.structure);
    $("#input-edit-description").val(estate.description);
    $("#input-edit-latitude").val(estate.latitude);
    $("#input-edit-longtitude").val(estate.longtitude);
    // Hiển thị ảnh  preview, hàm trong file-upload.js
    showImageInPreview(estate.imageUrlMain, 'preview-edit-image-main', 'input-edit-image-main');
    showImageInPreview(estate.imageUrl1, 'preview-edit-image-1', 'input-edit-image-1');
    showImageInPreview(estate.imageUrl2, 'preview-edit-image-2', 'input-edit-image-2');
    showImageInPreview(estate.imageUrl3, 'preview-edit-image-3', 'input-edit-image-3');
    showImageInPreview(estate.imageUrl4, 'preview-edit-image-4', 'input-edit-image-4');
    //hiển thị chế độ slider toàn màn hinh
    $("#preview-edit-image-main").attr("data-src", estate.imageUrlMain);
    $("#preview-edit-image-1").attr("data-src", estate.imageUrl1);
    $("#preview-edit-image-2").attr("data-src", estate.imageUrl2);
    $("#preview-edit-image-3").attr("data-src", estate.imageUrl3);
    $("#preview-edit-image-4").attr("data-src", estate.imageUrl4);
    disabledInvalidType(estate.estateType);
    disabledPrice(estate.status);
}


async function getInputAddImage(estate) {
    //album.estate_id  = $("#select-add-estate");
    var albumFields = ['#input-add-image-main', '#input-add-image-1', '#input-add-image-2', '#input-add-image-3', '#input-add-image-4'];
    return Promise.all(
        albumFields.map((fileInputId, index) => {
            var file = $(fileInputId).prop('files')[0];
            return file ? uploadImage(file).then(url => {
                switch (index) {
                    case 0: estate.imageUrlMain = url; break;
                    case 1: estate.imageUrl1 = url; break;
                    case 2: estate.imageUrl2 = url; break;
                    case 3: estate.imageUrl3 = url; break;
                    case 4: estate.imageUrl4 = url; break;
                }
            }) : Promise.resolve();
        })
    ).then(() => estate);
}


async function getInputEditAlbum(estate) {
    const inputIds = [
        { id: '#input-edit-image-main', preview: '#preview-edit-image-main', prop: 'imageUrlMain' },
        { id: '#input-edit-image-1', preview: '#preview-edit-image-1', prop: 'imageUrl1' },
        { id: '#input-edit-image-2', preview: '#preview-edit-image-2', prop: 'imageUrl2' },
        { id: '#input-edit-image-3', preview: '#preview-edit-image-3', prop: 'imageUrl3' },
        { id: '#input-edit-image-4', preview: '#preview-edit-image-4', prop: 'imageUrl4' }
    ];
    for (const { id, preview, prop } of inputIds) {
        const fileInput = $(id).prop('files')[0];
        if (fileInput) {
            const url = await uploadImage(fileInput);
            estate[prop] = url;
        } else {
            const currentUrl = $(preview).attr('src');
            if (currentUrl) {
                estate[prop] = currentUrl;
            } else {
                $(id).prop('required', true);
            }
        }
    }
}

function showPrefixOnSelect(selectId, value, text) {
    $(`#${selectId}`).append($("<option/>").val(value).text(text)).val(value);
}
