
//cấu hình firebase

const firebaseConfig = {
    apiKey: "AIzaSyA34rI_8zXUKfrTcZZGcq3J93F3qM1vo6o",
    authDomain: "real-estate-bd8a4.firebaseapp.com",
    projectId: "real-estate-bd8a4",
    storageBucket: "real-estate-bd8a4.appspot.com",
    messagingSenderId: "683778059137",
    appId: "1:683778059137:web:4aa06ab798e545ccc81680",
    measurementId: "G-YRZHEM7ZS7"
};

firebase.initializeApp(firebaseConfig);


// //hàm xử lý upload ảnh
function uploadImage(file) {
    return new Promise((resolve, reject) => {
        const ref = firebase.storage().ref();
        const metadata = {
            contentType: file.type
        };
        const name = file.name;
        const uploadTask = ref.child('images/estates/' + name).put(file, metadata);

        uploadTask.on('state_changed',
            null,
            error => reject(error),
            () => {
                uploadTask.snapshot.ref.getDownloadURL().then(url => {
                    resolve(url);
                }).catch(reject);
            }
        );
    });
}


