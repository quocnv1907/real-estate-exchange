


$(document).ready(function () {
    isForSaleOrRent();
    isEstateType();
    enableLegal();
});

//dữ liệu gốc của prefix
let originalPrefix;

$(document).ready(function(){
    originalPrefix = $("#modal-edit-estate .edit-prefix").html();
})
// Khi modal mở ra, lưu trữ URL của các ảnh hiện tại
$("#btn-close-modal-edit").on("click", function () {
    window.location.reload();
})

$("#btn-close-modal-add").on('click', function () {
    window.location.reload();
})


/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-estate").modal("show");
});

$("#btn-edit-address").on("click", function () {
   // $("#btn-cancel-address").attr("hidden", false);
    $("#btn-edit-address").attr("hidden", true);
    $("#select-edit-province").prop("disabled", false);
})

// $("#btn-cancel-address").on("click", function () {
//     $("#btn-cancel-address").attr("hidden", true);
//     $("#btn-edit-address").attr("hidden", false);
//     $(".edit-prefix #select-edit-province").html(originalPrefix);
//     $("#select-edit-province, #select-edit-district, #select-edit-ward, #select-edit-street").prop("disabled", true)
//         .removeClass("is-invalid")
//         .next('span.error').hide();
// });


/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    estateId = getEstateIdFromButton(this);
    callApiGetEstateById(estateId);
    $("#modal-edit-estate").modal("show");
});

/***Xử lý front end khi cập nhật thành công */
function handleUpdateEstateSuccess() {
    $("#modal-edit-estate").modal("hide");
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListEstate();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteEstateSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin bất động sản");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListEstate();
}

function handleAddEstateSuccess() {
    $("#modal-add-estate").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Đã thêm thông tin bất động sản");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListEstate();
}

//chuyển trạng thái validator cho mục đích sử dụng bđs
function isForSaleOrRent() {
    $("#select-add-status, #select-edit-status").on("change", function () {
        const isForSale = $(this).val() === "Mở bán";
        const isForRent = $(this).val() === "Cho thuê";
        //price
        $("#input-add-price, #input-edit-price").prop("disabled", !isForSale).removeClass("is-invalid").next('span.error').hide();
        $("#input-add-price-rent, #input-edit-price-rent").prop("disabled", !isForRent).removeClass("is-invalid").next('span.error').hide();
        //legal add
        $("#select-add-legal option[value='Hợp đồng thuê'], #select-edit-legal option[value='Hợp đồng thuê']").prop("disabled", !isForRent);
        $("#input-add-price-rent, #input-edit-price-rent").val("");

        $("#select-add-legal option[value='Sổ hồng'], #select-edit-legal option[value='Sổ hồng']").prop("disabled", !isForSale);
        $("#input-add-price, #input-edit-price").val("");
    });
}

function isEstateType() {
    $("#select-add-type, #select-edit-type").on("change", function () {
        var estateType = $(this).val();
        const isDisabled = estateType === "Đất nền" || estateType === "Kho - xưởng";
        // Vô hiệu hóa các trường khi cần thiết
        $("#input-add-num-bath, #input-add-num-bed, #input-add-furniture, #select-add-constructor, #select-add-designer, #select-add-structure")
            .prop("disabled", isDisabled).val("")
            .removeClass("is-invalid")
            .next('span.error').hide();

        $("#input-edit-num-bath, #input-edit-num-bed, #input-edit-furniture, #select-edit-constructor, #select-edit-designer, #select-edit-structure")
            .prop("disabled", isDisabled).val("")
            .removeClass("is-invalid")
            .next('span.error').hide();
    });
}

function enableLegal() {
    $("#select-add-status, #select-edit-status").on("change", function () {
        const isEnabled = $(this).val() !== "";
        $("#select-add-legal, #select-edit-legal").prop("disabled", !isEnabled).val("").removeClass("is-invalid")
            .next('span.error').hide();;
    });
}


function disabledInvalidType(type) {
    if (type == "Đất nền" || type == "Kho - xưởng") {
        $("#input-edit-num-bath, #input-edit-num-bed, #input-edit-furniture, #select-edit-constructor, #select-edit-designer, #select-edit-structure")
            .prop("disabled", true).val("")
            .removeClass("is-invalid")
            .next('span.error').hide();
    }
    else {
        $("#input-edit-num-bath, #input-edit-num-bed, #input-edit-furniture, #select-edit-constructor, #select-edit-designer, #select-edit-structure")
            .prop("disabled", false)
    }
}


function disabledPrice(status) {
    if (status == "Mở bán") {
        $("#input-edit-price-rent").prop("disabled", true).val("")
            .removeClass("is-invalid")
            .next('span.error').hide();
    }
    else if (status == "Cho thuê") {
        $("#input-edit-price").prop("disabled", true).val("")
            .removeClass("is-invalid")
            .next('span.error').hide();
    }
}

