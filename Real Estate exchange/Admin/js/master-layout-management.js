
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var masterLayoutId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-master-layouts").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "description" },
        { data: "area" },
        { data: "apartmentList" },
        { data: "photo" },
        { data: "createDate" },
        { data: "updateDate" }
    ],
    columnDefs: [
        {
            targets: 8,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListMasterLayout();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-master-layout").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditMasterLayout(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-master-layouts").on("click", ".btn-delete", function () {
    masterLayoutId = getMasterLayoutIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa thông tin đơn vị thiết kế này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});


/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateMasterLayout() {
    var masterLayout = {
        name: "",
        description: "",
        area: 0,
        numApartment: 0,
        photo: "",
        createDate: "",
        updateDate: ""
    }
    getInputAddMasterLayout(masterLayout);
    callApiAddDesignUnit(masterLayout);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateMasterLayout() {
    var masterLayout = {
        name: "",
        description: "",
        area: "",
        numApartment: "",
        photo: "",
        createDate: "",
        updateDate: "",
    }
    getInputEditMasterLayout(masterLayout);
    callApiUpdateMasterLayout(masterLayout);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditMasterLayout(btnEdit) {
    masterLayoutId = getMasterLayoutIdFromButton(btnEdit);
    callApiGetMasterLayoutById(masterLayoutId);
    $("#modal-edit-designer").modal("show");
    console.log(masterLayoutId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListMasterLayout() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/masterLayouts",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListMasterLayoutOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddDesignUnit(designerUnit) {
    $.ajax({
        url: "http://localhost:8080/api/designUnits/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(designerUnit),
        success: function () {
            handleAddMasterLayoutSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateMasterLayout(masterLayout) {
    $.ajax({
        url: "http://localhost:8080/api/masterlayouts/update/" + masterLayoutId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(masterLayout),
        success: function () {
            handleUpdateMasterLayoutSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/masterLayouts/delete/" + masterLayoutId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteDesignUnitSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetMasterLayoutById(id) {
    $.ajax({
        url: "http://localhost:8080/api/masterLayouts/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showMasterLayoutDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListMasterLayoutOnTable(masterLayout) {
    dataTable.clear();
    dataTable.rows.add(masterLayout);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getMasterLayoutIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddMasterLayout(masterLayout) {
    masterLayout.name = $("#input-add-name").val().trim();
    masterLayout.description = $("#input-add-description").val().trim();
    masterLayout.area = $("#input-add-area").val();
    masterLayout.numApartment = $("#input-add-num-apart").val();
    masterLayout.photo = $("#input-add-photo").val().trim();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditMasterLayout(masterLayout) {
    masterLayout.name = $("#input-edit-name").val().trim();
    masterLayout.description = $("#input-edit-description").val().trim();
    masterLayout.area = $("#input-edit-area").val();
    masterLayout.numApartment = $("#input-edit-num-apart").val();
    masterLayout.photo = $("#input-edit-photo").val().trim();
}

/**hiển thị chi tiết lên modal */
function showMasterLayoutDetailToModal(masterLayout) {
    $("#input-edit-name").val(masterLayout.name);
    $("#input-edit-description").val(masterLayout.description);
    $("#input-edit-area").val(masterLayout.area);
    $("#input-edit-num-apart").val(masterLayout.numApartment);
    $("#input-edit-photo").val(masterLayout.photo);
    $("#input-edit-create-date").val(masterLayout.createDate);
    $("#input-edit-update-date").val(masterLayout.updateDate);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateMasterLayoutSuccess() {
    $("#modal-edit-master-layout").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListMasterLayout();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteDesignUnitSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListMasterLayout();
}

function handleAddMasterLayoutSuccess() {
    $("#modal-add-master-layout").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListMasterLayout();
}

