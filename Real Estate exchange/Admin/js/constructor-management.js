
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var constructorId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-constructors").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "description" },
        { data: "address" },
        { data: "phone" },
        { data: "fax" },
        { data: "website" },
        { data: "email" },
        { data: "phone2" },
    ],
    columnDefs: [
        {
            targets: 9,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListConstructor();
    // callApiGetListDepartment();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-constructor").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditConstructor(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-constructors").on("click", ".btn-delete", function () {
    constructorId = getConstructorIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa thông tin nhà thầu này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});


/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateConstructor() {
    var constructor = {
        name: "",
        description: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: ""
    }
    getInputAddConstructor(constructor);
    callApiAddConstructor(constructor);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateConstructor() {
    var investor = {
        name: "",
        description: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: ""
    }
    getInputEditConstructor(investor);
    callApiUpdateInvestor(investor);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditConstructor(btnEdit) {
    constructorId = getConstructorIdFromButton(btnEdit);
    callApiGetConstructorById(constructorId);
    $("#modal-edit-constructor").modal("show");
    console.log(constructorId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListConstructor() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/constructors",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListConstructorOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddConstructor(constructor) {
    $.ajax({
        url: "http://localhost:8080/api/constructors/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(constructor),
        success: function () {
            handleAddConstructorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateInvestor(constructor) {
    $.ajax({
        url: "http://localhost:8080/api/constructors/update/" + constructorId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(constructor),
        success: function () {
            handleUpdateConstructorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/constructors/delete/" + constructorId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteConstructorSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetConstructorById(id) {
    $.ajax({
        url: "http://localhost:8080/api/constructors/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showConstructorDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListConstructorOnTable(constructor) {
    dataTable.clear();
    dataTable.rows.add(constructor);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getConstructorIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddConstructor(constructor) {
    constructor.name = $("#input-add-name").val().trim();
    constructor.description = $("#input-add-description").val().trim();
    constructor.address = $("#input-add-address").val();
    constructor.phone = $("#input-add-phone").val().trim();
    constructor.phone2 = $("#input-add-phone2").val().trim();
    constructor.fax = $("#input-add-fax").val().trim();
    constructor.email = $("#input-add-email").val().trim();
    constructor.website = $("#input-add-website").val().trim();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditConstructor(constructor) {
    constructor.name = $("#input-edit-name").val().trim();
    constructor.description = $("#input-edit-description").val().trim();
    constructor.address = $("#input-edit-address").val();
    constructor.phone = $("#input-edit-phone").val().trim();
    constructor.phone2 = $("#input-edit-phone2").val().trim();
    constructor.fax = $("#input-edit-fax").val().trim();
    constructor.email = $("#input-edit-email").val().trim();
    constructor.website = $("#input-edit-website").val().trim();
}

/**hiển thị chi tiết lên modal */
function showConstructorDetailToModal(constructor) {
    $("#input-edit-name").val(constructor.name);
    $("#input-edit-description").val(constructor.description);
    $("#input-edit-address").val(constructor.address);
    $("#input-edit-phone").val(constructor.phone);
    $("#input-edit-phone2").val(constructor.phone2);
    $("#input-edit-fax").val(constructor.fax);
    $("#input-edit-email").val(constructor.email);
    $("#input-edit-website").val(constructor.website);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateConstructorSuccess() {
    $("#modal-edit-constructor").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListConstructor();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteConstructorSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListConstructor();
}

function handleAddConstructorSuccess() {
    $("#modal-add-constructor").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListConstructor();
}

