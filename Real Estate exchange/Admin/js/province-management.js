
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var provinceId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-provinces").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "code" }
    ],
    columnDefs: [
        {
            targets: 3,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListProvinces();
    // callApiGetListDepartment();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-province").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditProvince(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-provinces").on("click", ".btn-delete", function () {
    provinceId = getProvinceIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa thông tin tỉnh này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateProvince() {
    var province = {
        name: "",
        code: "" 
    }
    getInputAddProvince(province);
    callApiAddProvince(province);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateProvince() {
    var province = {
        name: "",
        code: ""
    }
    console.log(province);
    getInputEditProvince(province);
    callApiUpdateProvince(province);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditProvince(btnEdit) {
    provinceId = getProvinceIdFromButton(btnEdit);
    callApiGetProvinceById(provinceId);
    $("#modal-edit-province").modal("show");
    console.log(provinceId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListProvinces() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/provinces",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListProvinceOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddProvince(province) {
    $.ajax({
        url: "http://localhost:8080/api/provinces/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(province),
        success: function () {
            handleAddProvinceSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateProvince(province) {
    $.ajax({
        url: "http://localhost:8080/api/provinces/update/" + provinceId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(province),
        success: function () {
            handleUpdateProvinceSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/provinces/delete/" + provinceId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteProvinceSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetProvinceById(id) {
    $.ajax({
        url: "http://localhost:8080/api/provinces/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showProvinceDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListProvinceOnTable(province) {
    dataTable.clear();
    dataTable.rows.add(province);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getProvinceIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddProvince(province) {
    province.name = $("#input-add-name").val().trim();
    province.code = $("#input-add-code").val().trim();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditProvince(province) {
    province.name = $("#input-edit-name").val().trim();
    province.code = $("#input-edit-code").val().trim();
}

/**hiển thị chi tiết lên modal */
function showProvinceDetailToModal(province) {
    $("#input-edit-name").val(province.name);
    $("#input-edit-code").val(province.code);
  
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateProvinceSuccess() {
    $("#modal-edit-province").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListProvinces();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteProvinceSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListProvinces();
}

function handleAddProvinceSuccess() {
    $("#modal-add-province").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListProvinces();
}

