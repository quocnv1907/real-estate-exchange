
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var streetId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-streets").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "prefix" }
    ],
    columnDefs: [
        {
            targets: 3,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListStreet();
    // callApiGetListDepartment();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-street").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditStreet(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-streets").on("click", ".btn-delete", function () {
    streetId = getStreetIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa thông tin đường này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateStreet() {
    var street = {
        name: "",
        prefix: "" 
    }
    getInputAddStreet(street);
    callApiAddStreet(street);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateStreet() {
    var street = {
        name: "",
        prefix: ""
    }
    console.log(street);
    getInputEditStreet(street);
    callApiUpdateStreet(street);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditStreet(btnEdit) {
    streetId = getStreetIdFromButton(btnEdit);
    callApiGetStreetById(streetId);
    $("#modal-edit-street").modal("show");
    console.log(streetId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListStreet() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/streets",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListStreetOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddStreet(street) {
    $.ajax({
        url: "http://localhost:8080/api/streets/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(street),
        success: function () {
            handleAddStreetSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateStreet(street) {
    $.ajax({
        url: "http://localhost:8080/api/streets/update/" + streetId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(street),
        success: function () {
            handleUpdateStreetSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/streets/delete/" + streetId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteProvinceSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetStreetById(id) {
    $.ajax({
        url: "http://localhost:8080/api/streets/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showStreetDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListStreetOnTable(province) {
    dataTable.clear();
    dataTable.rows.add(province);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getStreetIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddStreet(prefix) {
    prefix.name = $("#input-add-name").val().trim();
    prefix.prefix = $("#select-add-prefix").val();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditStreet(street) {
    street.name = $("#input-edit-name").val().trim();
    street.prefix = $("#select-edit-prefix").val();
}

/**hiển thị chi tiết lên modal */
function showStreetDetailToModal(street) {
    $("#input-edit-name").val(street.name);
    $("#select-edit-prefix").val(street.prefix);
  
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateStreetSuccess() {
    $("#modal-edit-street").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListStreet();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteProvinceSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListStreet();
}

function handleAddStreetSuccess() {
    $("#modal-add-street").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListStreet();
}

