
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var designUnitId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-designers").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "description" },
        { data: "address" },
        { data: "phone" },
        { data: "fax" },
        { data: "website" },
        { data: "email" },
        { data: "phone2" },
    ],
    columnDefs: [
        {
            targets: 9,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListDesignUnit();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-designer").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditDesignUnit(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-designers").on("click", ".btn-delete", function () {
    designUnitId = getDesignerIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa thông tin đơn vị thiết kế này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});


/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateDesignUnit() {
    var designUnit = {
        name: "",
        description: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: ""
    }
    getInputAddDesignUnit(designUnit);
    callApiAddDesignUnit(designUnit);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateDesignUnit() {
    var designUnit = {
        name: "",
        description: "",
        address: "",
        phone: "",
        phone2: "",
        fax: "",
        email: "",
        website: ""
    }
    getInputEditDesigner(designUnit);
    callApiUpdateDesignUnit(designUnit);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditDesignUnit(btnEdit) {
    designUnitId = getDesignerIdFromButton(btnEdit);
    callApiGetDesignerById(designUnitId);
    $("#modal-edit-designer").modal("show");
    console.log(designUnitId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListDesignUnit() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/designUnits",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListDesignUnitOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddDesignUnit(designerUnit) {
    $.ajax({
        url: "http://localhost:8080/api/designUnits/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(designerUnit),
        success: function () {
            handleAddDesignUnitSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateDesignUnit(designUnit) {
    $.ajax({
        url: "http://localhost:8080/api/designUnits/update/" + designUnitId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(designUnit),
        success: function () {
            handleUpdateDesignerSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/designUnits/delete/" + designUnitId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteDesignUnitSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetDesignerById(id) {
    $.ajax({
        url: "http://localhost:8080/api/designUnits/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showDesignerDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListDesignUnitOnTable(designUnit) {
    dataTable.clear();
    dataTable.rows.add(designUnit);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getDesignerIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddDesignUnit(designUnit) {
    designUnit.name = $("#input-add-name").val().trim();
    designUnit.description = $("#input-add-description").val().trim();
    designUnit.address = $("#input-add-address").val();
    designUnit.phone = $("#input-add-phone").val().trim();
    designUnit.phone2 = $("#input-add-phone2").val().trim();
    designUnit.fax = $("#input-add-fax").val().trim();
    designUnit.email = $("#input-add-email").val().trim();
    designUnit.website = $("#input-add-website").val().trim();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditDesigner(designer) {
    designer.name = $("#input-edit-name").val().trim();
    designer.description = $("#input-edit-description").val().trim();
    designer.address = $("#input-edit-address").val();
    designer.phone = $("#input-edit-phone").val().trim();
    designer.phone2 = $("#input-edit-phone2").val().trim();
    designer.fax = $("#input-edit-fax").val().trim();
    designer.email = $("#input-edit-email").val().trim();
    designer.website = $("#input-edit-website").val().trim();
}

/**hiển thị chi tiết lên modal */
function showDesignerDetailToModal(designUnit) {
    $("#input-edit-name").val(designUnit.name);
    $("#input-edit-description").val(designUnit.description);
    $("#input-edit-address").val(designUnit.address);
    $("#input-edit-phone").val(designUnit.phone);
    $("#input-edit-phone2").val(designUnit.phone2);
    $("#input-edit-fax").val(designUnit.fax);
    $("#input-edit-email").val(designUnit.email);
    $("#input-edit-website").val(designUnit.website);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateDesignerSuccess() {
    $("#modal-edit-designer").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListDesignUnit();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteDesignUnitSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListDesignUnit();
}

function handleAddDesignUnitSuccess() {
    $("#modal-add-constructor").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListDesignUnit();
}

