
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
const gCONTENT_TYPE = "application/json";
var districtId;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var dataTable = $("#table-districts").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "prefix" }
    ],
    columnDefs: [
        {
            targets: 3,
            render: function (data, type, row) {
                return `
                    <a class="btn btn-warning my-1 btn-sm btn-edit">
                        <i class="fa fa-edit" style="font-size:12px"></i>
                    </a>
                    <a class="btn btn-danger btn-sm btn-delete">
                        <i class="fas fa-trash" style="font-size:14px"></i>
                    </a> 
                `;
            }
        },
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListDistricts();
    // callApiGetListDepartment();
});

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-district").modal("show");
});

/**Xử lý nhấn nút icon edit trên table */
$(document).on("click", ".btn-edit", function () {
    onBtnEditDistrict(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-provinces").on("click", ".btn-delete", function () {
    districtId = getDistrictIdFromButton(this);
    $("#message-confirm-delete").text("Bạn có chắc muốn xóa quận/huyện này không?");
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreateDistrict() {
    var district = {
        name: "",
        prefix: "" 
    }
    getInputAddDistrict(district);
    callApiAddDistrict(district);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateDistrict() {
    var district = {
        name: "",
        prefix: ""
    }
    console.log(district);
    getInputEditDistrict(district);
    callApiUpdateDistrict(district);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditDistrict(btnEdit) {
    districtId = getDistrictIdFromButton(btnEdit);
    callApiGetDistrictById(districtId);
    $("#modal-edit-district").modal("show");
    console.log(districtId);
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListDistricts() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/api/districts",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            loadListDistrictOnTable(data);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


/***GỌI API thêm pet */
function callApiAddDistrict(province) {
    $.ajax({
        url: "http://localhost:8080/api/districts/create",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(province),
        success: function () {
            handleAddDistrictSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdateDistrict(district) {
    $.ajax({
        url: "http://localhost:8080/api/district-update/" + districtId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(district),
        success: function () {
            handleUpdateDistrictSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: "http://localhost:8080/api/districts/delete/" + districtId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (data) {
            handleDeleteDistrictSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của nhân viên */
function callApiGetDistrictById(id) {
    $.ajax({
        url: "http://localhost:8080/api/districts-detail/" + id,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            showDistrictDetailToModal(data);
        },
        error: function (error) {
            console.log(error.status);
        }
    });
}

/***Tải danh sách nhân viên lên data table */
function loadListDistrictOnTable(district) {
    dataTable.clear();
    dataTable.rows.add(district);
    dataTable.draw();
}

//Lấy Id của nhân viên từ button 
function getDistrictIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    return dataTable.row(tableRow).data().id;
}

//lấy thông tin nhập vào
function getInputAddDistrict(district) {
    district.name = $("#input-add-name").val().trim();
    district.prefix = $("#select-add-prefix").val();
}

//lấy dữ liệu nhập vào trên form edit 
function getInputEditDistrict(district) {
    district.name = $("#input-edit-name").val().trim();
    district.prefix = $("#select-edit-prefix").val();
}

/**hiển thị chi tiết lên modal */
function showDistrictDetailToModal(district) {
    $("#input-edit-name").val(district.name);
    $("#select-edit-prefix").val(district.prefix);
  
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateDistrictSuccess() {
    $("#modal-edit-district").modal("hide");
    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    $("#message-update-success").html("Cập nhật thành công");
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    callApiGetListDistricts();
}

/**Xử lý front end khi xóa thành công */
function handleDeleteDistrictSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    $("#message-delete-success").html("Đã xóa thông tin");
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListDistricts();
}

function handleAddDistrictSuccess() {
    $("#modal-add-province").modal("hide");
    $("#modal-add-success").modal("show");
    $("#message-add-success").html("Thêm thành công");
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListDistricts();
}

